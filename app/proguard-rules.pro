# Preserve the line number information for debugging stack traces.
-keepattributes SourceFile,LineNumberTable
# Hide the original source file name.
-renamesourcefileattribute SourceFile

# For OkHttp that is used by Picasso:

# 1. JSR 305 annotations are for embedding nullability information.
-dontwarn javax.annotation.**

# 2. A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

# 3. Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*

# 4. OkHttp platform used only on JVM and when Conscrypt dependency is available.
-dontwarn okhttp3.internal.platform.ConscryptPlatform