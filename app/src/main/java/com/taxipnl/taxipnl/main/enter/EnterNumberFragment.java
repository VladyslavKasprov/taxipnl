package com.taxipnl.taxipnl.main.enter;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentEnterNumberBinding;
import com.taxipnl.taxipnl.util.FormatUtils;

@LayoutResId(R.layout.fragment_enter_number)
public abstract class EnterNumberFragment extends BaseFragment<FragmentEnterNumberBinding> {

    protected EnterNumberFragmentVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = ViewModelProviders.of(this).get(EnterNumberFragmentVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.model, model);
        bind(BR.fragment, this);

        view.etEnterNumberMainNumber.setKeyListener(null);
        view.etEnterNumberAdditionalNumber.setKeyListener(null);
        setHintForAdditional();

        if (!hasAdditionalNumberInput()) {
            view.etEnterNumberAdditionalNumber.setVisibility(View.GONE);
        }

        model.selectMain();

        view.btnEnterNumberEnter.setOnClickListener(v -> onEnter());
    }

    protected boolean hasAdditionalNumberInput() {
        return true;
    }

    public String formatMainNumber(double number) {
        return FormatUtils.formatAsDecimal(getContext(), number);
    }

    public void onEnter() {
        if (isEnteredDataValid()) {
            onValidEnter();
        }
    }

    protected abstract boolean isEnteredDataValid();

    protected abstract void onValidEnter();

    protected double getMainNumber() {
        Double mainNumber = model.getMainNumber().getValue();
        if (mainNumber == null) {
            mainNumber = 0d;
        }
        return mainNumber;
    }

    protected double getAdditionalNumber() {
        Double additionalNumber = model.getAdditionalNumber().getValue();
        if (additionalNumber == null) {
            additionalNumber = 0d;
        }
        return additionalNumber;
    }

    private void setHintForAdditional() {
        SpannableString hint = new SpannableString(getString(R.string.enter_additional_hint));
        hint.setSpan(new RelativeSizeSpan(0.7f), 0, hint.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        view.etEnterNumberAdditionalNumber.setHint(hint);
    }
}