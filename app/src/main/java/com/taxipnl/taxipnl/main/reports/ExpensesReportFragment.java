package com.taxipnl.taxipnl.main.reports;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.LinearLayout;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.adapter.decorator.VerticalSpanDecorator;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuConfig;
import com.taxipnl.taxipnl.databinding.FragmentExpensesReportBinding;
import com.taxipnl.taxipnl.db.pojo.ExpenseTypeWithAmount;
import com.taxipnl.taxipnl.view.CenteredDividerItemDecoration;

import java.util.List;

@LayoutResId(R.layout.fragment_expenses_report)
@MenuConfig(inheritsParentsMenu = true)
public class ExpensesReportFragment extends BaseFragment<FragmentExpensesReportBinding> {

    private final static int HORIZONTAL_SPAN_COUNT = 3;
    private final static int VERTICAL_SPAN_COUNT = 3;

    private BaseAdapter<ExpenseTypeWithAmount> adapter;

    private ReportsVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getGlobalViewModel(ReportsVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        setupExpensesList();
        bind(BR.model, model);

        model.getExpenseAmountByType().observe(getViewLifecycleOwner(), this::updateExpenses);
    }

    private void setupExpensesList() {
        adapter = new BaseAdapter<ExpenseTypeWithAmount>(item -> R.layout.item_expense_with_type)
                .setDecorator(new VerticalSpanDecorator(VERTICAL_SPAN_COUNT));
        view.rvExpensesReport.setAdapter(adapter);
        view.rvExpensesReport.setLayoutManager(new GridLayoutManager(getContext(), HORIZONTAL_SPAN_COUNT));
        view.rvExpensesReport.addItemDecoration(
                new CenteredDividerItemDecoration(getContext(), LinearLayout.HORIZONTAL, false, R.dimen.spacing_12)
        );
        view.rvExpensesReport.addItemDecoration(
                new CenteredDividerItemDecoration(getContext(), LinearLayout.VERTICAL, false, R.dimen.spacing_12)
        );
        view.rvExpensesReport.setItemAnimator(null);
    }

    private void updateExpenses(List<ExpenseTypeWithAmount> items) {
        if (items != null) {
            adapter.setItems(items);
            view.rvExpensesReport.scrollToPosition(0);
        }
    }
}