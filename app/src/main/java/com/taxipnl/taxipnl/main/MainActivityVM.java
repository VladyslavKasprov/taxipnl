package com.taxipnl.taxipnl.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.SingleLiveEvent;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.db.entity.Shift;
import com.taxipnl.taxipnl.db.pojo.AuthorizationStatus;
import com.taxipnl.taxipnl.db.pojo.JobWithProvider;
import com.taxipnl.taxipnl.main.items.ProviderWithIncome;
import com.taxipnl.taxipnl.repository.BillingRepository;
import com.taxipnl.taxipnl.repository.JobsRepository;
import com.taxipnl.taxipnl.repository.ProvidersRepository;
import com.taxipnl.taxipnl.repository.ShiftsRepository;
import com.taxipnl.taxipnl.repository.TargetsRepository;
import com.taxipnl.taxipnl.repository.UserRepository;
import com.taxipnl.taxipnl.util.EconomicUtils;
import com.taxipnl.taxipnl.util.Safe;
import com.taxipnl.taxipnl.util.ShiftUtils;
import com.taxipnl.taxipnl.util.constants.SortingMode;

import org.threeten.bp.LocalDateTime;

import java.util.List;

import javax.inject.Inject;

public class MainActivityVM extends ViewModel {

    private ShiftsRepository shiftsRepository;
    private JobsRepository jobsRepository;

    private SingleLiveEvent<Boolean> isSubscriptionActive = new SingleLiveEvent<>();
    private SingleLiveEvent<AuthorizationStatus> authorizationStatus = new SingleLiveEvent<>();

    private LiveData<Shift> latestShift;
    private SingleLiveEvent<Boolean> isLatestShiftOpened = new SingleLiveEvent<>();
    private MediatorLiveData<Long> latestShiftDuration = new MediatorLiveData<>();
    private LiveData<List<JobWithProvider>> latestShiftJobs;
    private LiveData<Integer> latestShiftJobsCount;
    private LiveData<Double> latestShiftIncome;
    private MediatorLiveData<Double> latestShiftIncomePerHour = new MediatorLiveData<>();
    private LiveData<Double> latestShiftCashRevenue;
    private LiveData<List<Provider>> allProviders;
    private MediatorLiveData<List<ProviderWithIncome>> latestShiftIncomeByProvider = new MediatorLiveData<>();

    private MutableLiveData<Integer> latestShiftJobsSortingMode = new MutableLiveData<>();

    private LiveData<List<Shift>> weeklyShifts;
    private MediatorLiveData<Long> weeklyShiftsDuration = new MediatorLiveData<>();
    private LiveData<List<JobWithProvider>> weeklyShiftsJobs;
    private LiveData<Integer> weeklyShiftsJobsCount;
    private LiveData<Double> weeklyShiftsNonStreetCashIncome;
    private LiveData<Double> weeklyShiftsStreetCashIncome;
    private MediatorLiveData<Double> weeklyShiftsIncome = new MediatorLiveData<>();
    private LiveData<Double> weeklyTarget;
    private MediatorLiveData<Double> weeklyPerformance = new MediatorLiveData<>();

    private SingleLiveEvent<Integer> navigateTo = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> navigateToStart = new SingleLiveEvent<>();

    @Inject
    public MainActivityVM(BillingRepository billingRepository, ShiftsRepository shiftsRepository,
                          ProvidersRepository providersRepository, JobsRepository jobsRepository,
                          TargetsRepository targetsRepository, UserRepository userRepository) {
        this.shiftsRepository = shiftsRepository;
        this.jobsRepository = jobsRepository;

        isSubscriptionActive.addSource(billingRepository.isSubscriptionActive(), isSubscriptionActive::setValue);
        authorizationStatus.addSource(userRepository.getAuthorizationStatus(), authorizationStatus::setValue);

        latestShift = shiftsRepository.getLatestShift();
        isLatestShiftOpened.addSource(latestShift, latestShift ->
                isLatestShiftOpened.setValue(ShiftUtils.isShiftOpened(latestShift)));

        navigateTo.addSource(isSubscriptionActive, this::onIsSubscriptionActiveUpdate);
        navigateTo.addSource(authorizationStatus, this::onAuthorizationStatusUpdate);
        navigateTo.addSource(isLatestShiftOpened, this::onIsLatestShiftOpenedUpdate);

        latestShiftDuration.addSource(latestShift, latestShift ->
                latestShiftDuration.setValue(ShiftUtils.getShiftDuration(latestShift)));

        MediatorLiveData<Void> loadLatestShiftJobsTrigger = new MediatorLiveData<>();
        loadLatestShiftJobsTrigger.addSource(latestShift, aVoid -> loadLatestShiftJobsTrigger.setValue(null));
        loadLatestShiftJobsTrigger.addSource(latestShiftJobsSortingMode, aVoid -> loadLatestShiftJobsTrigger.setValue(null));
        latestShiftJobsSortingMode.setValue(SortingMode.Jobs.BY_ENTERED_DATE_DESC);
        latestShiftJobs = Transformations.switchMap(loadLatestShiftJobsTrigger, aVoid -> {
            if (ShiftUtils.isShiftOpened(latestShift.getValue()) && latestShiftJobsSortingMode.getValue() != null) {
                return jobsRepository.getShiftJobs(latestShift.getValue(), latestShiftJobsSortingMode.getValue());
            } else {
                return new MutableLiveData<>();
            }
        });
        latestShiftJobsCount = Transformations.map(latestShiftJobs, Safe::size);
        latestShiftIncome = Transformations.map(latestShiftJobs, EconomicUtils::getIncome);
        latestShiftIncomePerHour.addSource(latestShiftIncome, latestShiftIncome ->
                updateLatestShiftIncomePerHour(latestShiftIncome, latestShiftDuration.getValue()));
        latestShiftIncomePerHour.addSource(latestShiftDuration, latestShiftDuration ->
                updateLatestShiftIncomePerHour(latestShiftIncome.getValue(), latestShiftDuration));
        latestShiftCashRevenue = Transformations.map(latestShiftJobs, EconomicUtils::getCashRevenue);
        allProviders = providersRepository.getAll();
        latestShiftIncomeByProvider.addSource(allProviders, allProviders ->
                updateLatestShiftIncomeByProvider(allProviders, latestShiftJobs.getValue()));
        latestShiftIncomeByProvider.addSource(latestShiftJobs, latestShiftJobs ->
                updateLatestShiftIncomeByProvider(allProviders.getValue(), latestShiftJobs));


        // Shifts that were started after the start of latest shift's week.
        // This way we handle the case where an opened shift rolls over to next week.
        weeklyShifts = Transformations.switchMap(latestShift, latestShift ->
                shiftsRepository.getShiftsStartedAfterStartOfWeek(Safe.getStartDate(latestShift)));
        weeklyShiftsDuration.addSource(weeklyShifts, weeklyShifts ->
                weeklyShiftsDuration.setValue(ShiftUtils.getShiftsDuration(weeklyShifts)));

        // Jobs that were entered during weekly shifts.
        weeklyShiftsJobs = Transformations.switchMap(weeklyShifts, jobsRepository::getShiftsJobs);
        weeklyShiftsJobsCount = Transformations.map(weeklyShiftsJobs, Safe::size);
        weeklyShiftsNonStreetCashIncome = Transformations.map(weeklyShiftsJobs, EconomicUtils::getNonStreetCashIncome);
        weeklyShiftsStreetCashIncome = Transformations.map(weeklyShiftsJobs, EconomicUtils::getStreetCashIncome);
        weeklyShiftsIncome.addSource(weeklyShiftsNonStreetCashIncome, weeklyShiftsNonStreetIncome ->
                updateWeeklyShiftsIncome(weeklyShiftsNonStreetIncome, weeklyShiftsStreetCashIncome.getValue()));
        weeklyShiftsIncome.addSource(weeklyShiftsStreetCashIncome, weeklyShiftsStreetIncome ->
                updateWeeklyShiftsIncome(weeklyShiftsNonStreetCashIncome.getValue(), weeklyShiftsStreetIncome));

        // We're interested in target acting on the start date of latest shift.
        // Any targets set after the start date of latest shift are ignored.
        weeklyTarget = Transformations.switchMap(latestShift, latestShift ->
                targetsRepository.getTargetActingOn(Safe.getStartDate(latestShift)));

        weeklyPerformance.addSource(weeklyShiftsIncome, weeklyShiftsIncome ->
                updateWeeklyPerformance(weeklyShiftsIncome, weeklyTarget.getValue()));
        weeklyPerformance.addSource(weeklyTarget, target ->
                updateWeeklyPerformance(weeklyShiftsIncome.getValue(), target));
    }

    public LiveData<Integer> getNavigateTo() {
        return navigateTo;
    }

    private void onIsSubscriptionActiveUpdate(Boolean isSubscriptionActive) {
        navigateTo(isSubscriptionActive, authorizationStatus.getValue(), isLatestShiftOpened.getValue());
    }

    private void onAuthorizationStatusUpdate(AuthorizationStatus authorizationStatus) {
        navigateTo(isSubscriptionActive.getValue(), authorizationStatus, isLatestShiftOpened.getValue());
    }

    private void onIsLatestShiftOpenedUpdate(Boolean isLatestShiftOpened) {
        navigateTo(isSubscriptionActive.getValue(), authorizationStatus.getValue(), isLatestShiftOpened);
    }

    private void navigateTo(Boolean isSubscriptionActive, AuthorizationStatus authorizationStatus, Boolean isLatestShiftOpened) {
        if (isSubscriptionActive != null && authorizationStatus != null && isLatestShiftOpened != null) {
            navigateTo.setValue(
                    !isSubscriptionActive ? R.id.subscribeGraph :
                            !authorizationStatus.isRegistered ? R.id.registerGraph :
                                    !authorizationStatus.isLoggedIn ? R.id.loginGraph :
                                            isLatestShiftOpened ? R.id.shiftGraph : R.id.homeGraph
            );
        }
    }

    public LiveData<Void> onNavigateToStart() {
        return navigateToStart;
    }

    public void navigateToStart() {
        navigateToStart.call();
    }

    public void openShift() {
        if (ShiftUtils.isShiftOpened(latestShift.getValue())) {
            isLatestShiftOpened.setValue(true);
        } else {
            shiftsRepository.create(new Shift(LocalDateTime.now()));
        }
    }

    public void closeShift() {
        if (ShiftUtils.isShiftOpened(latestShift.getValue())) {
            if (latestShiftJobs.getValue() == null || latestShiftJobs.getValue().isEmpty()) {
                shiftsRepository.delete(latestShift.getValue());
            } else {
                Shift shift = latestShift.getValue();
                shift.setEndDateTime(LocalDateTime.now());
                shiftsRepository.update(shift);
            }
        } else {
            isLatestShiftOpened.setValue(false);
        }
    }



    // DashboardFragment
    public LiveData<Long> getLatestShiftDuration() {
        return latestShiftDuration;
    }

    public LiveData<Integer> getLatestShiftJobsCount() {
        return latestShiftJobsCount;
    }

    public LiveData<Double> getLatestShiftIncome() {
        return latestShiftIncome;
    }

    public LiveData<Double> getLatestShiftIncomePerHour() {
        return latestShiftIncomePerHour;
    }

    public LiveData<Double> getLatestShiftCashRevenue() {
        return latestShiftCashRevenue;
    }

    public LiveData<List<ProviderWithIncome>> getLatestShiftIncomeByProvider() {
        return latestShiftIncomeByProvider;
    }

    public LiveData<Long> getWeeklyShiftsDuration() {
        return weeklyShiftsDuration;
    }

    public LiveData<Integer> getWeeklyShiftsJobsCount() {
        return weeklyShiftsJobsCount;
    }

    public LiveData<Double> getWeeklyShiftsNonStreetCashIncome() {
        return weeklyShiftsNonStreetCashIncome;
    }

    public LiveData<Double> getWeeklyShiftsStreetCashIncome() {
        return weeklyShiftsStreetCashIncome;
    }

    public LiveData<Double> getWeeklyShiftsIncome() {
        return weeklyShiftsIncome;
    }

    public LiveData<Double> getWeeklyTarget() {
        return weeklyTarget;
    }

    public LiveData<Double> getWeeklyPerformance() {
        return weeklyPerformance;
    }

    public void updateDurations() {
        latestShiftDuration.setValue(ShiftUtils.getShiftDuration(latestShift.getValue()));
        weeklyShiftsDuration.setValue(ShiftUtils.getShiftsDuration(weeklyShifts.getValue()));
    }

    private void updateLatestShiftIncomePerHour(Double income, Long durationInSeconds) {
        if (income != null && durationInSeconds != null) {
            double durationInHours = (durationInSeconds + 1) / (double) 3600;
            latestShiftIncomePerHour.setValue(income / durationInHours);
        }
    }

    private void updateWeeklyShiftsIncome(Double addend1, Double addend2) {
        if (addend1 != null && addend2 != null) {
            weeklyShiftsIncome.setValue(addend1 + addend2);
        }
    }

    private void updateLatestShiftIncomeByProvider(List<Provider> providers, List<JobWithProvider> jobs) {
        if (providers != null && jobs != null) {
            latestShiftIncomeByProvider.setValue(EconomicUtils.getIncomeByProvider(providers, jobs));
        }
    }

    private void updateWeeklyPerformance(Double weeklyShiftsIncome, Double weeklyTarget) {
        if (weeklyShiftsIncome != null && weeklyTarget != null) {
            weeklyPerformance.setValue(weeklyShiftsIncome - weeklyTarget);
        }
    }

    public LiveData<Integer> getSortingMode() {
        return latestShiftJobsSortingMode;
    }

    public void toggleSortByDateTime() {
        Integer currentMode = latestShiftJobsSortingMode.getValue();
        if (currentMode == null || currentMode == SortingMode.Jobs.BY_ENTERED_DATE_DESC) {
            latestShiftJobsSortingMode.setValue(SortingMode.Jobs.BY_ENTERED_DATE_ASC);
        } else {
            latestShiftJobsSortingMode.setValue(SortingMode.Jobs.BY_ENTERED_DATE_DESC);
        }
    }

    public void toggleSortByProvider() {
        Integer currentMode = latestShiftJobsSortingMode.getValue();
        if (currentMode == null || currentMode == SortingMode.Jobs.BY_PROVIDER_ORDER_DESC) {
            latestShiftJobsSortingMode.setValue(SortingMode.Jobs.BY_PROVIDER_ORDER_ASC);
        } else {
            latestShiftJobsSortingMode.setValue(SortingMode.Jobs.BY_PROVIDER_ORDER_DESC);
        }
    }

    public void toggleSortByFare() {
        Integer currentMode = latestShiftJobsSortingMode.getValue();
        if (currentMode == null || currentMode == SortingMode.Jobs.BY_FARE_DESC) {
            latestShiftJobsSortingMode.setValue(SortingMode.Jobs.BY_FARE_ASC);
        } else {
            latestShiftJobsSortingMode.setValue(SortingMode.Jobs.BY_FARE_DESC);
        }
    }

    // JobsFilterFragment
    public LiveData<List<Provider>> getAllProviders() {
        return allProviders;
    }

    // DashboardExtraFragment
    public LiveData<List<JobWithProvider>> getLatestShiftJobs() {
        return latestShiftJobs;
    }


    // AddJobEnterFareTollFragment
    public void addJob(double fare, double toll, int providerId) {
        if (ShiftUtils.isShiftOpened(latestShift.getValue())) {
            LocalDateTime enteredDateTime = LocalDateTime.now();
            int shiftId = latestShift.getValue().getId();
            jobsRepository.insert(enteredDateTime, fare, toll, providerId, shiftId);
        }
    }
}