package com.taxipnl.taxipnl.main.settings.changetarget;

import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.repository.TargetsRepository;

import javax.inject.Inject;

public class ChangeTargetVM extends ViewModel {

    private TargetsRepository repository;

    @Inject
    public ChangeTargetVM(TargetsRepository repository) {
        this.repository = repository;
    }

    public void setCurrentTarget(double target) {
        repository.setCurrentTarget(target);
    }
}