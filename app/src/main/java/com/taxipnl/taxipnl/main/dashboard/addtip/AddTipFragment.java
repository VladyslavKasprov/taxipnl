package com.taxipnl.taxipnl.main.dashboard.addtip;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.main.enter.EnterNumberFragment;

public class AddTipFragment extends EnterNumberFragment {

    private TipsVM tipsVM;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tipsVM = getGlobalViewModel(TipsVM.class);
    }

    @Override
    protected boolean hasAdditionalNumberInput() {
        return false;
    }

    @Override
    protected boolean isEnteredDataValid() {
        double target = getMainNumber();
        if (target <= 0) {
            displayMessage(R.string.persist_tip_error_zero_amount);
            return false;
        }

        return true;
    }

    @Override
    protected void onValidEnter() {
        tipsVM.createTip(getMainNumber());
        findNavController().navigateUp();
    }
}