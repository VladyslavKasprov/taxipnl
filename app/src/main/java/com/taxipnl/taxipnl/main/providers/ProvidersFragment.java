package com.taxipnl.taxipnl.main.providers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuResId;
import com.taxipnl.taxipnl.databinding.FragmentProvidersBinding;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.main.ProvidersVM;
import com.taxipnl.taxipnl.view.DividerItemDecoration;

import java.util.Collections;
import java.util.List;

@LayoutResId(R.layout.fragment_providers)
@MenuResId(R.menu.providers)
public class ProvidersFragment extends BaseFragment<FragmentProvidersBinding> {

    BaseAdapter<Provider> adapter;

    ProvidersVM providersModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        providersModel = getGlobalViewModel(ProvidersVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        setupProvidersView();

        providersModel.getAllProvidersExceptStreet().observe(getViewLifecycleOwner(), this::updateProviders);
    }

    @Override
    public void onPause() {
        super.onPause();
        providersModel.updateProvidersOrder(adapter.getItems());
    }

    private void setupProvidersView() {
        adapter = new BaseAdapter<Provider>(item -> R.layout.item_provider)
                .setHandler(provider -> findNavController().navigate(
                        ProvidersFragmentDirections.actionProvidersToEditProvider(provider.getId())
                ));
        view.rvProviders.setAdapter(adapter);
        view.rvProviders.addItemDecoration(new DividerItemDecoration(getContext()));

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                Collections.swap(adapter.getItems(), viewHolder.getAdapterPosition(), target.getAdapterPosition());
                adapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {}
        }).attachToRecyclerView(view.rvProviders);
    }

    private void updateProviders(List<Provider> providers) {
        if (providers != null) {
            adapter.setItems(providers);
            view.rvProviders.scrollToPosition(0);
        }
    }
}