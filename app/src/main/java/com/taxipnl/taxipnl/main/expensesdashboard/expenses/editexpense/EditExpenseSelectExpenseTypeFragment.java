package com.taxipnl.taxipnl.main.expensesdashboard.expenses.editexpense;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentSelectExpenseTypeBinding;
import com.taxipnl.taxipnl.db.entity.ExpenseType;
import com.taxipnl.taxipnl.main.ExpenseTypesVM;
import com.taxipnl.taxipnl.view.DividerItemDecoration;

import java.util.List;

@LayoutResId(R.layout.fragment_select_expense_type)
public class EditExpenseSelectExpenseTypeFragment extends BaseFragment<FragmentSelectExpenseTypeBinding> {

    private BaseAdapter<ExpenseType> adapter;

    private EditExpenseFragmentVM editExpenseModel;
    private ExpenseTypesVM expenseTypesVM;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        editExpenseModel = getGlobalViewModel(EditExpenseFragmentVM.class);
        expenseTypesVM = getGlobalViewModel(ExpenseTypesVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupExpenseTypesView();

        expenseTypesVM.getAll().observe(getViewLifecycleOwner(), this::updateExpenseTypes);
    }

    private void setupExpenseTypesView() {
        adapter = new BaseAdapter<ExpenseType>(item -> R.layout.item_expense_type)
                .setHandler(expenseType -> {
                    editExpenseModel.setExpenseTypeId(expenseType.getId());
                    findNavController().navigateUp();
                });
        view.expenseTypesListForSelection.setAdapter(adapter);
        view.expenseTypesListForSelection.addItemDecoration(new DividerItemDecoration(getContext()));
    }

    private void updateExpenseTypes(List<ExpenseType> expenseTypes) {
        if (expenseTypes != null) {
            adapter.setItems(expenseTypes);
        }
    }
}