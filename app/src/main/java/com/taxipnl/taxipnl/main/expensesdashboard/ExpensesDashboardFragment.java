package com.taxipnl.taxipnl.main.expensesdashboard;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentExpensesDashboardBinding;
import com.taxipnl.taxipnl.db.entity.ExpenseType;
import com.taxipnl.taxipnl.main.ExpenseTypesVM;
import com.taxipnl.taxipnl.main.ExpensesVM;
import com.taxipnl.taxipnl.util.constants.Constants;
import com.taxipnl.taxipnl.view.DividerItemDecoration;

@LayoutResId(R.layout.fragment_expenses_dashboard)
public class ExpensesDashboardFragment extends BaseFragment<FragmentExpensesDashboardBinding> {

    BaseAdapter<ExpenseType> adapter;

    ExpenseTypesVM expenseTypesModel;
    ExpensesVM expensesModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        expenseTypesModel = getGlobalViewModel(ExpenseTypesVM.class);
        expensesModel = getGlobalViewModel(ExpensesVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.expensesModel, expensesModel);
        setupExpenseTypesView();

        expenseTypesModel.getAllDispensableExpenseTypesPlusToll().observe(getViewLifecycleOwner(), expenseTypes -> {
            if (expenseTypes != null) {
                adapter.setItems(expenseTypes);
            }
        });

        view.hitboxDashboardExtraToAddFuelExpense.setOnClickListener(v -> findNavController().navigate(
                ExpensesDashboardFragmentDirections.actionExpensesDashboardToAddExpense(Constants.EXPENSE_TYPE_ID_FUEL)
        ));
    }

    @Override
    public void onResume() {
        super.onResume();
        expensesModel.updateWeeklyAmount();
    }

    private void setupExpenseTypesView() {
        adapter = new BaseAdapter<ExpenseType>(item -> R.layout.item_expenses_dashboard_expense_type)
                .setHandler(expenseType -> findNavController().navigate(
                        ExpensesDashboardFragmentDirections.actionExpensesDashboardToAddExpense(expenseType.getId())
                ));
        view.rvExpensesDashboardExpenseTypes.setAdapter(adapter);
        view.rvExpensesDashboardExpenseTypes.addItemDecoration(
                new DividerItemDecoration(getContext()).withPaddingStart(getContext(), R.dimen.spacing_16)
        );
    }
}