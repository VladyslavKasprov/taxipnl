package com.taxipnl.taxipnl.main.jobs.editjob;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.SingleLiveEvent;
import com.taxipnl.taxipnl.db.pojo.JobWithProvider;
import com.taxipnl.taxipnl.repository.JobsRepository;
import com.taxipnl.taxipnl.repository.ProvidersRepository;

import javax.inject.Inject;

public class EditJobFragmentVM extends ViewModel {

    private JobsRepository jobsRepository;

    private MutableLiveData<Integer> jobId = new MutableLiveData<>();
    private MutableLiveData<Integer> providerId = new MutableLiveData<>();
    private MediatorLiveData<JobWithProvider> job = new MediatorLiveData<>();
    private boolean hasBeenEdited = false;

    private SingleLiveEvent<Void> done = new SingleLiveEvent<>();

    @Inject
    public EditJobFragmentVM(ProvidersRepository providersRepository, JobsRepository jobsRepository) {
        this.jobsRepository = jobsRepository;

        job.addSource(
                Transformations.switchMap(jobId, jobsRepository::get),
                job::setValue
        );
        job.addSource(
                Transformations.switchMap(providerId, providersRepository::get),
                provider -> {
                    JobWithProvider job = this.job.getValue();
                    if (provider != null && job != null) {
                        hasBeenEdited = true;
                        job.commission = provider.getCommission();
                        job.providerName = provider.getName();
                        job.providerPaymentMethod = provider.getPaymentMethod();
                        this.job.postValue(job);
                    }
                }
        );
    }

    public void loadJob(Integer id) {
        jobId.setValue(id);
    }

    public LiveData<JobWithProvider> getJob() {
        return job;
    }

    public LiveData<Void> onDone() {
        return done;
    }

    public void setFareToll(double fare, double toll) {
        JobWithProvider job = this.job.getValue();
        if (job != null) {
            hasBeenEdited = true;
            job.fare = fare;
            job.toll = toll;
            this.job.setValue(job);
        }
    }

    public void setProviderId(int id) {
        providerId.setValue(id);
    }

    public void save() {
        JobWithProvider job = this.job.getValue();
        if (hasBeenEdited && job != null) {
            if (providerId.getValue() != null) {
                jobsRepository.update(job.id, job.fare, job.toll, providerId.getValue());
            } else {
                jobsRepository.update(job.id, job.fare, job.toll);
            }
        }
        done.call();
    }

    public void delete() {
        if (jobId.getValue() != null) {
            jobsRepository.delete(jobId.getValue());
            done.call();
        }
    }

    public void clear() {
        jobId.setValue(null);
        providerId.setValue(null);
        job.setValue(null);
        hasBeenEdited = false;
    }
}