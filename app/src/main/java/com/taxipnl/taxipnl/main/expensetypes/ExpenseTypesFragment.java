package com.taxipnl.taxipnl.main.expensetypes;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuResId;
import com.taxipnl.taxipnl.databinding.FragmentExpenseTypesBinding;
import com.taxipnl.taxipnl.db.entity.ExpenseType;
import com.taxipnl.taxipnl.main.ExpenseTypesVM;
import com.taxipnl.taxipnl.view.DividerItemDecoration;

import java.util.Collections;
import java.util.List;

@LayoutResId(R.layout.fragment_expense_types)
@MenuResId(R.menu.expense_types)
public class ExpenseTypesFragment extends BaseFragment<FragmentExpenseTypesBinding> {

    BaseAdapter<ExpenseType> adapter;

    ExpenseTypesVM expenseTypesModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        expenseTypesModel = getGlobalViewModel(ExpenseTypesVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        setupExpenseTypesView();

        expenseTypesModel.getAllDispensableExpenseTypes().observe(getViewLifecycleOwner(), this::updateExpenseType);
    }

    @Override
    public void onPause() {
        super.onPause();
        expenseTypesModel.updateExpenseTypesOrder(adapter.getItems());
    }

    private void setupExpenseTypesView() {
        adapter = new BaseAdapter<ExpenseType>(item -> R.layout.item_expense_type)
                .setHandler(expenseType -> findNavController().navigate(
                        ExpenseTypesFragmentDirections.actionExpenseTypesToEditExpenseType(expenseType.getId())
                ));
        view.rvExpenseTypes.setAdapter(adapter);
        view.rvExpenseTypes.addItemDecoration(new DividerItemDecoration(getContext()));

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                Collections.swap(adapter.getItems(), viewHolder.getAdapterPosition(), target.getAdapterPosition());
                adapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {}
        }).attachToRecyclerView(view.rvExpenseTypes);
    }

    private void updateExpenseType(List<ExpenseType> expenseTypes) {
        if (expenseTypes != null) {
            adapter.setItems(expenseTypes);
            view.rvExpenseTypes.scrollToPosition(0);
        }
    }
}