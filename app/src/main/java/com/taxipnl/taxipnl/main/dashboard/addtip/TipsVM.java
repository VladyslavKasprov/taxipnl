package com.taxipnl.taxipnl.main.dashboard.addtip;

import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.db.entity.Tip;
import com.taxipnl.taxipnl.repository.TipsRepository;

import org.threeten.bp.LocalDateTime;

import javax.inject.Inject;

public class TipsVM extends ViewModel {

    private TipsRepository repository;

    @Inject
    public TipsVM(TipsRepository repository) {
        this.repository = repository;
    }

    public void createTip(double amount) {
        repository.create(new Tip(LocalDateTime.now(), amount));
    }
}