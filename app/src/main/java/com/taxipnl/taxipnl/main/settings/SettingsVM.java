package com.taxipnl.taxipnl.main.settings;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.repository.TargetsRepository;
import com.taxipnl.taxipnl.repository.UserRepository;

import java.util.Currency;

import javax.inject.Inject;

public class SettingsVM extends ViewModel {

    private UserRepository userRepository;

    private LiveData<Double> currentTarget;
    private LiveData<Currency> currency;

    @Inject
    public SettingsVM(TargetsRepository targetsRepository, UserRepository userRepository) {
        this.userRepository = userRepository;

        currentTarget = targetsRepository.getCurrentTarget();
        currency = userRepository.getCurrency();
    }

    public LiveData<Double> getCurrentTarget() {
        return currentTarget;
    }

    public LiveData<Currency> getCurrency() {
        return currency;
    }

    public void logout() {
        userRepository.logout();
    }
}