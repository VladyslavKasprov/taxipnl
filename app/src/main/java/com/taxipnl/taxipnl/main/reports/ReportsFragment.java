package com.taxipnl.taxipnl.main.reports;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuResId;
import com.taxipnl.taxipnl.databinding.FragmentReportsBinding;

import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;

@LayoutResId(R.layout.fragment_reports)
@MenuResId(R.menu.reports)
public class ReportsFragment extends BaseFragment<FragmentReportsBinding> {

    private ReportsVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        super.onCreate(savedInstanceState);
        model = getGlobalViewModel(ReportsVM.class);
        model.setDefaultFilter();
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.model, model);

        if (getActivity() != null) {
            NavDestination currentReport = Navigation.findNavController(getActivity(), R.id.nav_host_fragment_reports).getCurrentDestination();
            selectReportTitle(currentReport.getId());
        }

        view.btnReportsToIncomeReport.setOnClickListener(v -> {
            if (getActivity() != null) {
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment_reports).navigate(R.id.incomeReport);
                selectReportTitle(R.id.incomeReport);
            }
        });
        view.btnReportsToExpensesReport.setOnClickListener(v -> {
            if (getActivity() != null) {
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment_reports).navigate(R.id.expensesReport);
                selectReportTitle(R.id.expensesReport);
            }
        });
    }

    private void selectReportTitle(int destinationId) {
        switch (destinationId) {
            case R.id.incomeReport:
                view.btnReportsToIncomeReport.setSelected(true);
                view.btnReportsToExpensesReport.setSelected(false);
                break;
            case R.id.expensesReport:
                view.btnReportsToIncomeReport.setSelected(false);
                view.btnReportsToExpensesReport.setSelected(true);
                break;
        }
    }
}