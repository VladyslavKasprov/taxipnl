package com.taxipnl.taxipnl.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.db.entity.Expense;
import com.taxipnl.taxipnl.repository.ExpensesRepository;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;

import javax.inject.Inject;

public class ExpensesVM extends ViewModel {

    private ExpensesRepository repository;

    private MutableLiveData<LocalDate> currentDate = new MutableLiveData<>();
    private LiveData<Double> weeklyAmount;

    @Inject
    public ExpensesVM(ExpensesRepository repository) {
        this.repository = repository;
        currentDate.setValue(LocalDate.now());
        weeklyAmount = Transformations.switchMap(currentDate, repository::getWeeklyAmount);
    }

    public LiveData<Double> getWeeklyAmount() {
        return weeklyAmount;
    }

    public void updateWeeklyAmount() {
        currentDate.setValue(LocalDate.now());
    }

    public void create(double amount, int expenseTypeId) {
        LocalDateTime enteredDateTime = LocalDateTime.now();
        repository.create(new Expense(enteredDateTime, amount, expenseTypeId));
    }
}