package com.taxipnl.taxipnl.main.setup.register;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.SingleLiveEvent;
import com.taxipnl.taxipnl.repository.UserRepository;
import com.taxipnl.taxipnl.util.constants.Constants;

import java.util.Currency;
import java.util.List;

import javax.inject.Inject;

public class RegisterVM extends ViewModel {

    private UserRepository repository;

    private MutableLiveData<List<Currency>> currencies = new MutableLiveData<>();

    public MutableLiveData<String> password = new MutableLiveData<>();
    public MutableLiveData<String> passwordRepeated = new MutableLiveData<>();
    public MutableLiveData<Integer> currencyPosition = new MutableLiveData<>();

    private SingleLiveEvent<Integer> error = new SingleLiveEvent<>();

    @Inject
    public RegisterVM(UserRepository repository) {
        this.repository = repository;

        currencies.setValue(Constants.CURRENCIES);
    }

    public LiveData<List<Currency>> getCurrencies() {
        return currencies;
    }

    public LiveData<Integer> onError() {
        return error;
    }

    public void register() {
        String password = this.password.getValue();
        if (password == null || password.isEmpty()) {
            error.setValue(R.string.register_error_empty_password);
            return;
        }

        String passwordRepeated = this.passwordRepeated.getValue();
        if (!password.equals(passwordRepeated)) {
            error.setValue(R.string.register_error_passwords_dont_match);
            return;
        }

        Integer currencyPosition = this.currencyPosition.getValue();
        List<Currency> currencies = this.currencies.getValue();
        Currency currency = null;
        if (currencyPosition != null && currencies != null && currencyPosition < currencies.size()) {
            currency = currencies.get(currencyPosition);
        }
        if (currency == null) {
            error.setValue(R.string.register_error_empty_currency);
            return;
        }

        repository.register(password, currency);
    }
}