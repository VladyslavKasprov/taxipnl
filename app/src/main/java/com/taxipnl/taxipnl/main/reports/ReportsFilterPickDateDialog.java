package com.taxipnl.taxipnl.main.reports;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.main.PickDateDialog;

public abstract class ReportsFilterPickDateDialog extends PickDateDialog {

    ReportsVM filterModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filterModel = getGlobalViewModel(ReportsVM.class);
    }
}