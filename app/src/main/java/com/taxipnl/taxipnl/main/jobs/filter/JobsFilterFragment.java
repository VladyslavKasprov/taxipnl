package com.taxipnl.taxipnl.main.jobs.filter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentJobsFilterBinding;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.main.jobs.JobsFragmentVM;
import com.taxipnl.taxipnl.view.DividerItemDecoration;

import java.util.List;
import java.util.Set;

@LayoutResId(R.layout.fragment_jobs_filter)
public class JobsFilterFragment extends BaseFragment<FragmentJobsFilterBinding> {

    SelectProvidersAdapter adapter;

    JobsFragmentVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getGlobalViewModel(JobsFragmentVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        setupProvidersView();
        bind(BR.model, model);

        mainModel.getAllProviders().observe(getViewLifecycleOwner(), this::updateProviders);
        model.getProvidersIds().observe(getViewLifecycleOwner(), this::updateSelectedProviders);

        view.hitboxJobsFilterStartDate.setOnClickListener(v -> showPickStartDateDialog());
        view.hitboxJobsFilterEndDate.setOnClickListener(v -> showPickEndDateDialog());
        view.btnJobsFilterApply.setOnClickListener(v -> applyFilter());
        view.btnJobsFilterReset.setOnClickListener(v -> resetFilter());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        model.clearFilter();
    }

    private void setupProvidersView() {
        adapter = new SelectProvidersAdapter();
        view.rvJobsFilterProviders.setAdapter(adapter);
        view.rvJobsFilterProviders.addItemDecoration(new DividerItemDecoration(getContext()));
    }

    private void updateProviders(List<Provider> providers) {
        if (providers != null) {
            adapter.setItems(providers);
        }
    }

    private void updateSelectedProviders(Set<Integer> providersIds) {
        if (providersIds != null) {
            adapter.setSelectedProvidersIds(providersIds);
        }
    }

    protected void showPickStartDateDialog() {
        new JobsFilterPickStartDateDialog().showNow(getChildFragmentManager(), "");
    }

    protected void showPickEndDateDialog() {
        new JobsFilterPickEndDateDialog().showNow(getChildFragmentManager(), "");
    }

    protected void applyFilter() {
        model.setProvidersIds(adapter.getSelectedProvidersIds());
        model.applyFilter();
        findNavController().navigateUp();
    }

    protected void resetFilter() {
        model.resetFilter();
        findNavController().navigateUp();
    }
}