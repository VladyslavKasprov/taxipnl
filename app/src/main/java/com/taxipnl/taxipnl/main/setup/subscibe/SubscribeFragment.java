package com.taxipnl.taxipnl.main.setup.subscibe;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.Toast;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentSubscribeBinding;

@LayoutResId(R.layout.fragment_subscribe)
public class SubscribeFragment extends BaseFragment<FragmentSubscribeBinding> {

    SubscribeVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getLocalViewModel(SubscribeVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);

        model.getPrice().observe(getViewLifecycleOwner(), this::updatePrice);
        model.onLoading().observe(getViewLifecycleOwner(), this::disableControls);
        model.onError().observe(getViewLifecycleOwner(), this::showErrorDialog);

        view.btnSubscribeSubscribe.setOnClickListener(v -> model.subscribe(requireActivity()));
        view.tvSubscribePrivacyPolicy.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://taxipnl.com/privacy.pdf"));
            if (intent.resolveActivity(requireActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        });
        view.tvSubscribeTermsOfService.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://taxipnl.com/terms.pdf"));
            if (intent.resolveActivity(requireActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        });
    }

    private void updatePrice(String localizedPrice) {
        if (localizedPrice != null) {
            String firstLine = getString(R.string.subscribe_subscribe_1).toUpperCase();

            SpannableString secondLine = new SpannableString(getString(R.string.subscribe_subscribe_2, localizedPrice));
            secondLine.setSpan(new RelativeSizeSpan(0.65f), 0, secondLine.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            view.btnSubscribeSubscribe.setText(
                    new SpannableStringBuilder(firstLine)
                            .append("\n")
                            .append(secondLine)
            );

            view.tvSubscribeDescription2.setText(getString(R.string.subscribe_description_2, localizedPrice));
        }
    }

    private void disableControls(Void aVoid) {
        enableControls(false);
    }

    private void enableControls(boolean enabled) {
        view.btnSubscribeSubscribe.setEnabled(enabled);
    }

    private void showErrorDialog(BillingErrorResponse response) {
        enableControls(true);
        if (response != null) {
            BillingErrorResponseDialogFragment dialogFragment = new BillingErrorResponseDialogFragment();
            dialogFragment.setTargetFragment(this, 0);
            requireFragmentManager().beginTransaction()
                    .add(dialogFragment, "")
                    .commitAllowingStateLoss();
        } else {
            // TODO handle every error and remove
            Toast.makeText(requireContext(), "TODO received null BillingErrorResponse!", Toast.LENGTH_LONG).show();
        }
    }
}