package com.taxipnl.taxipnl.main.jobs.filter;

import org.threeten.bp.LocalDate;

import java.util.Set;

public class JobsFilter {

    public boolean isPeriodFilterEnabled;
    public LocalDate startDate;
    public LocalDate endDate;
    public boolean isProvidersFilterEnabled;
    public Set<Integer> providersIds;

    public JobsFilter(boolean isPeriodFilterEnabled, LocalDate startDate, LocalDate endDate,
                      boolean isProvidersFilterEnabled, Set<Integer> providersIds) {
        this.isPeriodFilterEnabled = isPeriodFilterEnabled;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isProvidersFilterEnabled = isProvidersFilterEnabled;
        this.providersIds = providersIds;
    }
}