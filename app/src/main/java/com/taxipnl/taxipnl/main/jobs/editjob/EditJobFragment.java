package com.taxipnl.taxipnl.main.jobs.editjob;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuResId;
import com.taxipnl.taxipnl.databinding.FragmentEditJobBinding;

@LayoutResId(R.layout.fragment_edit_job)
@MenuResId(R.menu.edit)
public class EditJobFragment extends BaseFragment<FragmentEditJobBinding> {

    private EditJobFragmentVM model;

    private int getJobId() {
        return EditJobFragmentArgs.fromBundle(getArguments()).getJobId();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getGlobalViewModel(EditJobFragmentVM.class);
        model.loadJob(getJobId());
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.model, model);

        model.onDone().observe(getViewLifecycleOwner(), onDone -> findNavController().popBackStack());

        view.etEditJobFare.setOnClickListener(v -> findNavController().navigate(
                EditJobFragmentDirections.actionEditJobToEditJobEnterFareToll(false)
        ));
        view.etEditJobToll.setOnClickListener(v -> findNavController().navigate(
                EditJobFragmentDirections.actionEditJobToEditJobEnterFareToll(true)
        ));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_delete) {
            model.delete();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        model.clear();
        super.onDestroy();
    }
}