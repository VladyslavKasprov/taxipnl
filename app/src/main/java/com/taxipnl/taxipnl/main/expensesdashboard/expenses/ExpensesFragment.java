package com.taxipnl.taxipnl.main.expensesdashboard.expenses;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuResId;
import com.taxipnl.taxipnl.databinding.FragmentExpensesBinding;
import com.taxipnl.taxipnl.db.pojo.ExpenseWithType;
import com.taxipnl.taxipnl.view.DividerItemDecoration;

import java.util.List;

@LayoutResId(R.layout.fragment_expenses)
@MenuResId(R.menu.expenses)
public class ExpensesFragment extends BaseFragment<FragmentExpensesBinding> {

    BaseAdapter<ExpenseWithType> adapter;

    ExpensesFragmentVM expensesModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        expensesModel = getGlobalViewModel(ExpensesFragmentVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bind(BR.expensesModel, expensesModel);
        setupExpensesView();

        expensesModel.getExpenses().observe(getViewLifecycleOwner(), this::updateExpenses);
    }

    public void setupExpensesView() {
        adapter = new BaseAdapter<ExpenseWithType>(item -> R.layout.item_expense)
                .setHandler(expense -> findNavController().navigate(
                        ExpensesFragmentDirections.actionExpensesToEditExpense(expense.id)
                ));
        view.rvExpenses.setAdapter(adapter);
        view.rvExpenses.addItemDecoration(
                new DividerItemDecoration(getContext()).withPaddingStart(getContext(), R.dimen.spacing_12)
        );
        view.rvExpenses.setItemAnimator(null);
    }

    public void updateExpenses(List<ExpenseWithType> expenses) {
        if (expenses != null) {
            adapter.setItems(expenses);
            view.rvExpenses.scrollToPosition(0);
        }
    }
}