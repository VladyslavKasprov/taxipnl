package com.taxipnl.taxipnl.main;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.DatePicker;

import com.taxipnl.taxipnl.base.BaseDialogFragment;

import org.threeten.bp.LocalDate;

public abstract class PickDateDialog extends BaseDialogFragment implements DatePickerDialog.OnDateSetListener {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LocalDate initial = getInitiallySelectedDate();

        return new DatePickerDialog(getActivity(), this,
                initial.getYear(), initial.getMonthValue() - 1, initial.getDayOfMonth());
    }

    protected abstract LocalDate getInitiallySelectedDate();

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        onDateSet(LocalDate.of(year, month + 1, dayOfMonth));
    }

    protected abstract void onDateSet(LocalDate date);
}