package com.taxipnl.taxipnl.main.settings;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentSettingsBinding;
import com.taxipnl.taxipnl.view.DividerItemDecoration;

@LayoutResId(R.layout.fragment_settings)
public class SettingsFragment extends BaseFragment<FragmentSettingsBinding> {

    private final static int[] SETTINGS_ITEMS = new int[] {
            R.layout.item_settings_providers, R.layout.item_settings_expense_types,
            R.layout.item_settings_jobs, R.layout.item_settings_change_target,
            R.layout.item_settings_send_feedback, R.layout.item_settings_reports,
            R.layout.item_settings_change_password, R.layout.item_settings_change_currency
    };

    BaseAdapter<Void> adapter;

    SettingsVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        model = getGlobalViewModel(SettingsVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.model, model);
        setupSettingsList();
    }

    private void setupSettingsList() {
        adapter = new BaseAdapter<Void>(item -> 0)
                .setHeaders(SETTINGS_ITEMS, model, getViewLifecycleOwner());
        view.rvSettings.setAdapter(adapter);
        view.rvSettings.addItemDecoration(new DividerItemDecoration(getContext()));
    }
}