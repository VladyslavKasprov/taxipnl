package com.taxipnl.taxipnl.main.settings.sendfeedback;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentSendFeedbackBinding;
import com.taxipnl.taxipnl.util.AndroidUtils;

@LayoutResId(R.layout.fragment_send_feedback)
public class SendFeedbackFragment extends BaseFragment<FragmentSendFeedbackBinding> {

    SendFeedbackVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getLocalViewModel(SendFeedbackVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.model, model);

        model.onDone().observe(getViewLifecycleOwner(), onDone -> findNavController().navigateUp());
        model.onError().observe(getViewLifecycleOwner(), this::displayMessage);

        AndroidUtils.showSoftKeyboard(getActivity(), view.etSendFeedbackFeedback);
    }

    @Override
    public void onDestroyView() {
        AndroidUtils.hideSoftKeyboard(getActivity());
        super.onDestroyView();
    }
}