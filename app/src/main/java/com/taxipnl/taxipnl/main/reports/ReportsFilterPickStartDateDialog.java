package com.taxipnl.taxipnl.main.reports;

import org.threeten.bp.LocalDate;

public class ReportsFilterPickStartDateDialog extends ReportsFilterPickDateDialog {

    @Override
    protected LocalDate getInitiallySelectedDate() {
        LocalDate date = filterModel.getStartDate().getValue();
        return date == null ? LocalDate.now() : date;
    }

    @Override
    public void onDateSet(LocalDate date) {
        filterModel.setStartDate(date);
    }
}