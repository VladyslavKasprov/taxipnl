package com.taxipnl.taxipnl.main.providers.persist;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.SingleLiveEvent;
import com.taxipnl.taxipnl.db.entity.PaymentMethod;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.repository.ProvidersRepository;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

public class PersistProviderFragmentVM extends ViewModel {

    private ProvidersRepository repository;

    private MutableLiveData<List<PaymentMethod>> paymentMethods = new MutableLiveData<>();

    private SingleLiveEvent<Void> done = new SingleLiveEvent<>();
    private SingleLiveEvent<Integer> error = new SingleLiveEvent<>();

    @Inject
    public PersistProviderFragmentVM(ProvidersRepository repository) {
        this.repository = repository;

        paymentMethods.setValue(Arrays.asList(PaymentMethod.values()));
    }

    public LiveData<List<PaymentMethod>> getPaymentMethods() {
        return paymentMethods;
    }

    public LiveData<Void> onDone() {
        return done;
    }

    public LiveData<Integer> onError() {
        return error;
    }

    public LiveData<Provider> get(int id) {
        return repository.get(id);
    }

    public void create(Provider provider) {
        checkDuplicateAndPersist(provider, true);
    }

    public void update(Provider provider) {
        checkDuplicateAndPersist(provider, false);
    }

    private void checkDuplicateAndPersist(Provider provider, boolean isCreate) {
        LiveData<Provider> request = repository.get(provider.getName(), provider.getPaymentMethod());
        error.addSource(request, duplicate -> {
            error.removeSource(request);
            if (duplicate == null || duplicate.getId() == provider.getId()) {
                if (isCreate) {
                    repository.create(provider);
                } else {
                    repository.update(provider.getId(), provider.getName(), provider.getCommission(), provider.isSelectable());
                }
                done.call();
            } else {
                error.postValue(R.string.persist_provider_error_duplicate_name_and_payment_method);
            }
        });
    }

    public void delete(int id) {
        LiveData<Integer> request = repository.delete(id);
        error.addSource(request, numberOfDeleted -> {
            error.removeSource(request);
            if (numberOfDeleted != null) {
                if (numberOfDeleted == 0) {
                    error.postValue(R.string.persist_provider_delete_error_has_associated_jobs);
                } else {
                    done.call();
                }
            }
        });
    }
}