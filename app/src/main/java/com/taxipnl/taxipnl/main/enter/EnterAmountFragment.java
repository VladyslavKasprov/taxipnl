package com.taxipnl.taxipnl.main.enter;

import com.taxipnl.taxipnl.R;

public abstract class EnterAmountFragment extends EnterNumberFragment {

    @Override
    protected boolean hasAdditionalNumberInput() {
        return false;
    }

    protected boolean isEnteredDataValid() {
        double amount = getMainNumber();
        if (amount <= 0) {
            displayMessage(R.string.persist_expense_error_zero_amount);
            return false;
        }

        return true;
    }
}