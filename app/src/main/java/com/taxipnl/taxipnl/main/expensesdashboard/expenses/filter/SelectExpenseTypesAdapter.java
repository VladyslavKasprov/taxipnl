package com.taxipnl.taxipnl.main.expensesdashboard.expenses.filter;

import android.support.annotation.NonNull;
import android.widget.Switch;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.db.entity.ExpenseType;

import java.util.HashSet;
import java.util.Set;

public class SelectExpenseTypesAdapter extends BaseAdapter<ExpenseType> {

    private Set<Integer> selectedItemsIds = new HashSet<>();

    public SelectExpenseTypesAdapter() {
        super(item -> R.layout.item_select_expense_types);
    }

    public Set<Integer> getSelectedItemsIds() {
        return selectedItemsIds;
    }

    public void setSelectedItemsIds(Set<Integer> providerIds) {
        this.selectedItemsIds = new HashSet<>(providerIds);
        notifyItemRangeChanged(0, items.size());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder<ExpenseType> viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
        ExpenseType expenseType = items.get(position);
        Switch selected = viewHolder.itemView.findViewById(R.id.sw_select_expense_types_selected);
        selected.setChecked(selectedItemsIds.contains(expenseType.getId()));
        selected.setOnCheckedChangeListener((v, isChecked) -> {
            if (isChecked) {
                selectedItemsIds.add(expenseType.getId());
            } else {
                selectedItemsIds.remove(expenseType.getId());
            }
        });
    }
}