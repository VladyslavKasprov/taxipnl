package com.taxipnl.taxipnl.main.expensesdashboard.expenses;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.db.pojo.ExpenseWithType;
import com.taxipnl.taxipnl.main.expensesdashboard.expenses.filter.ExpensesFilter;
import com.taxipnl.taxipnl.repository.ExpensesRepository;
import com.taxipnl.taxipnl.util.EconomicUtils;
import com.taxipnl.taxipnl.util.Safe;
import com.taxipnl.taxipnl.util.constants.SortingMode;

import org.threeten.bp.LocalDate;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

public class ExpensesFragmentVM extends ViewModel {

    private static final ExpensesFilter DEFAULT_FILTER = new ExpensesFilter(
            false, LocalDate.now(), LocalDate.now(),
            false, Collections.emptySet()
    );
    private static final int DEFAULT_SORTING_MODE = SortingMode.Expenses.BY_ENTERED_DATE_DESC;

    private ExpensesRepository expensesRepository;

    private LiveData<List<ExpenseWithType>> expenses;
    private LiveData<Double> expensesAmount;

    private MediatorLiveData<ExpensesFilter> filter = new MediatorLiveData<>();
    private MutableLiveData<Boolean> isPeriodFilterEnabled = new MutableLiveData<>();
    private MutableLiveData<LocalDate> startDate = new MutableLiveData<>();
    private MutableLiveData<LocalDate> endDate = new MutableLiveData<>();
    private MutableLiveData<Boolean> isExpenseTypesFilterEnabled = new MutableLiveData<>();
    private MutableLiveData<Set<Integer>> expenseTypesIds = new MutableLiveData<>();

    private MutableLiveData<Integer> sortingMode = new MutableLiveData<>();

    @Inject
    public ExpensesFragmentVM(ExpensesRepository expensesRepository) {
        this.expensesRepository = expensesRepository;

        MediatorLiveData<Void> loadExpensesTrigger = new MediatorLiveData<>();
        loadExpensesTrigger.addSource(filter, filter -> loadExpensesTrigger.setValue(null));
        loadExpensesTrigger.addSource(sortingMode, sortingMode -> loadExpensesTrigger.setValue(null));
        expenses = Transformations.switchMap(loadExpensesTrigger, aVoid ->
                loadExpenses(filter.getValue(), sortingMode.getValue()));
        expensesAmount = Transformations.map(expenses, EconomicUtils::getAmount);

        filter.addSource(filter, filter -> {
            if (filter != null) {
                isPeriodFilterEnabled.setValue(filter.isPeriodFilterEnabled);
                startDate.setValue(filter.startDate);
                endDate.setValue(filter.endDate);
                isExpenseTypesFilterEnabled.setValue(filter.isExpenseTypesFilterEnabled);
                expenseTypesIds.setValue(filter.expenseTypesIds);
            }
        });

        resetFilter();
        sortingMode.setValue(DEFAULT_SORTING_MODE);
    }

    public LiveData<List<ExpenseWithType>> getExpenses() {
        return expenses;
    }

    public LiveData<Double> getExpensesAmount() {
        return expensesAmount;
    }

    private LiveData<List<ExpenseWithType>> loadExpenses(ExpensesFilter filter, Integer sortingMode) {
        if (filter != null && sortingMode != null) {
            if (filter.isPeriodFilterEnabled && filter.isExpenseTypesFilterEnabled) {
                return expensesRepository.get(filter.startDate, filter.endDate, filter.expenseTypesIds, sortingMode);
            }
            if (filter.isPeriodFilterEnabled) {
                return expensesRepository.get(filter.startDate, filter.endDate, sortingMode);
            }
            if (filter.isExpenseTypesFilterEnabled) {
                return expensesRepository.get(filter.expenseTypesIds, sortingMode);
            }
            return expensesRepository.getAll(sortingMode);
        } else {
            return new MutableLiveData<>();
        }
    }


    public void applyFilter() {
        boolean isPeriodFilterEnabled = Safe.unbox(this.isPeriodFilterEnabled.getValue());
        LocalDate startDate = isPeriodFilterEnabled && this.startDate.getValue() != null ?
                this.startDate.getValue() : DEFAULT_FILTER.startDate;
        LocalDate endDate = isPeriodFilterEnabled && this.endDate.getValue() != null ?
                this.endDate.getValue() : DEFAULT_FILTER.endDate;
        boolean isExpenseTypesFilterEnabled = Safe.unbox(this.isExpenseTypesFilterEnabled.getValue());
        Set<Integer> providersIds = isExpenseTypesFilterEnabled && expenseTypesIds.getValue() != null ?
                expenseTypesIds.getValue() : DEFAULT_FILTER.expenseTypesIds;
        filter.setValue(new ExpensesFilter(
                isPeriodFilterEnabled, startDate, endDate,
                isExpenseTypesFilterEnabled, providersIds
        ));
    }

    public void resetFilter() {
        filter.setValue(DEFAULT_FILTER);
    }

    public void clearFilter() {
        filter.setValue(filter.getValue());
    }

    public MutableLiveData<Boolean> getIsPeriodFilterEnabled() {
        return isPeriodFilterEnabled;
    }

    public LiveData<LocalDate> getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        if (endDate.getValue() == null || endDate.getValue().isBefore(startDate)) {
            endDate.setValue(startDate);
        }
        this.startDate.setValue(startDate);
    }

    public LiveData<LocalDate> getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        if (startDate.getValue() == null || startDate.getValue().isAfter(endDate)) {
            startDate.setValue(endDate);
        }
        this.endDate.setValue(endDate);
    }

    public MutableLiveData<Boolean> getIsExpenseTypesFilterEnabled() {
        return isExpenseTypesFilterEnabled;
    }

    public LiveData<Set<Integer>> getExpenseTypesIds() {
        return expenseTypesIds;
    }

    public void setExpenseTypesIds(Set<Integer> ids) {
        expenseTypesIds.setValue(ids);
    }


    public LiveData<Integer> getSortingMode() {
        return sortingMode;
    }

    public void toggleSortByDateTime() {
        Integer currentMode = sortingMode.getValue();
        if (currentMode == null || currentMode == SortingMode.Expenses.BY_ENTERED_DATE_DESC) {
            sortingMode.setValue(SortingMode.Expenses.BY_ENTERED_DATE_ASC);
        } else {
            sortingMode.setValue(SortingMode.Expenses.BY_ENTERED_DATE_DESC);
        }
    }

    public void toggleSortByExpenseType() {
        Integer currentMode = sortingMode.getValue();
        if (currentMode == null || currentMode == SortingMode.Expenses.BY_EXPENSE_TYPE_ORDER_DESC) {
            sortingMode.setValue(SortingMode.Expenses.BY_EXPENSE_TYPE_ORDER_ASC);
        } else {
            sortingMode.setValue(SortingMode.Expenses.BY_EXPENSE_TYPE_ORDER_DESC);
        }
    }

    public void toggleSortByAmount() {
        Integer currentMode = sortingMode.getValue();
        if (currentMode == null || currentMode == SortingMode.Expenses.BY_AMOUNT_DESC) {
            sortingMode.setValue(SortingMode.Expenses.BY_AMOUNT_ASC);
        } else {
            sortingMode.setValue(SortingMode.Expenses.BY_AMOUNT_DESC);
        }
    }
}