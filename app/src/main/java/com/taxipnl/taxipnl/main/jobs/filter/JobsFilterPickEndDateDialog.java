package com.taxipnl.taxipnl.main.jobs.filter;

import org.threeten.bp.LocalDate;

public class JobsFilterPickEndDateDialog extends JobsFilterPickDateDialog {

    @Override
    protected LocalDate getInitiallySelectedDate() {
        LocalDate date = filterModel.getEndDate().getValue();
        return date == null ? LocalDate.now() : date;
    }

    @Override
    public void onDateSet(LocalDate date) {
        filterModel.setEndDate(date);
    }
}