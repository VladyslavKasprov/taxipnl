package com.taxipnl.taxipnl.main.dashboard.addjob;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.main.enter.EnterFareTollFragment;

public class AddJobEnterFareTollFragment extends EnterFareTollFragment {

    private int getProviderId() {
        return AddJobEnterFareTollFragmentArgs.fromBundle(getArguments()).getProviderId();
    }

    @Override
    protected void onValidEnter() {
        double fare = getMainNumber();
        double toll = getAdditionalNumber();
        int providerId = getProviderId();
        mainModel.addJob(fare, toll, providerId);
        findNavController().navigate(
                R.id.action_addJobEnterFareToll_to_dashboard
        );
    }
}