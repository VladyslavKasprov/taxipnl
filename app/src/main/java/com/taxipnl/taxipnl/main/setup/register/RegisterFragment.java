package com.taxipnl.taxipnl.main.setup.register;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.arrayadapter.BaseArrayAdapter;
import com.taxipnl.taxipnl.base.arrayadapter.TextDecorator;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentRegisterBinding;
import com.taxipnl.taxipnl.util.AndroidUtils;

import java.util.Currency;
import java.util.List;

@LayoutResId(R.layout.fragment_register)
public class RegisterFragment extends BaseFragment<FragmentRegisterBinding> {

    BaseArrayAdapter<Currency> adapter;

    RegisterVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getLocalViewModel(RegisterVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        setupCurrencySpinner();
        bind(BR.model, model);

        model.getCurrencies().observe(getViewLifecycleOwner(), this::updateCurrencies);
        model.onError().observe(getViewLifecycleOwner(), this::displayMessage);
    }

    @Override
    public void onPause() {
        AndroidUtils.hideSoftKeyboard(getActivity());
        super.onPause();
    }

    private void setupCurrencySpinner() {
        adapter = new BaseArrayAdapter<Currency>(getContext(), item -> item.getSymbol() + " " + item.getDisplayName())
                .setItemDecorator(new TextDecorator(getContext(), android.R.color.black, R.dimen.textsize_currency_spinner))
                .setDropDownItemDecorator(new TextDecorator(getContext(), android.R.color.black, R.dimen.textsize_currency_spinner));
        view.spinnerRegisterCurrency.setAdapter(adapter);
    }

    private void updateCurrencies(List<Currency> currencies) {
        if (currencies != null) {
            adapter.clear();
            adapter.addAll(currencies);
        }
    }
}