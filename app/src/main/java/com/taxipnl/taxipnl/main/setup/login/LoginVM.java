package com.taxipnl.taxipnl.main.setup.login;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.SingleLiveEvent;
import com.taxipnl.taxipnl.repository.UserRepository;

import javax.inject.Inject;

public class LoginVM extends ViewModel {

    private UserRepository repository;

    public MutableLiveData<String> password = new MutableLiveData<>();

    private SingleLiveEvent<Integer> error = new SingleLiveEvent<>();

    @Inject
    public LoginVM(UserRepository repository) {
        this.repository = repository;
    }

    public LiveData<Integer> onError() {
        return error;
    }

    public void login() {
        String password = this.password.getValue();
        if (password == null || password.isEmpty()) {
            error.setValue(R.string.login_error_empty_password);
            return;
        }

        LiveData<String> request = repository.getPassword();
        error.addSource(request, userPassword -> {
            if (userPassword != null) {
                error.removeSource(request);
                if (!password.equals(userPassword)) {
                    error.setValue(R.string.login_error_incorrect_password);
                } else {
                    repository.login();
                }
            }
        });
    }
}