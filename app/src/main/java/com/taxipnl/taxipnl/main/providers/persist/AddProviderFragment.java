package com.taxipnl.taxipnl.main.providers.persist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.util.AndroidUtils;

public class AddProviderFragment extends PersistProviderFragment {

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        view.btnPersistProviderConfirm.setText(R.string.all_create);
    }

    @Override
    public void onResume() {
        super.onResume();
        AndroidUtils.showSoftKeyboard(getActivity(), view.etPersistProviderName);
    }

    @Override
    protected void persist(Provider provider) {
        model.create(provider);
    }

    @Override
    protected boolean getIsSelectable() {
        return true;
    }
}