package com.taxipnl.taxipnl.main.expensesdashboard.expenses.filter;

import org.threeten.bp.LocalDate;

public class ExpensesFilterPickStartDateDialog extends ExpensesFilterPickDateDialog {

    @Override
    protected LocalDate getInitiallySelectedDate() {
        LocalDate date = filterModel.getStartDate().getValue();
        return date == null ? LocalDate.now() : date;
    }

    @Override
    public void onDateSet(LocalDate date) {
        filterModel.setStartDate(date);
    }
}