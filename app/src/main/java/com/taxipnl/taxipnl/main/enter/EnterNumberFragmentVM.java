package com.taxipnl.taxipnl.main.enter;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class EnterNumberFragmentVM extends ViewModel {

    private MutableLiveData<Double> mainNumber = new MutableLiveData<>();
    private MutableLiveData<Double> additionalNumber = new MutableLiveData<>();
    private boolean isMainSelected = true;
    private boolean isAlwaysInteger = false;
    private boolean isClearOnFirstEnterAfterSwitch = false;
    private boolean isClearOnFirstEnter = false;

    public LiveData<Double> getMainNumber() {
        return mainNumber;
    }

    public void setMainNumber(double value) {
        mainNumber.setValue(value);
    }

    public LiveData<Double> getAdditionalNumber() {
        return additionalNumber;
    }

    public void setAdditionalNumber(double value) {
        additionalNumber.setValue(value);
    }

    public void selectMain() {
        isMainSelected = true;
        if (isClearOnFirstEnterAfterSwitch) {
            isClearOnFirstEnter = true;
        }
        setZeroIfNull(mainNumber);
    }

    public void selectAdditional() {
        isMainSelected = false;
        if (isClearOnFirstEnterAfterSwitch) {
            isClearOnFirstEnter = true;
        }
        setZeroIfNull(additionalNumber);
    }

    public void setAlwaysInteger() {
        isAlwaysInteger = true;
    }

    public void setClearOnFirstEnterAfterSwitch() {
        isClearOnFirstEnterAfterSwitch = true;
        isClearOnFirstEnter = true;
    }

    public void addDigitToCurrentlySelected(double digit) {
        if (isClearOnFirstEnter) {
            isClearOnFirstEnter = false;
            clearCurrentlySelected();
        }

        if (isAlwaysInteger) {
            setCurrentlySelectedNumber(getCurrentlySelectedNumber() * 10 + digit);
        } else {
            setCurrentlySelectedNumber(getCurrentlySelectedNumber() * 10 + digit / 100);
        }
    }

    public void increaseOrderOfCurrentlySelected() {
        if (isClearOnFirstEnter) {
            isClearOnFirstEnter = false;
            clearCurrentlySelected();
        }
        setCurrentlySelectedNumber(getCurrentlySelectedNumber() * 100);
    }

    public void clearCurrentlySelected() {
        setCurrentlySelectedNumber(0);
    }

    private double getCurrentlySelectedNumber() {
        if (isMainSelected) {
            setZeroIfNull(mainNumber);
            return mainNumber.getValue();
        } else {
            setZeroIfNull(additionalNumber);
            return additionalNumber.getValue();
        }
    }

    private void setZeroIfNull(MutableLiveData<Double> number) {
        if (number.getValue() == null) {
            number.setValue(0d);
        }
    }

    private void setCurrentlySelectedNumber(double number) {
        if (isMainSelected) {
            mainNumber.setValue(number);
        } else {
            additionalNumber.setValue(number);
        }
    }
}