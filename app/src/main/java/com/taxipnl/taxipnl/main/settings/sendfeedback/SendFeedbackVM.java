package com.taxipnl.taxipnl.main.settings.sendfeedback;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.SingleLiveEvent;
import com.taxipnl.taxipnl.network.TaxipnlService;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendFeedbackVM extends ViewModel {

    private TaxipnlService service;

    public MutableLiveData<String> feedback = new MutableLiveData<>();

    private SingleLiveEvent<Void> done = new SingleLiveEvent<>();
    private SingleLiveEvent<Integer> error = new SingleLiveEvent<>();

    @Inject
    public SendFeedbackVM(TaxipnlService service) {
        this.service = service;
    }

    public LiveData<Void> onDone() {
        return done;
    }

    public LiveData<Integer> onError() {
        return error;
    }

    public void sendFeedback() {
        String feedback = this.feedback.getValue() != null ? this.feedback.getValue().trim() : null;
        if (feedback == null || feedback.isEmpty()) {
            error.setValue(R.string.send_feedback_error_empty_feedback);
        }

        service.sendFeedback(feedback).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {}

            @Override
            public void onFailure(Call<Void> call, Throwable t) {}
        });
        done.call();
    }
}