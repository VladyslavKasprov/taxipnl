package com.taxipnl.taxipnl.main.expensesdashboard.expenses.editexpense;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.SingleLiveEvent;
import com.taxipnl.taxipnl.db.pojo.ExpenseWithType;
import com.taxipnl.taxipnl.repository.ExpenseTypesRepository;
import com.taxipnl.taxipnl.repository.ExpensesRepository;

import javax.inject.Inject;

public class EditExpenseFragmentVM extends ViewModel {

    private ExpensesRepository expensesRepository;

    private MutableLiveData<Integer> expenseId = new MutableLiveData<>();
    private MutableLiveData<Integer> expenseTypeId = new MutableLiveData<>();
    private MediatorLiveData<ExpenseWithType> expense = new MediatorLiveData<>();
    private boolean hasBeenEdited = false;

    private SingleLiveEvent<Void> done = new SingleLiveEvent<>();

    @Inject
    public EditExpenseFragmentVM(ExpenseTypesRepository expenseTypesRepository, ExpensesRepository expensesRepository) {
        this.expensesRepository = expensesRepository;

        expense.addSource(
                Transformations.switchMap(expenseId, expensesRepository::get),
                expense::setValue
        );
        expense.addSource(
                Transformations.switchMap(expenseTypeId, expenseTypesRepository::get),
                expenseType -> {
                    ExpenseWithType expense = this.expense.getValue();
                    if (expenseType != null && expense != null) {
                        hasBeenEdited = true;
                        expense.expenseTypeName = expenseType.getName();
                        this.expense.postValue(expense);
                    }
                }
        );
    }

    public void loadExpense(Integer id) {
        expenseId.setValue(id);
    }

    public LiveData<ExpenseWithType> getExpense() {
        return expense;
    }

    public LiveData<Void> onDone() {
        return done;
    }

    public void setAmount(double amount) {
        ExpenseWithType expense = this.expense.getValue();
        if (expense != null) {
            hasBeenEdited = true;
            expense.amount = amount;
            this.expense.setValue(expense);
        }
    }

    public void setExpenseTypeId(int id) {
        expenseTypeId.setValue(id);
    }

    public void save() {
        ExpenseWithType expense = this.expense.getValue();
        if (hasBeenEdited && expense != null) {
            if (expenseTypeId.getValue() != null) {
                expensesRepository.update(expense.id, expense.amount, expenseTypeId.getValue());
            } else {
                expensesRepository.update(expense.id, expense.amount);
            }
        }
        done.call();
    }

    public void delete() {
        if (expenseId.getValue() != null) {
            expensesRepository.delete(expenseId.getValue());
            done.call();
        }
    }

    public void clear() {
        expenseId.setValue(null);
        expenseTypeId.setValue(null);
        expense.setValue(null);
        hasBeenEdited = false;
    }
}