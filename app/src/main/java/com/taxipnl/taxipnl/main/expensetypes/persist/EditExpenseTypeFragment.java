package com.taxipnl.taxipnl.main.expensetypes.persist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.config.MenuResId;

@MenuResId(R.menu.edit)
public class EditExpenseTypeFragment extends PersistExpenseTypeFragment {

    private int getExpenseTypeId() {
        return EditExpenseTypeFragmentArgs.fromBundle(getArguments()).getExpenseTypeId();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model.loadExpenseType(getExpenseTypeId());
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        view.btnPersistExpenseTypePersist.setText(R.string.all_update);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_delete) {
            model.delete();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}