package com.taxipnl.taxipnl.main.expensesdashboard.expenses.editexpense;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.db.pojo.ExpenseWithType;
import com.taxipnl.taxipnl.main.enter.EnterAmountFragment;

public class EditExpenseEnterAmountFragment extends EnterAmountFragment {

    private EditExpenseFragmentVM editExpenseModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        editExpenseModel = getGlobalViewModel(EditExpenseFragmentVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model.setClearOnFirstEnterAfterSwitch();

        ExpenseWithType expense = editExpenseModel.getExpense().getValue();
        if (expense != null) {
            model.setMainNumber(expense.amount);
        }
    }

    @Override
    protected void onValidEnter() {
        editExpenseModel.setAmount(getMainNumber());
        findNavController().navigateUp();
    }
}