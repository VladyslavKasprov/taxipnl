package com.taxipnl.taxipnl.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.repository.ProvidersRepository;

import java.util.List;

import javax.inject.Inject;

public class ProvidersVM extends ViewModel {

    private ProvidersRepository repository;

    private LiveData<List<Provider>> allProvidersExceptStreet;
    private LiveData<List<Provider>> allSelectableProviders;

    @Inject
    public ProvidersVM(ProvidersRepository repository) {
        this.repository = repository;

        allProvidersExceptStreet = repository.getAllExceptStreet();
        allSelectableProviders = repository.getAllSelectable();
    }

    public LiveData<List<Provider>> getAllProvidersExceptStreet() {
        return allProvidersExceptStreet;
    }

    public LiveData<List<Provider>> getAllSelectableProviders() {
        return allSelectableProviders;
    }

    public void updateProvidersOrder(List<Provider> providers) {
        int orderNumber = 1;
        for (Provider provider : providers) {
            provider.setOrderNumber(orderNumber);
            orderNumber++;
        }
        update(providers);
    }

    private void update(List<Provider> providers) {
        repository.update(providers);
    }
}