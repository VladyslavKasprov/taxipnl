package com.taxipnl.taxipnl.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.db.entity.ExpenseType;
import com.taxipnl.taxipnl.repository.ExpenseTypesRepository;

import java.util.List;

import javax.inject.Inject;

public class ExpenseTypesVM extends ViewModel {

    private ExpenseTypesRepository repository;

    private LiveData<List<ExpenseType>> allExpenseTypes;
    private LiveData<List<ExpenseType>> allDispensableExpenseTypes;
    private LiveData<List<ExpenseType>> allDispensableExpenseTypesPlusToll;

    @Inject
    public ExpenseTypesVM(ExpenseTypesRepository repository) {
        this.repository = repository;

        allExpenseTypes = repository.getAll();
        allDispensableExpenseTypes = repository.getAllDispensable();
        allDispensableExpenseTypesPlusToll = repository.getAllDispensablePlusToll();
    }

    public LiveData<List<ExpenseType>> getAll() {
        return allExpenseTypes;
    }

    public LiveData<List<ExpenseType>> getAllDispensableExpenseTypes() {
        return allDispensableExpenseTypes;
    }

    public LiveData<List<ExpenseType>> getAllDispensableExpenseTypesPlusToll() {
        return allDispensableExpenseTypesPlusToll;
    }

    public void updateExpenseTypesOrder(List<ExpenseType> expenseTypes) {
        int orderNumber = 1;
        for (ExpenseType expenseType : expenseTypes) {
            expenseType.setOrderNumber(orderNumber);
            orderNumber++;
        }
        update(expenseTypes);
    }

    private void update(List<ExpenseType> expenseTypes) {
        repository.update(expenseTypes);
    }
}