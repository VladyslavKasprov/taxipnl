package com.taxipnl.taxipnl.main.dashboard.addjob;

import com.taxipnl.taxipnl.main.dashboard.SelectProviderFragment;

public class AddJobSelectProviderFragment extends SelectProviderFragment {

    @Override
    protected void onProviderSelected(int id) {
        findNavController().navigate(
                AddJobSelectProviderFragmentDirections.actionAddJobSelectProviderToAddJobEnterFareToll(id)
        );
    }
}