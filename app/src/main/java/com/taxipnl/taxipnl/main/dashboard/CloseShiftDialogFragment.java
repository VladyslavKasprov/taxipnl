package com.taxipnl.taxipnl.main.dashboard;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseDialogFragment;
import com.taxipnl.taxipnl.main.MainActivityVM;

public class CloseShiftDialogFragment extends BaseDialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        viewModel = getGlobalViewModel(MainActivityVM.class);
        setCancelable(false);
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.close_shift)
                .setMessage(R.string.close_shift_message)
                .setPositiveButton(R.string.close_shift_positive, (dialog, which) -> viewModel.closeShift())
                .setNegativeButton(R.string.close_shift_negative, (dialog, which) -> dialog.dismiss())
                .create();
    }
}