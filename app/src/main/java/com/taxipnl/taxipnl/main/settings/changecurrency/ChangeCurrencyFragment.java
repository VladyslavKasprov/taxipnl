package com.taxipnl.taxipnl.main.settings.changecurrency;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentChangeCurrencyBinding;
import com.taxipnl.taxipnl.view.DividerItemDecoration;

import java.util.Currency;

import static com.taxipnl.taxipnl.util.constants.Constants.CURRENCIES;

@LayoutResId(R.layout.fragment_change_currency)
public class ChangeCurrencyFragment extends BaseFragment<FragmentChangeCurrencyBinding> {

    BaseAdapter<Currency> adapter;

    ChangeCurrencyVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getLocalViewModel(ChangeCurrencyVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupCurrencyList();

        adapter.setItems(CURRENCIES);
    }

    private void setupCurrencyList() {
        adapter = new BaseAdapter<Currency>(item -> R.layout.item_currency)
                .setHandler(currency -> {
                    model.setCurrency(currency);
                    findNavController().navigateUp();
                });
        view.rvChangeCurrency.setAdapter(adapter);
        view.rvChangeCurrency.addItemDecoration(new DividerItemDecoration(getContext()));
    }
}