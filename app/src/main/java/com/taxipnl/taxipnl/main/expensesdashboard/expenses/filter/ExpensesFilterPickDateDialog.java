package com.taxipnl.taxipnl.main.expensesdashboard.expenses.filter;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.main.PickDateDialog;
import com.taxipnl.taxipnl.main.expensesdashboard.expenses.ExpensesFragmentVM;

public abstract class ExpensesFilterPickDateDialog extends PickDateDialog {

    ExpensesFragmentVM filterModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filterModel = getGlobalViewModel(ExpensesFragmentVM.class);
    }
}