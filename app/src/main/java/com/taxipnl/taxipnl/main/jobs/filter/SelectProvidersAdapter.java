package com.taxipnl.taxipnl.main.jobs.filter;

import android.support.annotation.NonNull;
import android.widget.Switch;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.db.entity.Provider;

import java.util.HashSet;
import java.util.Set;

public class SelectProvidersAdapter extends BaseAdapter<Provider> {

    private Set<Integer> selectedProvidersIds = new HashSet<>();

    public SelectProvidersAdapter() {
        super(item -> R.layout.item_select_providers);
    }

    public Set<Integer> getSelectedProvidersIds() {
        return selectedProvidersIds;
    }

    public void setSelectedProvidersIds(Set<Integer> providerIds) {
        this.selectedProvidersIds = new HashSet<>(providerIds);
        notifyItemRangeChanged(0, items.size());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder<Provider> viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
        Provider provider = items.get(position);
        Switch selected = viewHolder.itemView.findViewById(R.id.sw_select_providers_selected);
        selected.setChecked(selectedProvidersIds.contains(provider.getId()));
        selected.setOnCheckedChangeListener((v, isChecked) -> {
            if (isChecked) {
                selectedProvidersIds.add(provider.getId());
            } else {
                selectedProvidersIds.remove(provider.getId());
            }
        });
    }
}