package com.taxipnl.taxipnl.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuConfig;
import com.taxipnl.taxipnl.databinding.FragmentHomeBinding;
import com.taxipnl.taxipnl.util.FormatUtils;

import org.threeten.bp.LocalDateTime;

@LayoutResId(R.layout.fragment_home)
@MenuConfig(inheritsParentsMenu = true)
public class HomeFragment extends BaseFragment<FragmentHomeBinding> {

    private BroadcastReceiver everyMinute = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_TIME_TICK.equals(intent.getAction())) {
                updateDateAndTime();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.model, mainModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateDateAndTime();
        getContext().registerReceiver(everyMinute, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().unregisterReceiver(everyMinute);
    }

    private void updateDateAndTime() {
        view.tvHomeTime.setText(FormatUtils.formatAsTime(LocalDateTime.now()));
        view.tvHomeDate.setText(FormatUtils.formatAsLongDate(LocalDateTime.now()));
    }
}