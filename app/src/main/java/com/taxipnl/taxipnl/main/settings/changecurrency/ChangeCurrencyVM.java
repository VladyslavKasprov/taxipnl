package com.taxipnl.taxipnl.main.settings.changecurrency;

import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.repository.UserRepository;
import com.taxipnl.taxipnl.util.FormatUtils;

import java.util.Currency;

import javax.inject.Inject;

public class ChangeCurrencyVM extends ViewModel {

    private UserRepository repository;

    @Inject
    public ChangeCurrencyVM(UserRepository repository) {
        this.repository = repository;
    }

    public void setCurrency(Currency currency) {
        repository.setCurrency(currency);
        FormatUtils.updateCurrency(currency);
    }
}