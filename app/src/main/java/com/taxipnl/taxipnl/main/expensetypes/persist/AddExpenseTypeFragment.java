package com.taxipnl.taxipnl.main.expensetypes.persist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.util.AndroidUtils;

public class AddExpenseTypeFragment extends PersistExpenseTypeFragment {

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        view.btnPersistExpenseTypePersist.setText(R.string.all_create);
    }

    @Override
    public void onResume() {
        super.onResume();
        AndroidUtils.showSoftKeyboard(getActivity(), view.etPersistExpenseTypeName);
    }
}