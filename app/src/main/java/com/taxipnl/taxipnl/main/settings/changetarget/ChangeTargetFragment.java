package com.taxipnl.taxipnl.main.settings.changetarget;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.main.enter.EnterNumberFragment;
import com.taxipnl.taxipnl.main.settings.SettingsVM;
import com.taxipnl.taxipnl.util.FormatUtils;

public class ChangeTargetFragment extends EnterNumberFragment {

    private ChangeTargetVM changeTargetModel;
    private SettingsVM settingsModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeTargetModel = getLocalViewModel(ChangeTargetVM.class);
        settingsModel = getGlobalViewModel(SettingsVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model.setAlwaysInteger();
        model.setClearOnFirstEnterAfterSwitch();

        settingsModel.getCurrentTarget().observe(getViewLifecycleOwner(), target -> {
            if (target != null) {
                model.setMainNumber(target);
            }
        });
    }

    @Override
    protected boolean hasAdditionalNumberInput() {
        return false;
    }

    @Override
    public String formatMainNumber(double number) {
        return FormatUtils.formatAsInteger(getContext(), number);
    }

    @Override
    protected boolean isEnteredDataValid() {
        double target = getMainNumber();
        if (target <= 0) {
            displayMessage(R.string.persist_target_error_zero_target);
            return false;
        }

        return true;
    }

    @Override
    protected void onValidEnter() {
        changeTargetModel.setCurrentTarget(getMainNumber());
        findNavController().navigateUp();
    }
}