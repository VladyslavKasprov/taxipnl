package com.taxipnl.taxipnl.main;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentStartBinding;

@LayoutResId(R.layout.fragment_start)
public class StartFragment extends BaseFragment<FragmentStartBinding> {}