package com.taxipnl.taxipnl.main.reports;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentReportsFilterBinding;

import java.util.Arrays;

@LayoutResId(R.layout.fragment_reports_filter)
public class ReportsFilterFragment extends BaseFragment<FragmentReportsFilterBinding> {

    private ReportsVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getGlobalViewModel(ReportsVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.model, model);
        setupQuickDurationsList();

        view.hitboxReportsFilterStartDate.setOnClickListener(v -> showPickStartDateDialog());
        view.hitboxReportsFilterEndDate.setOnClickListener(v -> showPickEndDateDialog());
        view.btnReportsFilterApply.setOnClickListener(v -> {
            model.applyFilter();
            findNavController().navigateUp();
        });
    }

    private void setupQuickDurationsList() {
        BaseAdapter<QuickDuration> adapter = new BaseAdapter<QuickDuration>(item -> R.layout.item_quick_duration)
                .setModel(model, getViewLifecycleOwner())
                .setHandler(item -> {
                    model.selectQuickDuration(item);
                    findNavController().navigateUp();
                });
        adapter.setItems(Arrays.asList(QuickDuration.values()));
        view.rvReportsFilterQuickDurations.setAdapter(adapter);
    }

    private void showPickStartDateDialog() {
        new ReportsFilterPickStartDateDialog().showNow(getChildFragmentManager(), "");
    }

    private void showPickEndDateDialog() {
        new ReportsFilterPickEndDateDialog().showNow(getChildFragmentManager(), "");
    }
}