package com.taxipnl.taxipnl.main.jobs.editjob;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.main.dashboard.SelectProviderFragment;

public class EditJobSelectProviderFragment extends SelectProviderFragment {

    private EditJobFragmentVM editJobModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        editJobModel = getGlobalViewModel(EditJobFragmentVM.class);
    }

    @Override
    protected void onProviderSelected(int id) {
        editJobModel.setProviderId(id);
        findNavController().navigateUp();
    }
}