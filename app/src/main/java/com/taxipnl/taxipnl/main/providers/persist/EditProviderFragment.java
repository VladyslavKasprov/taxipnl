package com.taxipnl.taxipnl.main.providers.persist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.config.MenuResId;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.util.FormatUtils;

@MenuResId(R.menu.edit)
public class EditProviderFragment extends PersistProviderFragment {

    private int getProviderId() {
        return EditProviderFragmentArgs.fromBundle(getArguments()).getProviderId();
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        view.btnPersistProviderConfirm.setText(R.string.all_update);
        view.spinnerPersistProviderPaymentMethod.setEnabled(false);
        view.groupPersistProviderIsSelectable.setVisibility(View.VISIBLE);

        model.get(getProviderId()).observe(getViewLifecycleOwner(), provider -> {
            if (provider != null) {
                view.etPersistProviderName.setText(provider.getName());
                view.etPersistProviderCommission.setText(FormatUtils.formatAsDecimal(getContext(), provider.getCommission()));
                view.spinnerPersistProviderPaymentMethod.setSelection(adapter.getPosition(provider.getPaymentMethod()));
                view.swPersistProviderIsSelectable.setChecked(provider.isSelectable());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_delete) {
            model.delete(getProviderId());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void persist(Provider provider) {
        model.update(provider);
    }

    @Override
    protected Provider getProvider() {
        Provider provider = super.getProvider();
        provider.setId(getProviderId());
        return provider;
    }

    @Override
    protected boolean getIsSelectable() {
        return view.swPersistProviderIsSelectable.isChecked();
    }
}