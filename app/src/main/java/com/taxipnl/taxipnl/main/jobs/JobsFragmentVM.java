package com.taxipnl.taxipnl.main.jobs;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.db.pojo.JobWithProvider;
import com.taxipnl.taxipnl.main.jobs.filter.JobsFilter;
import com.taxipnl.taxipnl.repository.JobsRepository;
import com.taxipnl.taxipnl.util.EconomicUtils;
import com.taxipnl.taxipnl.util.Safe;
import com.taxipnl.taxipnl.util.constants.SortingMode;

import org.threeten.bp.LocalDate;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

public class JobsFragmentVM extends ViewModel {

    private static final JobsFilter DEFAULT_FILTER = new JobsFilter(
            false, LocalDate.now(), LocalDate.now(),
            false, Collections.emptySet()
    );
    private static final int DEFAULT_SORTING_MODE = SortingMode.Jobs.BY_ENTERED_DATE_DESC;

    private JobsRepository jobsRepository;

    private LiveData<List<JobWithProvider>> jobs;
    private LiveData<Double> jobsRevenue;
    private LiveData<Double> jobsIncomePlusToll;

    private MediatorLiveData<JobsFilter> filter = new MediatorLiveData<>();
    private MutableLiveData<Boolean> isPeriodFilterEnabled = new MutableLiveData<>();
    private MutableLiveData<LocalDate> startDate = new MutableLiveData<>();
    private MutableLiveData<LocalDate> endDate = new MutableLiveData<>();
    private MutableLiveData<Boolean> isProvidersFilterEnabled = new MutableLiveData<>();
    private MutableLiveData<Set<Integer>> providersIds = new MutableLiveData<>();

    private MutableLiveData<Integer> sortingMode = new MutableLiveData<>();

    @Inject
    public JobsFragmentVM(JobsRepository jobsRepository) {
        this.jobsRepository = jobsRepository;

        MediatorLiveData<Void> loadJobsTrigger = new MediatorLiveData<>();
        loadJobsTrigger.addSource(filter, filter -> loadJobsTrigger.setValue(null));
        loadJobsTrigger.addSource(sortingMode, sortingMode -> loadJobsTrigger.setValue(null));
        jobs = Transformations.switchMap(loadJobsTrigger, aVoid ->
                loadJobs(filter.getValue(), sortingMode.getValue()));
        jobsRevenue = Transformations.map(jobs, EconomicUtils::getRevenue);
        jobsIncomePlusToll = Transformations.map(jobs, EconomicUtils::getIncomePlusToll);

        filter.addSource(filter, filter -> {
            if (filter != null) {
                isPeriodFilterEnabled.setValue(filter.isPeriodFilterEnabled);
                startDate.setValue(filter.startDate);
                endDate.setValue(filter.endDate);
                isProvidersFilterEnabled.setValue(filter.isProvidersFilterEnabled);
                providersIds.setValue(filter.providersIds);
            }
        });

        resetFilter();
        sortingMode.setValue(DEFAULT_SORTING_MODE);
    }

    public LiveData<List<JobWithProvider>> getJobs() {
        return jobs;
    }

    public LiveData<Double> getJobsRevenue() {
        return jobsRevenue;
    }

    public LiveData<Double> getJobsIncomePlusToll() {
        return jobsIncomePlusToll;
    }

    private LiveData<List<JobWithProvider>> loadJobs(JobsFilter filter, Integer sortingMode) {
        if (filter != null && sortingMode != null) {
            if (filter.isPeriodFilterEnabled && filter.isProvidersFilterEnabled) {
                return jobsRepository.get(filter.startDate, filter.endDate, filter.providersIds, sortingMode);
            }
            if (filter.isPeriodFilterEnabled) {
                return jobsRepository.get(filter.startDate, filter.endDate, sortingMode);
            }
            if (filter.isProvidersFilterEnabled) {
                return jobsRepository.get(filter.providersIds, sortingMode);
            }
            return jobsRepository.getAll(sortingMode);
        } else {
            return new MutableLiveData<>();
        }
    }


    public void applyFilter() {
        boolean isPeriodFilterEnabled = Safe.unbox(this.isPeriodFilterEnabled.getValue());
        LocalDate startDate = isPeriodFilterEnabled && this.startDate.getValue() != null ?
                this.startDate.getValue() : DEFAULT_FILTER.startDate;
        LocalDate endDate = isPeriodFilterEnabled && this.endDate.getValue() != null ?
                this.endDate.getValue() : DEFAULT_FILTER.endDate;
        boolean isProvidersFilterEnabled = Safe.unbox(this.isProvidersFilterEnabled.getValue());
        Set<Integer> providersIds = isProvidersFilterEnabled && this.providersIds.getValue() != null ?
                this.providersIds.getValue() : DEFAULT_FILTER.providersIds;
        filter.setValue(new JobsFilter(
                isPeriodFilterEnabled, startDate, endDate,
                isProvidersFilterEnabled, providersIds
        ));
    }

    public void resetFilter() {
        filter.setValue(DEFAULT_FILTER);
    }

    public void clearFilter() {
        filter.setValue(filter.getValue());
    }

    public MutableLiveData<Boolean> getIsPeriodFilterEnabled() {
        return isPeriodFilterEnabled;
    }

    public MutableLiveData<LocalDate> getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        if (endDate.getValue() == null || endDate.getValue().isBefore(startDate)) {
            endDate.setValue(startDate);
        }
        this.startDate.setValue(startDate);
    }

    public MutableLiveData<LocalDate> getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        if (startDate.getValue() == null || startDate.getValue().isAfter(endDate)) {
            startDate.setValue(endDate);
        }
        this.endDate.setValue(endDate);
    }

    public MutableLiveData<Boolean> getIsProvidersFilterEnabled() {
        return isProvidersFilterEnabled;
    }

    public LiveData<Set<Integer>> getProvidersIds() {
        return providersIds;
    }

    public void setProvidersIds(Set<Integer> ids) {
        providersIds.setValue(ids);
    }


    public LiveData<Integer> getSortingMode() {
        return sortingMode;
    }

    public void toggleSortByDateTime() {
        Integer currentMode = sortingMode.getValue();
        if (currentMode == null || currentMode == SortingMode.Jobs.BY_ENTERED_DATE_DESC) {
            sortingMode.setValue(SortingMode.Jobs.BY_ENTERED_DATE_ASC);
        } else {
            sortingMode.setValue(SortingMode.Jobs.BY_ENTERED_DATE_DESC);
        }
    }

    public void toggleSortByProvider() {
        Integer currentMode = sortingMode.getValue();
        if (currentMode == null || currentMode == SortingMode.Jobs.BY_PROVIDER_ORDER_DESC) {
            sortingMode.setValue(SortingMode.Jobs.BY_PROVIDER_ORDER_ASC);
        } else {
            sortingMode.setValue(SortingMode.Jobs.BY_PROVIDER_ORDER_DESC);
        }
    }

    public void toggleSortByFare() {
        Integer currentMode = sortingMode.getValue();
        if (currentMode == null || currentMode == SortingMode.Jobs.BY_FARE_DESC) {
            sortingMode.setValue(SortingMode.Jobs.BY_FARE_ASC);
        } else {
            sortingMode.setValue(SortingMode.Jobs.BY_FARE_DESC);
        }
    }
}