package com.taxipnl.taxipnl.main.dashboard;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentSelectProviderBinding;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.main.ProvidersVM;
import com.taxipnl.taxipnl.util.constants.Constants;

@LayoutResId(R.layout.fragment_select_provider)
public abstract class SelectProviderFragment extends BaseFragment<FragmentSelectProviderBinding> {

    BaseAdapter<Provider> adapter;

    private ProvidersVM providersModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        providersModel = getGlobalViewModel(ProvidersVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        setupProvidersView();

        providersModel.getAllSelectableProviders().observe(getViewLifecycleOwner(), providers -> {
            if (providers != null) {
                adapter.setItems(providers);
            }
        });

        view.hitboxSelectProviderViewMore.setOnClickListener(this::toggleStreetShowProviders);
        view.hitboxSelectProviderViewLess.setOnClickListener(this::toggleStreetShowProviders);
        view.hitboxSelectProviderCash.setOnClickListener(v -> onCashProviderSelected());
        view.hitboxSelectProviderCard.setOnClickListener(v -> onCardProviderSelected());
    }

    protected abstract void onProviderSelected(int id);

    private void setupProvidersView() {
        adapter = new BaseAdapter<Provider>(item -> R.layout.item_select_provider)
                .setHandler(provider -> onProviderSelected(provider.getId()));
        view.rvSelectProviderProviders.setAdapter(adapter);
    }

    protected void toggleStreetShowProviders(View _view) {
        boolean showLess = _view.getId() == R.id.hitbox_select_provider_view_more;
        view.groupSelectProviderViewMore.setVisibility(showLess ? View.GONE : View.VISIBLE);
        view.groupSelectProviderViewLess.setVisibility(showLess ? View.VISIBLE : View.GONE);
    }

    protected void onCashProviderSelected() {
        onProviderSelected(Constants.PROVIDER_ID_STREET_CASH);
    }

    protected void onCardProviderSelected() {
        onProviderSelected(Constants.PROVIDER_ID_STREET_CARD);
    }
}