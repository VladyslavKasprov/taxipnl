package com.taxipnl.taxipnl.main.reports;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.db.entity.Shift;
import com.taxipnl.taxipnl.db.pojo.ExpenseTypeWithAmount;
import com.taxipnl.taxipnl.db.pojo.JobWithProvider;
import com.taxipnl.taxipnl.main.items.ProviderWithIncome;
import com.taxipnl.taxipnl.repository.ExpensesRepository;
import com.taxipnl.taxipnl.repository.JobsRepository;
import com.taxipnl.taxipnl.repository.ProvidersRepository;
import com.taxipnl.taxipnl.repository.ShiftsRepository;
import com.taxipnl.taxipnl.repository.TipsRepository;
import com.taxipnl.taxipnl.util.DateUtils;
import com.taxipnl.taxipnl.util.DurationUtils;
import com.taxipnl.taxipnl.util.EconomicUtils;
import com.taxipnl.taxipnl.util.Safe;
import com.taxipnl.taxipnl.util.ShiftUtils;

import org.threeten.bp.LocalDate;

import java.util.List;

import javax.inject.Inject;

public class ReportsVM extends ViewModel {

    private static final QuickDuration DEFAULT_QUICK_DURATION = QuickDuration.YESTERDAY;

    private LiveData<Shift> lastClosedShift;
    private LiveData<List<Shift>> shifts;
    private MediatorLiveData<Long> shiftsDuration = new MediatorLiveData<>();
    private LiveData<List<JobWithProvider>> shiftsJobs;
    private LiveData<Integer> shiftsJobsCount;
    private LiveData<Double> shiftsRevenue;
    private LiveData<Double> shiftsIncome;
    private MediatorLiveData<Double> shiftsIncomePerJob = new MediatorLiveData<>();
    private MediatorLiveData<Double> shiftsIncomePerHour = new MediatorLiveData<>();
    private LiveData<Double> shiftsNonStreetCashIncome;
    private LiveData<Double> shiftsStreetCashIncome;
    private LiveData<Double> shiftsCashIncome;
    private LiveData<List<Provider>> allProviders;
    private MediatorLiveData<List<ProviderWithIncome>> shiftsIncomeByProvider = new MediatorLiveData<>();
    private LiveData<Double> tipsAmount;

    private LiveData<Double> expensesAmount;
    private LiveData<List<ExpenseTypeWithAmount>> expenseAmountByType;

    private MediatorLiveData<ReportsFilter> filter = new MediatorLiveData<>();
    private MutableLiveData<LocalDate> startDate = new MutableLiveData<>();
    private MutableLiveData<LocalDate> endDate = new MutableLiveData<>();
    private MutableLiveData<Integer> daysAmount = new MutableLiveData<>();
    private MutableLiveData<QuickDuration> selectedQuickDuration = new MutableLiveData<>();

    @Inject
    public ReportsVM(ShiftsRepository shiftsRepository, ProvidersRepository providersRepository,
                     JobsRepository jobsRepository, ExpensesRepository expensesRepository,
                     TipsRepository tipsRepository) {

        lastClosedShift = shiftsRepository.getLastClosedShift();
        filter.addSource(lastClosedShift, shift -> {});

        filter.addSource(filter, this::updateDisplayedFilter);

        shifts = Transformations.switchMap(filter, filter -> {
            if (filter == null) {
                return new MutableLiveData<>();
            }
            if (QuickDuration.LATEST_SHIFT.equals(selectedQuickDuration.getValue())) {
                return Transformations.map(lastClosedShift, Safe::singletonList);
            }
            return shiftsRepository.getShiftsStartedInPeriod(filter.startDate, filter.endDate);
        });

        shiftsDuration.addSource(shifts, shifts ->
                shiftsDuration.setValue(ShiftUtils.getShiftsDuration(shifts)));

        shiftsJobs = Transformations.switchMap(shifts, jobsRepository::getShiftsJobs);

        shiftsJobsCount = Transformations.map(shiftsJobs, Safe::size);

        shiftsRevenue = Transformations.map(shiftsJobs, EconomicUtils::getRevenue);
        shiftsIncome = Transformations.map(shiftsJobs, EconomicUtils::getIncome);

        shiftsIncomePerJob.addSource(shiftsIncome, shiftsIncome ->
                updateShiftsIncomePerJob(shiftsIncome, shiftsJobsCount.getValue()));
        shiftsIncomePerJob.addSource(shiftsJobsCount, shiftsJobsCount ->
                updateShiftsIncomePerJob(shiftsIncome.getValue(), shiftsJobsCount));

        shiftsIncomePerHour.addSource(shiftsIncome, shiftsIncome ->
                updateShiftsIncomePerHour(shiftsIncome, shiftsDuration.getValue()));
        shiftsIncomePerHour.addSource(shiftsDuration, shiftsDuration ->
                updateShiftsIncomePerHour(shiftsIncome.getValue(), shiftsDuration));

        shiftsNonStreetCashIncome = Transformations.map(shiftsJobs, EconomicUtils::getNonStreetCashIncome);

        shiftsStreetCashIncome = Transformations.map(shiftsJobs, EconomicUtils::getStreetCashIncome);

        shiftsCashIncome = Transformations.map(shiftsJobs, EconomicUtils::getCashIncome);

        allProviders = providersRepository.getAll();
        shiftsIncomeByProvider.addSource(allProviders, allProviders ->
                updateShiftsIncomeByProvider(allProviders, shiftsJobs.getValue()));
        shiftsIncomeByProvider.addSource(shiftsJobs, shiftsJobs ->
                updateShiftsIncomeByProvider(allProviders.getValue(), shiftsJobs));

        tipsAmount = Transformations.switchMap(filter, filter ->
                tipsRepository.getTipsAmount(filter.startDate, filter.endDate));


        expensesAmount = Transformations.switchMap(filter, filter ->
                expensesRepository.getAmount(filter.startDate, filter.endDate));

        expenseAmountByType = Transformations.switchMap(filter, filter ->
                expensesRepository.getExpenseAmountByType(filter.startDate, filter.endDate));
    }

    public LiveData<Long> getShiftsDuration() {
        return shiftsDuration;
    }

    public LiveData<Integer> getShiftsJobsCount() {
        return shiftsJobsCount;
    }

    public LiveData<Double> getShiftsRevenue() {
        return shiftsRevenue;
    }

    public LiveData<Double> getShiftsIncome() {
        return shiftsIncome;
    }

    public LiveData<Double> getShiftsIncomePerJob() {
        return shiftsIncomePerJob;
    }

    public LiveData<Double> getShiftsIncomePerHour() {
        return shiftsIncomePerHour;
    }

    public LiveData<Double> getShiftsNonStreetCashIncome() {
        return shiftsNonStreetCashIncome;
    }

    public LiveData<Double> getShiftsStreetCashIncome() {
        return shiftsStreetCashIncome;
    }

    public LiveData<Double> getShiftsCashIncome() {
        return shiftsCashIncome;
    }

    public LiveData<List<ProviderWithIncome>> getShiftsIncomeByProvider() {
        return shiftsIncomeByProvider;
    }

    public LiveData<Double> getTipsAmount() {
        return tipsAmount;
    }

    private void updateShiftsIncomePerJob(Double income, Integer jobsCount) {
        if (income != null && jobsCount != null) {
            double incomePerJob = jobsCount > 0 ? income / jobsCount : 0;
            shiftsIncomePerJob.setValue(incomePerJob);
        }
    }

    private void updateShiftsIncomePerHour(Double income, Long durationInSeconds) {
        if (income != null && durationInSeconds != null) {
            double durationInHours = (durationInSeconds + 1) / (double) 3600;
            shiftsIncomePerHour.setValue(income / durationInHours);
        }
    }

    private void updateShiftsIncomeByProvider(List<Provider> providers, List<JobWithProvider> jobs) {
        if (providers != null && jobs != null) {
            shiftsIncomeByProvider.setValue(EconomicUtils.getIncomeByProvider(providers, jobs));
        }
    }

    public LiveData<List<ExpenseTypeWithAmount>> getExpenseAmountByType() {
        return expenseAmountByType;
    }

    public LiveData<Double> getExpensesAmount() {
        return expensesAmount;
    }


    public void setDefaultFilter() {
        selectQuickDuration(DEFAULT_QUICK_DURATION);
    }

    private void applyFilter(@NonNull LocalDate startDate, @NonNull LocalDate endDate,
                             QuickDuration quickDuration) {
        filter.setValue(new ReportsFilter(startDate, endDate));
        selectedQuickDuration.setValue(quickDuration);
    }

    public void applyFilter() {
        if (startDate.getValue() != null && endDate.getValue() != null) {
            applyFilter(startDate.getValue(), endDate.getValue(), null);
        } else {
            setDefaultFilter();
        }
    }

    public void selectQuickDuration(QuickDuration quickDuration) {
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = LocalDate.now();
        switch (quickDuration) {
            case LATEST_SHIFT:
                if (lastClosedShift.getValue() != null) {
                    Shift lastClosedShift = this.lastClosedShift.getValue();
                    if (lastClosedShift != null) {
                        startDate = lastClosedShift.getStartDateTime().toLocalDate();
                        if (lastClosedShift.getEndDateTime() != null) {
                            endDate = lastClosedShift.getEndDateTime().toLocalDate();
                        }
                    }
                }
                break;
            case YESTERDAY:
                startDate = DateUtils.getYesterday();
                endDate = startDate;
                break;
            case THIS_WEEK:
                startDate = DateUtils.getStartOfCurrentWeek();
                break;
            case PREVIOUS_WEEK:
                startDate = DateUtils.getStartOfPreviousWeek();
                endDate = startDate.plusDays(6);
                break;
            case THIS_WEEK_LAST_YEAR:
                startDate = DateUtils.getStartOfCurrentWeekLastYear();
                endDate = startDate.plusDays(6);
                break;
            case THIS_MONTH:
                startDate = DateUtils.getStartOfCurrentMonth();
                break;
            case THIS_YEAR:
                startDate = DateUtils.getStartOfCurrentYear();
                break;
            default:
                setDefaultFilter();
                return;
        }
        applyFilter(startDate, endDate, quickDuration);
    }

    public LiveData<LocalDate> getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        if (endDate.getValue() == null || endDate.getValue().isBefore(startDate)) {
            endDate.setValue(startDate);
        }
        this.startDate.setValue(startDate);
    }

    public LiveData<LocalDate> getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        if (startDate.getValue() == null || startDate.getValue().isAfter(endDate)) {
            startDate.setValue(endDate);
        }
        this.endDate.setValue(endDate);
    }

    public LiveData<Integer> getDaysAmount() {
        return daysAmount;
    }

    public LiveData<QuickDuration> getSelectedQuickDuration() {
        return selectedQuickDuration;
    }

    private void updateDisplayedFilter(ReportsFilter filter) {
        if (filter != null) {
            startDate.setValue(filter.startDate);
            endDate.setValue(filter.endDate);
            daysAmount.setValue(DurationUtils.getNumberOfDaysBetween(filter.startDate, filter.endDate));
        }
    }
}