package com.taxipnl.taxipnl.main.reports;

import org.threeten.bp.LocalDate;

public class ReportsFilterPickEndDateDialog extends ReportsFilterPickDateDialog {

    @Override
    protected LocalDate getInitiallySelectedDate() {
        LocalDate date = filterModel.getEndDate().getValue();
        return date == null ? LocalDate.now() : date;
    }

    @Override
    public void onDateSet(LocalDate date) {
        filterModel.setEndDate(date);
    }
}