package com.taxipnl.taxipnl.main.reports;

import android.support.annotation.StringRes;

import com.taxipnl.taxipnl.R;

public enum QuickDuration {

    LATEST_SHIFT(R.string.quick_duration_latest_shift),
    YESTERDAY(R.string.quick_duration_yesterday),
    THIS_WEEK(R.string.quick_duration_this_week),
    PREVIOUS_WEEK(R.string.quick_duration_previous_week),
    THIS_WEEK_LAST_YEAR(R.string.quick_duration_this_week_last_year),
    THIS_MONTH(R.string.quick_duration_this_month),
    THIS_YEAR(R.string.quick_duration_this_year);

    @StringRes
    private int nameResId;

    QuickDuration(int nameResId) {
        this.nameResId = nameResId;
    }

    public int getNameResId() {
        return nameResId;
    }
}