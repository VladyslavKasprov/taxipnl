package com.taxipnl.taxipnl.main.enter;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.util.constants.Constants;

public abstract class EnterFareTollFragment extends EnterNumberFragment {

    protected boolean isEnteredDataValid() {
        double fare = getMainNumber();
        if (fare <= 0) {
            displayMessage(R.string.persist_job_error_zero_fare);
            return false;
        }
        if (fare >= Constants.MAX_FARE) {
            displayMessage(R.string.persist_job_error_big_fare);
            return false;
        }

        double toll = getAdditionalNumber();
        if (toll >= fare) {
            displayMessage(R.string.persist_job_error_toll_higher_than_fare);
            return false;
        }
        return true;
    }
}