package com.taxipnl.taxipnl.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.repository.JobsRepository;
import com.taxipnl.taxipnl.repository.ShiftsRepository;
import com.taxipnl.taxipnl.util.Safe;

import org.threeten.bp.LocalDate;

import javax.inject.Inject;

public class JobsVM extends ViewModel {

    private LiveData<Double> sameDayLastYearIncome;
    private LiveData<Double> sameDayLastWeekIncome;

    @Inject
    public JobsVM(ShiftsRepository shiftsRepository, JobsRepository jobsRepository) {
        LiveData<LocalDate> date = Transformations.map(shiftsRepository.getLatestShift(), Safe::getStartDate);
        sameDayLastWeekIncome = Transformations.switchMap(date, jobsRepository::getSameDayLastWeekIncome);
        sameDayLastYearIncome = Transformations.switchMap(date, jobsRepository::getSameDayOfSameWeekLastYearIncome);
    }

    public LiveData<Double> getSameDayLastWeekIncome() {
        return sameDayLastWeekIncome;
    }

    public LiveData<Double> getSameDayLastYearIncome() {
        return sameDayLastYearIncome;
    }
}