package com.taxipnl.taxipnl.main.reports;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.LinearLayout;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.adapter.decorator.VerticalSpanDecorator;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuConfig;
import com.taxipnl.taxipnl.databinding.FragmentIncomeReportBinding;
import com.taxipnl.taxipnl.main.items.ProviderWithIncome;
import com.taxipnl.taxipnl.view.CenteredDividerItemDecoration;

import java.util.List;

@LayoutResId(R.layout.fragment_income_report)
@MenuConfig(inheritsParentsMenu = true)
public class IncomeReportFragment extends BaseFragment<FragmentIncomeReportBinding> {

    private final static int HORIZONTAL_SPAN_COUNT = 3;
    private final static int VERTICAL_SPAN_COUNT = 3;
    private final static int[] REPORT_HEADERS = new int[] {
            R.layout.item_income_report_shifts_jobs_count, R.layout.item_income_report_shifts_duration, R.layout.item_income_report_tips,
            R.layout.item_income_report_shifts_income_per_job, R.layout.item_income_report_shifts_income_per_hour, R.layout.item_income_report_shifts_revenue,
            R.layout.item_income_report_shifts_non_street_cash_income, R.layout.item_income_report_shifts_street_cash_income, R.layout.item_income_report_shifts_street_cash_income
    };

    private BaseAdapter<ProviderWithIncome> adapter;

    private ReportsVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getGlobalViewModel(ReportsVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupReportView();
        bind(BR.model, model);

        model.getShiftsIncomeByProvider().observe(getViewLifecycleOwner(), this::updateShiftsIncomeByProvider);
    }

    private void setupReportView() {
        adapter = new BaseAdapter<ProviderWithIncome>(item -> R.layout.item_provider_with_income)
                .setHeaders(REPORT_HEADERS, model, getViewLifecycleOwner())
                .setDecorator(new VerticalSpanDecorator(VERTICAL_SPAN_COUNT));
        view.rvIncomeReport.setAdapter(adapter);
        view.rvIncomeReport.setLayoutManager(new GridLayoutManager(getContext(), HORIZONTAL_SPAN_COUNT));
        view.rvIncomeReport.addItemDecoration(
                new CenteredDividerItemDecoration(getContext(), LinearLayout.HORIZONTAL, false, R.dimen.spacing_12)
        );
        view.rvIncomeReport.addItemDecoration(
                new CenteredDividerItemDecoration(getContext(), LinearLayout.VERTICAL, false, R.dimen.spacing_12)
        );
        view.rvIncomeReport.setItemAnimator(null);
    }

    private void updateShiftsIncomeByProvider(List<ProviderWithIncome> items) {
        if (items != null) {
            adapter.setItems(items);
            view.rvIncomeReport.scrollToPosition(0);
        }
    }
}