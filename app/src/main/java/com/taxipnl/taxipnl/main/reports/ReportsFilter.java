package com.taxipnl.taxipnl.main.reports;

import android.support.annotation.NonNull;

import org.threeten.bp.LocalDate;

public class ReportsFilter {

    @NonNull
    public LocalDate startDate;

    @NonNull
    public LocalDate endDate;

    public ReportsFilter(@NonNull LocalDate startDate, @NonNull LocalDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
