package com.taxipnl.taxipnl.main.expensesdashboard.expenses.filter;

import org.threeten.bp.LocalDate;

public class ExpensesFilterPickEndDateDialog extends ExpensesFilterPickDateDialog {

    @Override
    protected LocalDate getInitiallySelectedDate() {
        LocalDate date = filterModel.getEndDate().getValue();
        return date == null ? LocalDate.now() : date;
    }

    @Override
    protected void onDateSet(LocalDate date) {
        filterModel.setEndDate(date);
    }
}