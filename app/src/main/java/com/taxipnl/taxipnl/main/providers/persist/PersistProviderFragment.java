package com.taxipnl.taxipnl.main.providers.persist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.arrayadapter.BaseArrayAdapter;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentPersistProviderBinding;
import com.taxipnl.taxipnl.db.entity.PaymentMethod;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.util.AndroidUtils;
import com.taxipnl.taxipnl.util.constants.Constants;

import java.util.List;

@LayoutResId(R.layout.fragment_persist_provider)
public abstract class PersistProviderFragment extends BaseFragment<FragmentPersistProviderBinding> {

    protected BaseArrayAdapter<PaymentMethod> adapter;

    protected PersistProviderFragmentVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getLocalViewModel(PersistProviderFragmentVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        setupPaymentMethodsSpinner();

        model.getPaymentMethods().observe(getViewLifecycleOwner(), this::updatePaymentMethods);
        model.onDone().observe(getViewLifecycleOwner(), onDone -> findNavController().popBackStack());
        model.onError().observe(getViewLifecycleOwner(), errorStringResId -> {
            if (errorStringResId != null) {
                displayMessage(errorStringResId);
                view.btnPersistProviderConfirm.setEnabled(true);
            }
        });

        view.btnPersistProviderConfirm.setOnClickListener(this::persist);
    }

    @Override
    public void onPause() {
        AndroidUtils.hideSoftKeyboard(getActivity());
        super.onPause();
    }

    private void setupPaymentMethodsSpinner() {
        adapter = new BaseArrayAdapter<>(getContext(), item -> item.getName(getContext()));
        view.spinnerPersistProviderPaymentMethod.setAdapter(adapter);
    }

    private void updatePaymentMethods(List<PaymentMethod> paymentMethods) {
        if (paymentMethods != null) {
            adapter.clear();
            adapter.addAll(paymentMethods);
        }
    }

    protected void persist(View view) {
        view.setEnabled(false);
        if (isEnteredDataValid()) {
            persist(getProvider());
        } else {
            view.setEnabled(true);
        }
    }

    protected boolean isEnteredDataValid() {
        String name = getEnteredName();
        if (name.isEmpty()) {
            displayMessage(R.string.persist_provider_error_empty_name);
            return false;
        }

        Double commission = getEnteredCommission();
        if (commission == null) {
            displayMessage(R.string.persist_provider_error_malformed_commission);
            return false;
        }
        if (commission < 0 || commission >= Constants.MAX_COMMISSION) {
            displayMessage(R.string.persist_provider_error_commission_higher_than_max);
            return false;
        }

        PaymentMethod paymentMethod = getPaymentMethod();
        if (paymentMethod == null) {
            displayMessage(R.string.persist_provider_error_empty_payment_method);
            return false;
        }
        return true;
    }

    protected String getEnteredName() {
        return view.etPersistProviderName.getText().toString().trim();
    }

    protected Double getEnteredCommission() {
        try {
            return Double.valueOf(view.etPersistProviderCommission.getText().toString().trim());
        } catch (NumberFormatException ignored) {}
        return null;
    }

    protected PaymentMethod getPaymentMethod() {
        return (PaymentMethod) view.spinnerPersistProviderPaymentMethod.getSelectedItem();
    }

    protected abstract boolean getIsSelectable();

    protected abstract void persist(Provider provider);

    protected Provider getProvider() {
        String name = getEnteredName();
        double commission = getEnteredCommission();
        PaymentMethod paymentMethod = getPaymentMethod();
        boolean isSelectable = getIsSelectable();
        return new Provider(name, commission, paymentMethod, isSelectable);
    }
}