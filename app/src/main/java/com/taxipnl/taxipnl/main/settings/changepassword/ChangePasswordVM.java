package com.taxipnl.taxipnl.main.settings.changepassword;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.SingleLiveEvent;
import com.taxipnl.taxipnl.repository.UserRepository;

import javax.inject.Inject;

public class ChangePasswordVM extends ViewModel {

    private UserRepository repository;

    public MutableLiveData<String> currentPassword = new MutableLiveData<>();
    public MutableLiveData<String> newPassword = new MutableLiveData<>();
    public MutableLiveData<String> newPasswordRepeated = new MutableLiveData<>();

    private SingleLiveEvent<Void> done = new SingleLiveEvent<>();
    private SingleLiveEvent<Integer> error = new SingleLiveEvent<>();

    @Inject
    public ChangePasswordVM(UserRepository repository) {
        this.repository = repository;
    }

    public LiveData<Void> onDone() {
        return done;
    }

    public LiveData<Integer> onError() {
        return error;
    }

    public void changePassword() {
        LiveData<String> request = repository.getPassword();
        error.addSource(request, userPassword -> {
            if (userPassword == null) {
                return;
            }
            error.removeSource(request);

            if (!userPassword.equals(currentPassword.getValue())) {
                error.setValue(R.string.change_password_error_incorrect_password);
                return;
            }

            String newPassword = this.newPassword.getValue();
            if (newPassword == null || newPassword.isEmpty()) {
                error.setValue(R.string.change_password_error_new_password_empty);
                return;
            }

            if (!newPassword.equals(newPasswordRepeated.getValue())) {
                error.setValue(R.string.change_password_error_new_passwords_dont_match);
                return;
            }

            repository.setPassword(newPassword);
            done.call();
        });
    }
}