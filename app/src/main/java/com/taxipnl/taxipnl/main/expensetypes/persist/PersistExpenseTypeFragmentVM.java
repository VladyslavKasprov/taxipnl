package com.taxipnl.taxipnl.main.expensetypes.persist;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.SingleLiveEvent;
import com.taxipnl.taxipnl.db.entity.ExpenseType;
import com.taxipnl.taxipnl.repository.ExpenseTypesRepository;

import javax.inject.Inject;

public class PersistExpenseTypeFragmentVM extends ViewModel {

    private ExpenseTypesRepository repository;

    private MutableLiveData<Integer> expenseTypeId = new MutableLiveData<>();
    public MediatorLiveData<String> name = new MediatorLiveData<>();

    private SingleLiveEvent<Void> done = new SingleLiveEvent<>();
    private SingleLiveEvent<Integer> error = new SingleLiveEvent<>();

    @Inject
    public PersistExpenseTypeFragmentVM(ExpenseTypesRepository repository) {
        this.repository = repository;

        name.addSource(
                Transformations.switchMap(expenseTypeId, repository::get),
                expenseType -> {
                    if (expenseType != null) {
                        name.setValue(expenseType.getName());
                    }
                }
        );
    }

    public void loadExpenseType(int id) {
        expenseTypeId.setValue(id);
    }

    public LiveData<Void> onDone() {
        return done;
    }

    public LiveData<Integer> onError() {
        return error;
    }

    public void persist() {
        String name = this.name.getValue() != null ? this.name.getValue().trim() : null;
        if (name == null || name.isEmpty()) {
            error.setValue(R.string.persist_expense_type_error_empty_name);
            return;
        }

        ExpenseType expenseType = new ExpenseType(name);
        if (expenseTypeId.getValue() != null) {
            expenseType.setId(expenseTypeId.getValue());
        }
        checkDuplicateAndPersist(expenseType);
    }

    private void checkDuplicateAndPersist(ExpenseType expenseType) {
        LiveData<ExpenseType> request = repository.get(expenseType.getName());
        error.addSource(request, duplicate -> {
            error.removeSource(request);

            if (duplicate != null && duplicate.getId() != expenseType.getId()) {
                error.postValue(R.string.persist_expense_type_error_duplicate_name);
                return;
            }

            persist(expenseType);
        });
    }

    private void persist(ExpenseType expenseType) {
        if (expenseTypeId.getValue() == null) {
            repository.create(expenseType);
        } else {
            repository.update(expenseTypeId.getValue(), expenseType.getName());
        }
        done.call();
    }

    public void delete() {
        if (expenseTypeId.getValue() == null) {
            return;
        }
        LiveData<Integer> request = repository.delete(expenseTypeId.getValue());
        error.addSource(request, numberOfDeleted -> {
            error.removeSource(request);
            if (numberOfDeleted == null || numberOfDeleted == 0) {
                error.postValue(R.string.persist_expense_type_delete_error_has_associated_expenses);
            } else {
                done.call();
            }
        });
    }
}