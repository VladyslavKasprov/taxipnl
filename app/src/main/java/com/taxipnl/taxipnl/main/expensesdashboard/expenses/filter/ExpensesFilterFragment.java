package com.taxipnl.taxipnl.main.expensesdashboard.expenses.filter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentExpensesFilterBinding;
import com.taxipnl.taxipnl.db.entity.ExpenseType;
import com.taxipnl.taxipnl.main.ExpenseTypesVM;
import com.taxipnl.taxipnl.main.expensesdashboard.expenses.ExpensesFragmentVM;
import com.taxipnl.taxipnl.view.DividerItemDecoration;

import java.util.List;
import java.util.Set;

@LayoutResId(R.layout.fragment_expenses_filter)
public class ExpensesFilterFragment extends BaseFragment<FragmentExpensesFilterBinding> {

    SelectExpenseTypesAdapter adapter;

    ExpensesFragmentVM model;
    ExpenseTypesVM expenseTypesModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getGlobalViewModel(ExpensesFragmentVM.class);
        expenseTypesModel = getGlobalViewModel(ExpenseTypesVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        setupExpenseTypesView();
        bind(BR.model, model);

        expenseTypesModel.getAll().observe(getViewLifecycleOwner(), this::updateExpenseTypes);
        model.getExpenseTypesIds().observe(getViewLifecycleOwner(), this::updateSelectedProviders);

        view.hitboxExpensesFilterStartDate.setOnClickListener(v -> showPickStartDateDialog());
        view.hitboxExpensesFilterEndDate.setOnClickListener(v -> showPickEndDateDialog());
        view.btnExpensesFilterApply.setOnClickListener(v -> applyFilter());
        view.btnExpensesFilterReset.setOnClickListener(v -> resetFilter());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        model.clearFilter();
    }

    private void setupExpenseTypesView(){
        adapter = new SelectExpenseTypesAdapter();
        view.rvExpensesFilterExpenseTypes.setAdapter(adapter);
        view.rvExpensesFilterExpenseTypes.addItemDecoration(new DividerItemDecoration(getContext()));
    }

    private void updateExpenseTypes(List<ExpenseType> expenseTypes) {
        if (expenseTypes != null) {
            adapter.setItems(expenseTypes);
        }
    }

    private void updateSelectedProviders(Set<Integer> providersIds) {
        if (providersIds != null) {
            adapter.setSelectedItemsIds(providersIds);
        }
    }

    protected void showPickStartDateDialog() {
        new ExpensesFilterPickStartDateDialog().showNow(getChildFragmentManager(), "");
    }

    protected void showPickEndDateDialog() {
        new ExpensesFilterPickEndDateDialog().showNow(getChildFragmentManager(), "");
    }

    protected void applyFilter() {
        model.setExpenseTypesIds(adapter.getSelectedItemsIds());
        model.applyFilter();
        findNavController().navigateUp();
    }

    protected void resetFilter() {
        model.resetFilter();
        findNavController().navigateUp();
    }
}