package com.taxipnl.taxipnl.main.setup.subscibe;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseDialogFragment;

public class BillingErrorResponseDialogFragment extends BaseDialogFragment {

    SubscribeVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getLocalViewModelOf(getTargetFragment(), SubscribeVM.class);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BillingErrorResponse response = model.onError().getValue();
        if (response == null) {
            throw new IllegalArgumentException("Cannot create dialog for null BillingErrorResponse");
        }

        setCancelable(false);
        return new AlertDialog.Builder(requireContext())
                .setTitle(response.titleResId)
                .setMessage(response.messageResId)
                .setNeutralButton(R.string.billing_error_response_neutral, (dialog, which) -> {})
                .create();
    }
}