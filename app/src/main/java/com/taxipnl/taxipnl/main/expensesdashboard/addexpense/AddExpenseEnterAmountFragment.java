package com.taxipnl.taxipnl.main.expensesdashboard.addexpense;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.main.ExpensesVM;
import com.taxipnl.taxipnl.main.enter.EnterAmountFragment;

public class AddExpenseEnterAmountFragment extends EnterAmountFragment {

    ExpensesVM expensesModel;

    private int getExpenseTypeId() {
        return AddExpenseEnterAmountFragmentArgs.fromBundle(getArguments()).getExpenseTypeId();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        expensesModel = getGlobalViewModel(ExpensesVM.class);
    }

    @Override
    protected void onValidEnter() {
        double amount = getMainNumber();
        int expenseTypeId = getExpenseTypeId();
        expensesModel.create(amount, expenseTypeId);
        findNavController().navigateUp();
    }
}