package com.taxipnl.taxipnl.main.jobs.editjob;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.db.pojo.JobWithProvider;
import com.taxipnl.taxipnl.main.enter.EnterFareTollFragment;

public class EditJobEnterFareTollFragment extends EnterFareTollFragment {

    private EditJobFragmentVM editJobModel;

    private boolean getIsTollSelected() {
        return EditJobEnterFareTollFragmentArgs.fromBundle(getArguments()).getIsTollSelected();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        editJobModel = getGlobalViewModel(EditJobFragmentVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model.setClearOnFirstEnterAfterSwitch();
        if (getIsTollSelected()) {
            model.selectAdditional();
        }

        JobWithProvider job = editJobModel.getJob().getValue();
        if (job != null) {
            model.setMainNumber(job.fare);
            if (job.toll != 0) {
                model.setAdditionalNumber(job.toll);
            }
        }
    }

    @Override
    protected void onValidEnter() {
        editJobModel.setFareToll(getMainNumber(), getAdditionalNumber());
        findNavController().navigateUp();
    }
}