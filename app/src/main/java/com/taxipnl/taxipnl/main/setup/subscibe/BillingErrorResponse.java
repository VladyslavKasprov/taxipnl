package com.taxipnl.taxipnl.main.setup.subscibe;

import android.support.annotation.StringRes;
import android.util.SparseArray;

import com.android.billingclient.api.BillingClient.BillingResponse;
import com.taxipnl.taxipnl.R;

import java.io.Serializable;

public class BillingErrorResponse implements Serializable {

    private static final SparseArray<BillingErrorResponse> ERROR_RESPONSES = new SparseArray<>();
    static {
        ERROR_RESPONSES.append(BillingResponse.FEATURE_NOT_SUPPORTED,
                new BillingErrorResponse(R.string.billing_error_response_feature_not_supported_title, R.string.billing_error_response_feature_not_supported_message));
        ERROR_RESPONSES.append(BillingResponse.SERVICE_DISCONNECTED,
                new BillingErrorResponse(R.string.billing_error_response_service_disconnected_title, R.string.billing_error_response_service_disconnected_message));
        ERROR_RESPONSES.append(BillingResponse.USER_CANCELED,
                new BillingErrorResponse(R.string.billing_error_response_user_cancelled_title, R.string.billing_error_response_user_cancelled_message));
        ERROR_RESPONSES.append(BillingResponse.SERVICE_UNAVAILABLE,
                new BillingErrorResponse(R.string.billing_error_response_service_unavailable_title, R.string.billing_error_response_service_unavailable_message));
        ERROR_RESPONSES.append(BillingResponse.BILLING_UNAVAILABLE,
                new BillingErrorResponse(R.string.billing_error_response_billing_unavailable_title, R.string.billing_error_response_billing_unavailable_message));
        ERROR_RESPONSES.append(BillingResponse.DEVELOPER_ERROR,
                new BillingErrorResponse(R.string.billing_error_response_developer_error_title, R.string.billing_error_response_developer_error_message));
        ERROR_RESPONSES.append(BillingResponse.ERROR,
                new BillingErrorResponse(R.string.billing_error_response_error_title, R.string.billing_error_response_error_message));
    }

    @StringRes public final int titleResId;
    @StringRes public final int messageResId;

    private BillingErrorResponse(@StringRes int titleResId, @StringRes int messageResId) {
        this.titleResId = titleResId;
        this.messageResId = messageResId;
    }

    public static BillingErrorResponse forCode(@BillingResponse int responseCode) {
        return ERROR_RESPONSES.get(responseCode);
    }
}