package com.taxipnl.taxipnl.main.expensetypes.persist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentPersistExpenseTypeBinding;
import com.taxipnl.taxipnl.util.AndroidUtils;

@LayoutResId(R.layout.fragment_persist_expense_type)
public abstract class PersistExpenseTypeFragment extends BaseFragment<FragmentPersistExpenseTypeBinding> {

    protected PersistExpenseTypeFragmentVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getLocalViewModel(PersistExpenseTypeFragmentVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.model, model);

        model.onDone().observe(getViewLifecycleOwner(), onDone -> findNavController().popBackStack());
        // TODO disable controls (persist and delete buttons) in onLoading and enable in onError
        model.onError().observe(getViewLifecycleOwner(), this::displayMessage);
    }

    @Override
    public void onPause() {
        AndroidUtils.hideSoftKeyboard(getActivity());
        super.onPause();
    }
}