package com.taxipnl.taxipnl.main.items;

import com.taxipnl.taxipnl.db.entity.PaymentMethod;
import com.taxipnl.taxipnl.db.entity.Provider;

public class ProviderWithIncome {

    public String name;
    public PaymentMethod paymentMethod;
    public double income;

    public ProviderWithIncome(Provider provider, double income) {
        this.name = provider.getName();
        this.paymentMethod = provider.getPaymentMethod();
        this.income = income;
    }
}