package com.taxipnl.taxipnl.main;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.widget.Toolbar;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseActivity;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuResId;
import com.taxipnl.taxipnl.base.config.NavHostFragmentId;
import com.taxipnl.taxipnl.base.config.ThemeResId;
import com.taxipnl.taxipnl.main.settings.SettingsVM;
import com.taxipnl.taxipnl.util.FormatUtils;

import androidx.navigation.NavController;
import androidx.navigation.NavDestination;

import static com.taxipnl.taxipnl.util.NavigationUtils.matchDestinations;
import static com.taxipnl.taxipnl.util.NavigationUtils.popToTopLevelDestination;
import static com.taxipnl.taxipnl.util.NavigationUtils.setupToolbarWithNavController;
import static com.taxipnl.taxipnl.util.NavigationUtils.switchNavGraphIfPossible;
import static com.taxipnl.taxipnl.util.constants.Constants.SETUP_GRAPHS;
import static com.taxipnl.taxipnl.util.constants.Constants.TOP_LEVEL_DESTINATIONS;
import static com.taxipnl.taxipnl.util.constants.Constants.USER_INACTIVITY_TIMEOUT;

@ThemeResId(R.style.AppTheme)
@LayoutResId(R.layout.activity_main)
@MenuResId(R.menu.standard)
@NavHostFragmentId(R.id.nav_host_fragment_main)
public class MainActivity extends BaseActivity {

    CountDownTimer navigateToStartCountDownTimer = new CountDownTimer(USER_INACTIVITY_TIMEOUT, USER_INACTIVITY_TIMEOUT) {
        @Override
        public void onTick(long millisUntilFinished) {}

        @Override
        public void onFinish() {
            model.navigateToStart();
        }
    };

    NavController.OnDestinationChangedListener hideToolbarOnSetupGraphs = (controller, destination, args) -> new Handler().post(() -> {
        if (matchDestinations(destination, SETUP_GRAPHS)) {
            getSupportActionBar().hide();
        } else {
            getSupportActionBar().show();
        }
    });

    MainActivityVM model;
    SettingsVM settingsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureNavigation();

        model = getViewModel(MainActivityVM.class);
        model.getNavigateTo().observe(this, this::navigateTo);
        model.onNavigateToStart().observe(this, onCall ->
                popToTopLevelDestination(findNavController(), TOP_LEVEL_DESTINATIONS));

        settingsModel = getViewModel(SettingsVM.class);
        settingsModel.getCurrency().observe(this, currency -> {
            if (currency != null) {
                FormatUtils.updateCurrency(currency); // required to set currency on startup
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigateToStartCountDownTimer.start();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        navigateToStartCountDownTimer.cancel();
        navigateToStartCountDownTimer.start();
    }

    private void configureNavigation() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        NavController navController = findNavController();
        setupToolbarWithNavController(toolbar, navController, TOP_LEVEL_DESTINATIONS);
        navController.addOnDestinationChangedListener(hideToolbarOnSetupGraphs);
    }

    private void navigateTo(Integer destinationId) {
        if (destinationId == null) {
            return;
        }

        // TODO remove workaround
        NavDestination currentDestination = findNavController().getCurrentDestination();
        int currentNavGraphId = currentDestination.getParent().getId();
        int currentDestinationId = currentDestination.getId();
        if ((destinationId == R.id.shiftGraph || destinationId == R.id.homeGraph)
                && currentNavGraphId == R.id.mainGraph && currentDestinationId != R.id.start) {
            return;
        }

        new Handler().post(() -> switchNavGraphIfPossible(findNavController(), destinationId));
    }
}