package com.taxipnl.taxipnl.main.dashboard;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentDashboardExtraBinding;
import com.taxipnl.taxipnl.db.pojo.JobWithProvider;
import com.taxipnl.taxipnl.main.JobsVM;
import com.taxipnl.taxipnl.view.DividerItemDecoration;

import java.util.List;

@LayoutResId(R.layout.fragment_dashboard_extra)
public class DashboardExtraFragment extends BaseFragment<FragmentDashboardExtraBinding> {

    BaseAdapter<JobWithProvider> adapter;

    JobsVM jobsModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        jobsModel = getGlobalViewModel(JobsVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.model, mainModel);
        bind(BR.jobsModel, jobsModel);
        setupJobsView();

        mainModel.getLatestShiftJobs().observe(getViewLifecycleOwner(), this::updateJobs);

        view.btnDashboardExtraCloseShift.setOnClickListener(v -> showCloseShiftDialog());
    }

    private void setupJobsView() {
        adapter = new BaseAdapter<JobWithProvider>(item -> R.layout.item_recent_job)
                .setHandler(job -> findNavController().navigate(
                        DashboardExtraFragmentDirections.actionDashboardExtraToEditJob(job.id)
                ));
        view.rvDashboardExtraJobs.setAdapter(adapter);
        view.rvDashboardExtraJobs.addItemDecoration(
                new DividerItemDecoration(getContext()).withPaddingStart(getContext(), R.dimen.spacing_16)
        );
        view.rvDashboardExtraJobs.setItemAnimator(null);
    }

    private void updateJobs(List<JobWithProvider> jobs) {
        if (jobs != null) {
            adapter.setItems(jobs);
            view.rvDashboardExtraJobs.scrollToPosition(0);
        }
    }

    protected void showCloseShiftDialog() {
        new CloseShiftDialogFragment().showNow(getFragmentManager(), "");
    }
}