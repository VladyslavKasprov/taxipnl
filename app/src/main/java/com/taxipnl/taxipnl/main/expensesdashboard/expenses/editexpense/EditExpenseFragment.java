package com.taxipnl.taxipnl.main.expensesdashboard.expenses.editexpense;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuResId;
import com.taxipnl.taxipnl.databinding.FragmentEditExpenseBinding;

@LayoutResId(R.layout.fragment_edit_expense)
@MenuResId(R.menu.edit)
public class EditExpenseFragment extends BaseFragment<FragmentEditExpenseBinding> {

    private EditExpenseFragmentVM model;

    private int getExpenseId() {
        return EditExpenseFragmentArgs.fromBundle(getArguments()).getExpenseId();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getGlobalViewModel(EditExpenseFragmentVM.class);
        model.loadExpense(getExpenseId());
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.model, model);

        model.onDone().observe(getViewLifecycleOwner(), onDone -> findNavController().popBackStack());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_delete) {
            model.delete();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        model.clear();
        super.onDestroy();
    }
}