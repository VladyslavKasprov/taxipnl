package com.taxipnl.taxipnl.main.expensesdashboard.expenses.filter;

import org.threeten.bp.LocalDate;

import java.util.Set;

public class ExpensesFilter {

    public boolean isPeriodFilterEnabled;
    public LocalDate startDate;
    public LocalDate endDate;
    public boolean isExpenseTypesFilterEnabled;
    public Set<Integer> expenseTypesIds;

    public ExpensesFilter(boolean isPeriodFilterEnabled, LocalDate startDate, LocalDate endDate,
                          boolean isExpenseTypesFilterEnabled, Set<Integer> expenseTypesIds) {
        this.isPeriodFilterEnabled = isPeriodFilterEnabled;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isExpenseTypesFilterEnabled = isExpenseTypesFilterEnabled;
        this.expenseTypesIds = expenseTypesIds;
    }
}