package com.taxipnl.taxipnl.main.jobs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuResId;
import com.taxipnl.taxipnl.databinding.FragmentJobsBinding;
import com.taxipnl.taxipnl.db.pojo.JobWithProvider;
import com.taxipnl.taxipnl.view.DividerItemDecoration;

import java.util.List;

@LayoutResId(R.layout.fragment_jobs)
@MenuResId(R.menu.jobs)
public class JobsFragment extends BaseFragment<FragmentJobsBinding> {

    BaseAdapter<JobWithProvider> adapter;

    JobsFragmentVM jobsModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        jobsModel = getGlobalViewModel(JobsFragmentVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.jobsModel, jobsModel);
        setupJobsView();

        jobsModel.getJobs().observe(getViewLifecycleOwner(), this::updateJobs);
    }

    private void setupJobsView() {
        adapter = new BaseAdapter<JobWithProvider>(item -> R.layout.item_job)
                .setHandler(job -> findNavController().navigate(
                        JobsFragmentDirections.actionJobsToEditJob(job.id)
                ));
        view.rvJobs.setAdapter(adapter);
        view.rvJobs.addItemDecoration(
                new DividerItemDecoration(getContext()).withPaddingStart(getContext(), R.dimen.spacing_12)
        );
        view.rvJobs.setItemAnimator(null);
    }

    private void updateJobs(List<JobWithProvider> jobs) {
        if (jobs != null) {
            adapter.setItems(jobs);
            view.rvJobs.scrollToPosition(0);
        }
    }
}