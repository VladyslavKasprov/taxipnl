package com.taxipnl.taxipnl.main.setup.subscibe;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.taxipnl.taxipnl.repository.BillingRepository;

import javax.inject.Inject;

public class SubscribeVM extends ViewModel {

    private BillingRepository repository;

    private LiveData<String> price;

    private LiveData<Void> loading;
    private LiveData<BillingErrorResponse> error;

    @Inject
    public SubscribeVM(BillingRepository repository) {
        this.repository = repository;

        price = repository.getPrice();
        loading = repository.onLoading();
        error = repository.onError();
    }

    public LiveData<String> getPrice() {
        return price;
    }

    public LiveData<Void> onLoading() {
        return loading;
    }

    public LiveData<BillingErrorResponse> onError() {
        return error;
    }

    public void subscribe(Activity activity) {
        repository.subscribe(activity);
    }
}