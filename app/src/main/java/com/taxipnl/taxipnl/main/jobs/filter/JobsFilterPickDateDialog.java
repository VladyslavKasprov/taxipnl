package com.taxipnl.taxipnl.main.jobs.filter;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.main.PickDateDialog;
import com.taxipnl.taxipnl.main.jobs.JobsFragmentVM;

public abstract class JobsFilterPickDateDialog extends PickDateDialog {

    JobsFragmentVM filterModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filterModel = getGlobalViewModel(JobsFragmentVM.class);
    }
}