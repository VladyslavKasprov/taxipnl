package com.taxipnl.taxipnl.main.setup.login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.databinding.FragmentRegisterBinding;
import com.taxipnl.taxipnl.util.AndroidUtils;

@LayoutResId(R.layout.fragment_login)
public class LoginFragment extends BaseFragment<FragmentRegisterBinding> {

    LoginVM model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = getLocalViewModel(LoginVM.class);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        bind(BR.model, model);

        model.onError().observe(getViewLifecycleOwner(), this::displayMessage);
    }

    @Override
    public void onPause() {
        AndroidUtils.hideSoftKeyboard(getActivity());
        super.onPause();
    }
}