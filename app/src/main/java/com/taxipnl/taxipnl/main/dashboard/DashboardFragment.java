package com.taxipnl.taxipnl.main.dashboard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.LinearLayout;

import com.taxipnl.taxipnl.BR;
import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.base.BaseFragment;
import com.taxipnl.taxipnl.base.adapter.BaseAdapter;
import com.taxipnl.taxipnl.base.adapter.decorator.VerticalSpanDecorator;
import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuConfig;
import com.taxipnl.taxipnl.databinding.FragmentDashboardBinding;
import com.taxipnl.taxipnl.main.items.ProviderWithIncome;
import com.taxipnl.taxipnl.view.CenteredDividerItemDecoration;

import java.util.List;

@LayoutResId(R.layout.fragment_dashboard)
@MenuConfig(inheritsParentsMenu = true)
public class DashboardFragment extends BaseFragment<FragmentDashboardBinding> {

    private final static int HORIZONTAL_SPAN_COUNT = 2;
    private final static int VERTICAL_SPAN_COUNT = 3;
    private final static int[] REPORT_HEADERS = new int[] {
            R.layout.item_dashboard_jobs_count, R.layout.item_dashboard_shift_duration,
            R.layout.item_dashboard_latest_shift_cash_revenue, R.layout.item_dashboard_latest_shift_income_per_hour,
            R.layout.item_dashboard_weekly_shifts_non_street_cash_income, R.layout.item_dashboard_weekly_shifts_street_cash_income
    };

    private BaseAdapter<ProviderWithIncome> adapter;

    private BroadcastReceiver everyMinute = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_TIME_TICK.equals(intent.getAction())) {
                mainModel.updateDurations();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View _view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(_view, savedInstanceState);
        setupReportView();
        bind(BR.model, mainModel);

        mainModel.getLatestShiftIncomeByProvider().observe(getViewLifecycleOwner(), this::updateCurrentShiftIncomeByProvider);
    }

    @Override
    public void onResume() {
        super.onResume();
        mainModel.updateDurations();
        getContext().registerReceiver(everyMinute, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().unregisterReceiver(everyMinute);
    }

    private void setupReportView() {
        adapter = new BaseAdapter<ProviderWithIncome>(item -> R.layout.item_provider_with_income)
                .setHeaders(REPORT_HEADERS, mainModel, getViewLifecycleOwner())
                .setDecorator(new VerticalSpanDecorator(VERTICAL_SPAN_COUNT));
        view.rvDashboardReport.setAdapter(adapter);
        view.rvDashboardReport.setLayoutManager(new GridLayoutManager(getContext(), HORIZONTAL_SPAN_COUNT));
        view.rvDashboardReport.addItemDecoration(
                new CenteredDividerItemDecoration(getContext(), LinearLayout.HORIZONTAL, false, R.dimen.spacing_8)
        );
        view.rvDashboardReport.addItemDecoration(
                new CenteredDividerItemDecoration(getContext(), LinearLayout.VERTICAL, false, R.dimen.spacing_12)
        );
        view.rvDashboardReport.setItemAnimator(null);
    }

    private void updateCurrentShiftIncomeByProvider(List<ProviderWithIncome> items) {
        if (items != null) {
            adapter.setItems(items);
            view.rvDashboardReport.scrollToPosition(0);
        }
    }
}