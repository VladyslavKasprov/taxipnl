package com.taxipnl.taxipnl.util;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;

import java.util.List;
import java.util.Set;

import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class NavigationUtils {

    private static final int CURRENT_NAV_GRAPH_UNDEFINED = 0;

    /**
     * Determines whether the given <code>destinationIds</code> match the NavDestination. This
     * handles both the default case (the destination's id is in the given ids) and the nested
     * case where the given ids is a parent/grandparent/etc of the destination.
     */
    public static boolean matchDestinations(@NonNull NavDestination destination, @NonNull List<Integer> destinationIds) {
        NavDestination currentDestination = destination;
        do {
            if (destinationIds.contains(currentDestination.getId())) {
                return true;
            }
            currentDestination = currentDestination.getParent();
        } while (currentDestination != null);
        return false;
    }

    public static void setupToolbarWithNavController(@NonNull Toolbar toolbar, @NonNull NavController navController,
                                                     Set<Integer> topLevelDestinationIds) {
        NavigationUI.setupWithNavController(
                toolbar, navController, new AppBarConfiguration.Builder(topLevelDestinationIds).build()
        );
    }

    public static void popToTopLevelDestination(@NonNull NavController navController, Set<Integer> topLevelDestinationIds) {
        for (Integer destinationId : topLevelDestinationIds) {
            navController.popBackStack(destinationId, false);
        }
    }

    public static void switchNavGraphIfPossible(@NonNull NavController navController, @IdRes int destinationNavGraphId) {
        int currentNavGraphId = getCurrentNavGraphIdIfDefined(navController);
        if (currentNavGraphId != CURRENT_NAV_GRAPH_UNDEFINED && currentNavGraphId != destinationNavGraphId) {
            switchNavGraph(navController, destinationNavGraphId);
        }
    }

    @IdRes
    private static int getCurrentNavGraphIdIfDefined(@NonNull NavController navController) {
        return navController.getCurrentDestination() != null &&
                navController.getCurrentDestination().getParent() != null ?
                navController.getCurrentDestination().getParent().getId() : CURRENT_NAV_GRAPH_UNDEFINED;
    }

    private static void switchNavGraph(@NonNull NavController navController, @IdRes int destinationNavGraphId) {
        navController.popBackStack(navController.getGraph().getId(), true);
        navController.navigate(destinationNavGraphId);
    }
}