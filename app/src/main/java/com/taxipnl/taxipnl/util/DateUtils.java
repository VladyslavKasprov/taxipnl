package com.taxipnl.taxipnl.util;

import android.support.annotation.NonNull;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.WeekFields;

public class DateUtils {

    private static final TemporalField WEEK_OF_YEAR = WeekFields.ISO.weekOfYear();
    private static final TemporalField DAY_OF_WEEK = WeekFields.ISO.dayOfWeek();
    private static final DayOfWeek START_OF_WORKING_WEEK = DayOfWeek.MONDAY;

    public static LocalDate getYesterday() {
        return LocalDate.now().minusDays(1);
    }

    public static LocalDate getStartOfCurrentWeek() {
        return getStartOfWeek(LocalDate.now());
    }

    public static LocalDate getStartOfPreviousWeek() {
        return getStartOfWeek(LocalDate.now().minusWeeks(1));
    }

    public static LocalDate getStartOfWeek(@NonNull LocalDate dateInTheWeek) {
        return dateInTheWeek.with(START_OF_WORKING_WEEK);
    }

    public static LocalDate getSameDayOfSameWeekLastYear(@NonNull LocalDate dateOfTheDay) {
        int weekNumber = getWeekOfYearNumber(dateOfTheDay);
        int dayOfWeekNumber = getDayOfWeekNumber(dateOfTheDay);
        return dateOfTheDay.minusYears(1)
                .with(WEEK_OF_YEAR, weekNumber)
                .with(DAY_OF_WEEK, dayOfWeekNumber);
    }

    public static LocalDate getStartOfCurrentWeekLastYear() {
        return getStartOfSameWeekLastYear(LocalDate.now());
    }

    public static LocalDate getStartOfSameWeekLastYear(@NonNull LocalDate dateInTheWeek) {
        int weekNumber = getWeekOfYearNumber(dateInTheWeek);
        return dateInTheWeek.minusYears(1)
                .with(WEEK_OF_YEAR, weekNumber)
                .with(START_OF_WORKING_WEEK);
    }

    private static int getWeekOfYearNumber(@NonNull LocalDate date) {
        return date.get(WEEK_OF_YEAR);
    }

    private static int getDayOfWeekNumber(@NonNull LocalDate date) {
        return date.get(DAY_OF_WEEK);
    }

    public static LocalDate getStartOfCurrentMonth() {
        return LocalDate.now().withDayOfMonth(1);
    }

    public static LocalDate getStartOfCurrentYear() {
        return LocalDate.now().withDayOfYear(1);
    }
}