package com.taxipnl.taxipnl.util;

import android.support.annotation.NonNull;

import com.taxipnl.taxipnl.db.entity.PaymentMethod;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.db.pojo.ExpenseWithType;
import com.taxipnl.taxipnl.db.pojo.JobWithProvider;
import com.taxipnl.taxipnl.main.items.ProviderWithIncome;
import com.taxipnl.taxipnl.util.constants.Constants;

import java.util.ArrayList;
import java.util.List;

public class EconomicUtils {

    public static List<ProviderWithIncome> getIncomeByProvider(@NonNull List<Provider> providers,
                                                               @NonNull List<JobWithProvider> jobs) {
        List<ProviderWithIncome> result = new ArrayList<>();
        for (Provider provider : providers) {
            List<JobWithProvider> jobsForProvider = new ArrayList<>();
            for (JobWithProvider job : jobs) {
                if (job.providerId == provider.getId()) {
                    jobsForProvider.add(job);
                }
            }
            double income = getIncome(jobsForProvider);
            result.add(new ProviderWithIncome(provider, income));
        }
        return result;
    }

    public static Double getAmount(List<ExpenseWithType> expenses) {
        if (expenses != null) {
            Double amount = 0d;
            for (ExpenseWithType expense : expenses) {
                amount += expense.amount;
            }
            return amount;
        }
        return null;
    }

    public static Double getRevenue(List<JobWithProvider> jobs) {
        if (jobs != null) {
            Double revenue = 0d;
            for (JobWithProvider job : jobs) {
                revenue += job.fare;
            }
            return revenue;
        }
        return null;
    }

    public static Double getCashRevenue(List<JobWithProvider> jobs) {
        if (jobs != null) {
            Double revenue = 0d;
            for (JobWithProvider job : jobs) {
                if (job.providerPaymentMethod == PaymentMethod.CASH) {
                    revenue += job.fare;
                }
            }
            return revenue;
        }
        return null;
    }

    public static Double getCashIncome(List<JobWithProvider> jobs) {
        if (jobs != null) {
            Double income = 0d;
            for (JobWithProvider job : jobs) {
                if (job.providerPaymentMethod == PaymentMethod.CASH) {
                    income += getIncome(job);
                }
            }
            return income;
        }
        return null;
    }

    public static Double getNonStreetCashIncome(List<JobWithProvider> jobs) {
        if (jobs != null) {
            Double income = 0d;
            for (JobWithProvider job : jobs) {
                if (job.providerId != Constants.PROVIDER_ID_STREET_CASH) {
                    income += getIncome(job);
                }
            }
            return income;
        }
        return null;
    }

    public static Double getStreetCashIncome(List<JobWithProvider> jobs) {
        if (jobs != null) {
            Double income = 0d;
            for (JobWithProvider job : jobs) {
                if (job.providerId == Constants.PROVIDER_ID_STREET_CASH) {
                    income += getIncome(job);
                }
            }
            return income;
        }
        return null;
    }

    public static Double getIncome(List<JobWithProvider> jobs) {
        if (jobs != null) {
            Double income = 0d;
            for (JobWithProvider job : jobs) {
                income += getIncome(job);
            }
            return income;
        }
        return null;
    }

    public static double getIncome(JobWithProvider job) {
        return getIncome(job.fare, job.commission, job.toll);
    }

    private static double getIncome(double fare, double commission, double toll) {
        return (fare - toll) * (1 - commission / 100.0);
    }


    public static Double getIncomePlusToll(List<JobWithProvider> jobs) {
        if (jobs != null) {
            Double income = 0d;
            for (JobWithProvider job : jobs) {
                income += getIncomePlusToll(job);
            }
            return income;
        }
        return null;
    }

    public static double getIncomePlusToll(JobWithProvider job) {
        return isStreetProvider(job) ?
                getIncome(job.fare, job.commission, job.toll) :
                getIncome(job.fare, job.commission, job.toll) + job.toll;
    }

    private static boolean isStreetProvider (JobWithProvider job) {
        return job.providerId == Constants.PROVIDER_ID_STREET_CASH ||
                job.providerId == Constants.PROVIDER_ID_STREET_CARD;
    }
}