package com.taxipnl.taxipnl.util;

import android.support.annotation.NonNull;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;

public class DateTimeUtils {

    public static LocalDateTime getStartOfCurrentWeek() {
        return DateUtils.getStartOfCurrentWeek().atStartOfDay();
    }

    public static LocalDateTime getStartOfWeek(@NonNull LocalDate dateInTheWeek) {
        return DateUtils.getStartOfWeek(dateInTheWeek).atStartOfDay();
    }
}