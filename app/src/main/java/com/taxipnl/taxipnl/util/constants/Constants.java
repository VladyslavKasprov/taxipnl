package com.taxipnl.taxipnl.util.constants;

import com.taxipnl.taxipnl.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Constants {

    public static final int PROVIDER_ID_STREET_CARD = 1;
    public static final int PROVIDER_ORDER_NUMBER_STREET_CARD = - 2;

    public static final int PROVIDER_ID_STREET_CASH = 2;
    public static final int PROVIDER_ORDER_NUMBER_STREET_CASH = - 1;

    public static final int EXPENSE_TYPE_ID_FUEL = 1;
    public static final int EXPENSE_TYPE_ORDER_NUMBER_FUEL = - 2;

    public static final int EXPENSE_TYPE_ID_TOLL = 2;
    public static final int EXPENSE_TYPE_ORDER_NUMBER_TOLL = - 1;

    public static final int INITIAL_WEEKLY_TARGET = 1000;

    // Job validation
    public static final double MAX_FARE = 100000;

    // Provider validation
    public static final int MAX_COMMISSION = 100;

    public static final String CHECK_SUBSCRIPTION_EXPIRATION_UNIQUE_WORK_NAME = "CHECK_SUBSCRIPTION_EXPIRATION";

    public static final String CHECK_SHIFT_INACTIVITY_UNIQUE_WORK_NAME = "CHECK_SHIFT_INACTIVITY";
    public static final int SHIFT_INACTIVITY_MAX_HOURS = 5;
    public static final int CLOSE_SHIFT_AFTER_LAST_ACTION_MINUTES = 30;

    public static final int USER_INACTIVITY_TIMEOUT = 3 * 60 * 1000;

    // Currency codes
    private static final String CURRENCY_CODE_EURO = "EUR";
    private static final String CURRENCY_CODE_US_DOLLAR = "USD";
    private static final String CURRENCY_CODE_BRITISH_POUND = "GBP";

    // Currencies
    public static final List<Currency> CURRENCIES = new ArrayList<>();
    static {
        CURRENCIES.add(Currency.getInstance(CURRENCY_CODE_EURO));
        CURRENCIES.add(Currency.getInstance(CURRENCY_CODE_US_DOLLAR));
        CURRENCIES.add(Currency.getInstance(CURRENCY_CODE_BRITISH_POUND));
    }

    // Navigation
    public static final List<Integer> SETUP_GRAPHS = Arrays.asList(
            R.id.start, R.id.subscribeGraph, R.id.registerGraph, R.id.loginGraph
    );
    public static final Set<Integer> TOP_LEVEL_DESTINATIONS = new HashSet<>(Arrays.asList(
            R.id.home, R.id.dashboard
    ));
}