package com.taxipnl.taxipnl.util;

import android.content.Context;
import android.support.annotation.NonNull;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.db.entity.PaymentMethod;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class FormatUtils {

    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("MMM d, YYYY");
    private static final DateTimeFormatter DATE_FORMATTER_2 = DateTimeFormatter.ofPattern("dd MMM, YYYY");
    private static final DateTimeFormatter DATE_FORMATTER_3 = DateTimeFormatter.ofPattern("MMMM dd, YYYY");
    private static final DateTimeFormatter LONG_DATE_FORMATTER = DateTimeFormatter.ofPattern("EEEE, MMMM d, YYYY");

    private static final DecimalFormat CURRENCY_FORMAT = (DecimalFormat) NumberFormat.getCurrencyInstance(Locale.getDefault());
    private static final DecimalFormat CURRENCY_WITHOUT_FRACTION_FORMAT = (DecimalFormat) CURRENCY_FORMAT.clone();

    public static void updateCurrency(@NonNull Currency currency) {
        CURRENCY_FORMAT.setCurrency(currency);
        CURRENCY_FORMAT.setNegativePrefix("-" + currency.getSymbol());
        CURRENCY_FORMAT.setNegativeSuffix("");
        CURRENCY_FORMAT.setMaximumFractionDigits(2);
        CURRENCY_FORMAT.setMinimumFractionDigits(2);

        CURRENCY_WITHOUT_FRACTION_FORMAT.setCurrency(currency);
        CURRENCY_WITHOUT_FRACTION_FORMAT.setNegativePrefix("-" + currency.getSymbol());
        CURRENCY_WITHOUT_FRACTION_FORMAT.setNegativeSuffix("");
        CURRENCY_WITHOUT_FRACTION_FORMAT.setMaximumFractionDigits(0);
    }

    public static String formatAsTime(@NonNull LocalDateTime dateTime) {
        return dateTime.format(TIME_FORMATTER);
    }

    public static String formatAsDate(@NonNull LocalDateTime dateTime) {
        return dateTime.format(DATE_FORMATTER);
    }

    public static String formatAsDate2(@NonNull LocalDate date) {
        return date.format(DATE_FORMATTER_2);
    }

    public static String formatAsDate3(@NonNull LocalDate date) {
        return date.format(DATE_FORMATTER_3);
    }

    public static String formatAsLongDate(@NonNull LocalDateTime dateTime) {
        return dateTime.format(LONG_DATE_FORMATTER);
    }

    public static String formatAsFullProviderName(@NonNull Context context, @NonNull String providerName, @NonNull PaymentMethod paymentMethod) {
        if (providerName.isEmpty()) {
            return paymentMethod.getName(context);
        } else {
            return context.getResources().getString(R.string.all_format_full_provider_name,
                    providerName, paymentMethod.getName(context));
        }
    }

    public static String formatAsCurrency(double number) {
        return CURRENCY_FORMAT.format(number);
    }

    public static String formatAsCurrencyWithoutFraction(double number) {
        return CURRENCY_WITHOUT_FRACTION_FORMAT.format(number);
    }

    public static String formatAsDecimal(@NonNull Context context, double number) {
        return context.getString(R.string.all_format_decimal, number);
    }

    public static String formatAsInteger(@NonNull Context context, double number) {
        return context.getString(R.string.all_format_integer, number);
    }

    public static String formatAsPercent(@NonNull Context context, double commission) {
        return context.getResources().getString(R.string.all_format_percent, commission);
    }

    public static String formatAsHoursAndMinutes(@NonNull Context context, long seconds) {
        return context.getResources().getString(R.string.all_format_hours_and_minutes,
                seconds / 3600, (seconds % 3600) / 60);
    }
}