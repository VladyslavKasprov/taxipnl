package com.taxipnl.taxipnl.util;

import org.threeten.bp.Duration;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;

public class DurationUtils {

    public static int getNumberOfDaysBetween(LocalDate startInclusive, LocalDate endInclusive) {
        LocalDateTime endExclusive = endInclusive.plusDays(1).atStartOfDay();
        return (int) Duration.between(startInclusive.atStartOfDay(), endExclusive).toDays();
    }
}
