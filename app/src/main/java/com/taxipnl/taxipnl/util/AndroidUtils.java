package com.taxipnl.taxipnl.util;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.taxipnl.taxipnl.R;

public class AndroidUtils {

    public static int getWindowWidthPixels(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int getWindowHeightPixels(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        int windowHeightPixels = displayMetrics.heightPixels;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            windowHeightPixels -= activity.getResources().getDimensionPixelOffset(R.dimen.status_bar_height);
        }

        return windowHeightPixels;
    }

    public static void showSoftKeyboard(Activity activity, View view) {
        if (activity != null && view != null && view.requestFocus()) {
            InputMethodManager keyboard = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (keyboard != null) {
                keyboard.showSoftInput(view, 0);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity != null) {
            InputMethodManager keyboard = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (keyboard != null) {
                View focusedView = activity.getCurrentFocus();
                if (focusedView != null) {
                    keyboard.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
                }
            }
        }
    }
}