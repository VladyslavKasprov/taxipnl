package com.taxipnl.taxipnl.util;

import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.db.entity.Shift;

import org.jetbrains.annotations.Contract;
import org.threeten.bp.LocalDate;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Safe {

    @Contract(value = "null -> false", pure = true)
    public static boolean unbox(Boolean value) {
        return value != null && value;
    }

    @Contract(value = "null -> null", pure = true)
    public static <T> Integer size(@Nullable Collection<T> collection) {
        return collection == null ? null : collection.size();
    }

    @Contract(value = "null -> !null", pure = true)
    public static <T> List<T> singletonList(@Nullable T item) {
        return item == null ? Collections.emptyList() : Collections.singletonList(item);
    }


    //    Model specific
    @Contract(value = "null -> null", pure = true)
    public static LocalDate getStartDate(Shift shift) {
        return shift != null ? shift.getStartDateTime().toLocalDate() : null;
    }
}