package com.taxipnl.taxipnl.util;

import android.support.annotation.NonNull;

import com.taxipnl.taxipnl.db.entity.Shift;

import org.threeten.bp.Duration;
import org.threeten.bp.LocalDateTime;

import java.util.List;

public class ShiftUtils {

    public static boolean isShiftOpened(Shift shift) {
        return shift != null && shift.getEndDateTime() == null;
    }

    // Durations are calculated in seconds
    public static Long getShiftsDuration(List<Shift> shifts) {
        if (shifts != null) {
            long duration = 0;
            for (Shift shift : shifts) {
                duration += getShiftDuration(shift);
            }
            return duration;
        }
        return null;
    }

    // Durations are calculated in seconds
    public static Long getShiftDuration(Shift shift) {
        if (shift != null) {
            LocalDateTime shiftEnd = getShiftsEnd(shift);
            return Duration.between(shift.getStartDateTime(), shiftEnd).getSeconds();
        }
        return null;
    }

    private static LocalDateTime getShiftsEnd(@NonNull Shift shift) {
        return shift.getEndDateTime() != null ? shift.getEndDateTime() : LocalDateTime.now();
    }
}