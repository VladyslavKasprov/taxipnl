package com.taxipnl.taxipnl.util;

import android.databinding.BindingAdapter;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.text.Editable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.taxipnl.taxipnl.db.entity.PaymentMethod;
import com.taxipnl.taxipnl.view.SimpleTextWatcher;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;

import androidx.navigation.Navigation;

public class BindingAdapters {

    @BindingAdapter("android:onClickNavigateTo")
    public static void setOnClickNavigation(View view, @IdRes int destinationId){
        view.setOnClickListener(Navigation.createNavigateOnClickListener(destinationId));
    }

    @BindingAdapter("android:imageSrc")
    public static void setImage(ImageView view, @DrawableRes int imageResId) {
        Picasso.get()
                .load(imageResId)
                .into(view);
    }

    @BindingAdapter("android:time")
    public static void setTime(TextView view, LocalDateTime dateTime) {
        if (dateTime != null) {
            view.setText(FormatUtils.formatAsTime(dateTime));
        }
    }

    @BindingAdapter("android:date")
    public static void setDate(TextView view, LocalDateTime dateTime) {
        if (dateTime != null) {
            view.setText(FormatUtils.formatAsDate(dateTime));
        }
    }

    @BindingAdapter("android:date2")
    public static void setDate2(TextView view, LocalDate date) {
        if (date != null) {
            view.setText(FormatUtils.formatAsDate2(date));
        }
    }

    @BindingAdapter("android:date3")
    public static void setDate3(TextView view, LocalDate date) {
        if (date != null) {
            view.setText(FormatUtils.formatAsDate3(date));
        }
    }

    @BindingAdapter({"android:providerName", "android:providerPaymentMethod"})
    public static void setFullProviderName(TextView view, String name, PaymentMethod paymentMethod) {
        if (name != null && paymentMethod != null) {
            view.setText(FormatUtils.formatAsFullProviderName(view.getContext(), name, paymentMethod));
        }
    }

    @BindingAdapter("android:currency")
    public static void setCurrency(TextView view, Double number) {
        if (number != null) {
            view.setText(FormatUtils.formatAsCurrency(number));
        }
    }

    @BindingAdapter("android:currencyWithoutFraction")
    public static void setCurrencyWithoutFraction(TextView view, Double number) {
        if (number != null) {
            view.setText(FormatUtils.formatAsCurrencyWithoutFraction(number));
        }
    }

    @BindingAdapter("android:decimal")
    public static void setDecimal(TextView view, Double number) {
        if (number != null) {
            view.setText(FormatUtils.formatAsDecimal(view.getContext(), number));
        }
    }

    @BindingAdapter("android:integer")
    public static void setInteger(TextView view, Integer number) {
        if (number != null) {
            view.setText(FormatUtils.formatAsInteger(view.getContext(), number));
        }
    }

    @BindingAdapter("android:percent")
    public static void setPercent(TextView view, Double number) {
        if (number != null) {
            view.setText(FormatUtils.formatAsPercent(view.getContext(), number));
        }
    }

    @BindingAdapter("android:hoursAndMinutes")
    public static void setHoursAndMinutes(TextView view, Long seconds) {
        if (seconds != null) {
            view.setText(FormatUtils.formatAsHoursAndMinutes(view.getContext(), seconds));
        }
    }

    @BindingAdapter({"android:textString", "android:textResId", "android:textStringIf"})
    public static void setTextFromStringOrResId(TextView view, String text, @StringRes int textResId, boolean isTextString) {
        if (isTextString) {
            if (text != null) {
                view.setText(text);
            }
        } else {
            view.setText(textResId);
        }
    }

    @BindingAdapter("android:formatResId")
    public static void setFormatResId(TextView view, @StringRes int format) {
        view.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            protected void update(Editable sequence) {
                String newText = view.getResources().getString(format, sequence);
                view.setText(newText);
            }
        });
    }

    @BindingAdapter("android:selectedIf")
    public static void setSelectedIf(View view, boolean selected) {
        view.setSelected(selected);
    }

    @BindingAdapter("android:invisibleIf")
    public static void setInvisibleIf(View view, boolean invisible) {
        view.setVisibility(invisible ? View.INVISIBLE : View.VISIBLE);
    }

    @BindingAdapter("android:goneIf")
    public static void setGoneIf(View view, boolean gone) {
        view.setVisibility(gone ? View.GONE : View.VISIBLE);
    }
}