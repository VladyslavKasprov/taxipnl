package com.taxipnl.taxipnl.util.constants;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class SortingMode {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            Jobs.BY_ENTERED_DATE_DESC, Jobs.BY_ENTERED_DATE_ASC,
            Jobs.BY_PROVIDER_ORDER_DESC, Jobs.BY_PROVIDER_ORDER_ASC,
            Jobs.BY_FARE_DESC, Jobs.BY_FARE_ASC
    })
    public @interface Jobs {
        int BY_ENTERED_DATE_DESC = 1;
        int BY_ENTERED_DATE_ASC = 2;
        int BY_PROVIDER_ORDER_DESC = 3;
        int BY_PROVIDER_ORDER_ASC = 4;
        int BY_FARE_DESC = 5;
        int BY_FARE_ASC = 6;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            Expenses.BY_ENTERED_DATE_DESC, Expenses.BY_ENTERED_DATE_ASC,
            Expenses.BY_EXPENSE_TYPE_ORDER_DESC, Expenses.BY_EXPENSE_TYPE_ORDER_ASC,
            Expenses.BY_AMOUNT_DESC, Expenses.BY_AMOUNT_ASC
    })
    public @interface Expenses {
        int BY_ENTERED_DATE_DESC = 1;
        int BY_ENTERED_DATE_ASC = 2;
        int BY_EXPENSE_TYPE_ORDER_DESC = 3;
        int BY_EXPENSE_TYPE_ORDER_ASC = 4;
        int BY_AMOUNT_DESC = 5;
        int BY_AMOUNT_ASC = 6;
    }
}