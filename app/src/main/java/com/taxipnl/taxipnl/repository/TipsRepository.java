package com.taxipnl.taxipnl.repository;

import android.arch.lifecycle.LiveData;

import com.taxipnl.taxipnl.db.dao.TipsDao;
import com.taxipnl.taxipnl.db.entity.Tip;
import com.taxipnl.taxipnl.di.qualifiers.DiskExecutor;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;

import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TipsRepository {

    private TipsDao dao;
    private Executor executor;

    @Inject
    public TipsRepository(TipsDao dao, @DiskExecutor Executor executor) {
        this.dao = dao;
        this.executor = executor;
    }

    public LiveData<Double> getTipsAmount(LocalDate startDate, LocalDate endDate) {
        LocalDateTime startDateTime = startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate.plusDays(1).atStartOfDay();
        return dao.getAmount(startDateTime, endDateTime);
    }

    public void create(Tip tip) {
        executor.execute(() -> dao.insert(tip));
    }
}