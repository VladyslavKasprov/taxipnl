package com.taxipnl.taxipnl.repository;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.Nullable;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClient.BillingResponse;
import com.android.billingclient.api.BillingClient.FeatureType;
import com.android.billingclient.api.BillingClient.SkuType;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;
import com.taxipnl.taxipnl.SingleLiveEvent;
import com.taxipnl.taxipnl.db.Converters;
import com.taxipnl.taxipnl.db.dao.SubscriptionDetailsDao;
import com.taxipnl.taxipnl.di.qualifiers.DiskExecutor;
import com.taxipnl.taxipnl.di.qualifiers.PackageName;
import com.taxipnl.taxipnl.main.setup.subscibe.BillingErrorResponse;

import org.threeten.bp.LocalDateTime;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.taxipnl.taxipnl.util.constants.Billing.SUBSCRIPTION_SKU;
import static org.threeten.bp.LocalDateTime.now;

@Singleton
public class BillingRepository implements PurchasesUpdatedListener, BillingClientStateListener,
        SkuDetailsResponseListener, PurchaseHistoryResponseListener {

    private SubscriptionDetailsDao dao;
    private Executor executor;

    private String packageName;
    private AndroidPublisher publisher;
    private SkuDetailsParams skuDetailsParams;
    private BillingClient billingClient;
    private BillingFlowParams billingFlowParams;

    private LiveData<Boolean> isSubscriptionActive;
    private SingleLiveEvent<Void> loading = new SingleLiveEvent<>();
    private SingleLiveEvent<BillingErrorResponse> error = new SingleLiveEvent<>();

    @Inject
    public BillingRepository(SubscriptionDetailsDao dao, @DiskExecutor Executor executor,
                             @PackageName String packageName, AndroidPublisher publisher,
                             SkuDetailsParams skuDetailsParams, Application application) {
        this.dao = dao;
        this.executor = executor;
        this.packageName = packageName;
        this.publisher = publisher;
        this.skuDetailsParams = skuDetailsParams;
        billingClient = BillingClient.newBuilder(application).setListener(this).build();

        isSubscriptionActive = dao.getIsSubscriptionActive();
        executor.execute(() -> {
            dao.clearIsSubscriptionActive();

            LocalDateTime expiryDateTime = dao.getExpiryDateTimeSync();
            if (expiryDateTime == null || now().isAfter(expiryDateTime)) {
                billingClient.startConnection(this);
            } else {
                setIsSubscriptionActive(true);
            }
        });
    }

    public LiveData<String> getPrice() {
        return dao.getPrice();
    }

    private void setPrice(String price) {
        executor.execute(() -> dao.updatePrice(price));
    }

    public LiveData<Boolean> isSubscriptionActive() {
        return isSubscriptionActive;
    }

    private void setIsSubscriptionActive(boolean isSubscriptionActive) {
        executor.execute(() -> dao.updateIsSubscriptionActive(isSubscriptionActive));
    }

    public LiveData<Void> onLoading() {
        return loading;
    }

    public LiveData<BillingErrorResponse> onError() {
        return error;
    }

    @Override
    public void onBillingSetupFinished(@BillingResponse int responseCode) {
        if (isError(responseCode)) {
            notifyOnError(responseCode);
            return;
        }

        billingClient.queryPurchaseHistoryAsync(SkuType.SUBS, this);
    }

    @Override
    public void onPurchaseHistoryResponse(@BillingResponse int responseCode, @Nullable List<Purchase> purchases) {
        if (!billingClient.isReady()) {
            billingClient.startConnection(this);
            notifyOnError(BillingResponse.SERVICE_DISCONNECTED);
            return;
        }
        billingClient.querySkuDetailsAsync(skuDetailsParams,this);

        checkPurchases(responseCode, purchases);
    }

    @Override
    public void onSkuDetailsResponse(@BillingResponse int responseCode, List<SkuDetails> skuDetailsList) {
        if (isError(responseCode)) {
            notifyOnError(responseCode);
            return;
        }

        // Can be true if, for example, wrong SkuDetailsParams were supplied to BillingClient.querySkuDetailsAsync.
        if (skuDetailsList.isEmpty()){
            error.postValue(BillingErrorResponse.forCode(BillingResponse.DEVELOPER_ERROR));
            return;
        }

        SkuDetails skuDetails = skuDetailsList.get(0);
        setPrice(skuDetails.getPrice());
        billingFlowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(skuDetails)
                .build();
    }

    public void subscribe(Activity activity) {
        loading.call();
        if (!billingClient.isReady()) {
            billingClient.startConnection(this);
            notifyOnError(BillingResponse.SERVICE_DISCONNECTED);
            return;
        }

        int isFeatureSupportedResponseCode = billingClient.isFeatureSupported(FeatureType.SUBSCRIPTIONS);
        if (isError(isFeatureSupportedResponseCode)) {
            notifyOnError(isFeatureSupportedResponseCode);
            return;
        }

        // Can be true if we received empty skuDetailsList in SkuDetailsResponseListener.onSkuDetailsResponse.
        if (billingFlowParams == null) {
            notifyOnError(BillingResponse.DEVELOPER_ERROR);
            return;
        }

        int billingFlowResponseCode = billingClient.launchBillingFlow(activity, billingFlowParams);
        if (isError(billingFlowResponseCode)) {
            notifyOnError(billingFlowResponseCode);
        }
    }

    @Override
    public void onPurchasesUpdated(@BillingResponse int responseCode, @Nullable List<Purchase> purchases) {
        checkPurchases(responseCode, purchases);
    }

    private void checkPurchases(@BillingResponse int responseCode, @Nullable List<Purchase> purchases) {
        if (isError(responseCode)) {
            setIsSubscriptionActive(false);
            notifyOnError(responseCode);
            return;
        }

        if (purchases != null) {
            for (Purchase purchase : purchases) {
                if (SUBSCRIPTION_SKU.equals(purchase.getSku())) {
                    executor.execute(() -> {
                        dao.updatePurchaseToken(purchase.getPurchaseToken());
                        try {
                            SubscriptionPurchase purchaseFull = publisher
                                    .purchases()
                                    .subscriptions()
                                    .get(packageName, purchase.getSku(), purchase.getPurchaseToken())
                                    .execute();
                            LocalDateTime expiryDateTime = Converters.toLocalDateTimeFromMilli(purchaseFull.getExpiryTimeMillis());
                            dao.updateExpiryDateTime(expiryDateTime);
                            if (now().isBefore(expiryDateTime)) {
                                setIsSubscriptionActive(true);
                                return;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        setIsSubscriptionActive(false);
                    });
                    return;
                }
            }
        }

        setIsSubscriptionActive(false);
    }

    @Override
    public void onBillingServiceDisconnected() {
        // We will try to restart the connection on the next request to Google Play.
    }

    private void notifyOnError(@BillingResponse int responseCode) {
        error.postValue(BillingErrorResponse.forCode(responseCode));
    }

    private static boolean isError(@BillingResponse int responseCode) {
        return responseCode != BillingResponse.OK;
    }
}