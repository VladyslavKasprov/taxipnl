package com.taxipnl.taxipnl.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.db.dao.ShiftsDao;
import com.taxipnl.taxipnl.db.entity.Shift;
import com.taxipnl.taxipnl.di.qualifiers.DiskExecutor;
import com.taxipnl.taxipnl.util.DateTimeUtils;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ShiftsRepository {

    private ShiftsDao dao;
    private Executor executor;

    @Inject
    public ShiftsRepository(ShiftsDao dao, @DiskExecutor Executor executor) {
        this.dao = dao;
        this.executor = executor;
    }

    public LiveData<Shift> getLatestShift() {
        return dao.getLatestShift();
    }

    public LiveData<Shift> getLastClosedShift() {
        return dao.getLastClosedShift();
    }

    public LiveData<List<Shift>> getShiftsStartedAfterStartOfWeek(@Nullable LocalDate dateInTheWeek) {
        if (dateInTheWeek == null) {
            return new MutableLiveData<>();
        }
        LocalDateTime startDateTime = DateTimeUtils.getStartOfWeek(dateInTheWeek);
        return dao.getShiftsStartedAfter(startDateTime);
    }

    public LiveData<List<Shift>> getShiftsStartedInPeriod(LocalDate startDate, LocalDate endDate) {
        LocalDateTime startDateTime = startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate.plusDays(1).atStartOfDay();
        return dao.getShiftsStartedInPeriod(startDateTime, endDateTime);
    }

    public void create(Shift shift) {
        executor.execute(() -> dao.insert(shift));
    }

    public void update(Shift shift) {
        executor.execute(() -> dao.update(shift));
    }

    public void delete(Shift shift) {
        executor.execute(() -> dao.delete(shift));
    }
}