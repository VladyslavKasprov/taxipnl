package com.taxipnl.taxipnl.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.db.dao.JobsDao;
import com.taxipnl.taxipnl.db.entity.Shift;
import com.taxipnl.taxipnl.db.pojo.JobWithProvider;
import com.taxipnl.taxipnl.di.qualifiers.DiskExecutor;
import com.taxipnl.taxipnl.util.DateUtils;
import com.taxipnl.taxipnl.util.constants.SortingMode;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class JobsRepository {

    private JobsDao dao;
    private Executor executor;

    @Inject
    public JobsRepository(JobsDao dao, @DiskExecutor Executor executor) {
        this.dao = dao;
        this.executor = executor;
    }

    public LiveData<List<JobWithProvider>> getAll(@SortingMode.Jobs int sortingMode) {
        return dao.getAll(sortingMode);
    }

    public LiveData<List<JobWithProvider>> get(LocalDate startDate, LocalDate endDate, @SortingMode.Jobs int sortingMode) {
        LocalDateTime startDateTime = startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate.plusDays(1).atStartOfDay();
        return dao.get(startDateTime, endDateTime, sortingMode);
    }

    public LiveData<List<JobWithProvider>> get(Set<Integer> providersIds, @SortingMode.Jobs int sortingMode) {
        return dao.get(providersIds, sortingMode);
    }

    public LiveData<List<JobWithProvider>> get(LocalDate startDate, LocalDate endDate,
                                               Set<Integer> providersIds, @SortingMode.Jobs int sortingMode) {
        LocalDateTime startDateTime = startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate.plusDays(1).atStartOfDay();
        return dao.get(startDateTime, endDateTime, providersIds, sortingMode);
    }

    public LiveData<List<JobWithProvider>> getShiftJobs(@NonNull Shift shift, @SortingMode.Jobs int sortingMode) {
        return dao.getShiftJobs(shift.getId(), sortingMode);
    }

    public LiveData<List<JobWithProvider>> getShiftsJobs(@Nullable List<Shift> shifts) {
        if (shifts == null) {
            return new MutableLiveData<>();
        }
        List<Integer> shiftsIds = new ArrayList<>();
        for (Shift shift : shifts) {
            shiftsIds.add(shift.getId());
        }
        return dao.getShiftsJobs(shiftsIds);
    }

    public LiveData<JobWithProvider> get(@Nullable Integer id) {
        return id != null ? dao.get(id) : new MutableLiveData<>();
    }

    public LiveData<Double> getSameDayLastWeekIncome(@Nullable LocalDate dateOfTheDay) {
        if (dateOfTheDay == null) {
            return new MutableLiveData<>();
        }
        LocalDate sameDayLastWeek = dateOfTheDay.minusWeeks(1);
        LocalDateTime startDateTime = sameDayLastWeek.atStartOfDay();
        LocalDateTime endDateTime = sameDayLastWeek.plusDays(1).atStartOfDay();
        return dao.sumIncome(startDateTime, endDateTime);
    }

    public LiveData<Double> getSameDayOfSameWeekLastYearIncome(@Nullable LocalDate dateOfTheDay) {
        if (dateOfTheDay == null) {
            return new MutableLiveData<>();
        }
        LocalDate sameDayOfSameWeekLastYear = DateUtils.getSameDayOfSameWeekLastYear(dateOfTheDay);
        LocalDateTime startDateTime = sameDayOfSameWeekLastYear.atStartOfDay();
        LocalDateTime endDateTime = sameDayOfSameWeekLastYear.plusDays(1).atStartOfDay();
        return dao.sumIncome(startDateTime, endDateTime);
    }

    public void insert(LocalDateTime enteredDateTime, double fare, double toll, int providerId, int shiftId) {
        executor.execute(() -> dao.insert(enteredDateTime, fare, toll, providerId, shiftId));
    }

    public void update(int id, double fare, double toll) {
        executor.execute(() -> dao.update(id, fare, toll));
    }

    public void update(int id, double fare, double toll, int providerId) {
        executor.execute(() -> dao.update(id, fare, toll, providerId));
    }

    public void delete(int id) {
        executor.execute(() -> dao.delete(id));
    }
}