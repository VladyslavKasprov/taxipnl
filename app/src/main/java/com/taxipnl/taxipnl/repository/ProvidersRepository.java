package com.taxipnl.taxipnl.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.db.dao.ProvidersDao;
import com.taxipnl.taxipnl.db.entity.PaymentMethod;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.di.qualifiers.DiskExecutor;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ProvidersRepository {

    private ProvidersDao dao;
    private Executor executor;

    @Inject
    public ProvidersRepository(ProvidersDao dao, @DiskExecutor Executor executor) {
        this.dao = dao;
        this.executor = executor;
    }

    public LiveData<List<Provider>> getAll() {
        return dao.getAll();
    }

    public LiveData<List<Provider>> getAllExceptStreet() {
        return dao.getAllExceptStreet();
    }

    public LiveData<List<Provider>> getAllSelectable() {
        return dao.getAllSelectable();
    }

    public LiveData<Provider> get(@Nullable Integer id) {
        return id != null ? dao.get(id) : new MutableLiveData<>();
    }

    public LiveData<Provider> get(String name, PaymentMethod paymentMethod) {
        return dao.get(name, paymentMethod);
    }

    public void create(Provider provider) {
        executor.execute(() -> dao.insert(provider));
    }

    public void update(int id, String name, double commission, boolean isSelectable) {
        executor.execute(() -> dao.update(id, name, commission, isSelectable));
    }

    public void update(List<Provider> providers) {
        executor.execute(() -> dao.update(providers));
    }

    public LiveData<Integer> delete(int id) {
        MutableLiveData<Integer> numberOfAffectedRows = new MutableLiveData<>();
        executor.execute(() -> numberOfAffectedRows.postValue(dao.delete(id)));
        return numberOfAffectedRows;
    }
}