package com.taxipnl.taxipnl.repository;

import android.arch.lifecycle.LiveData;

import com.taxipnl.taxipnl.db.dao.UserDao;
import com.taxipnl.taxipnl.db.entity.User;
import com.taxipnl.taxipnl.db.pojo.AuthorizationStatus;
import com.taxipnl.taxipnl.di.qualifiers.DiskExecutor;

import java.util.Currency;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class UserRepository {

    private UserDao dao;
    private Executor executor;

    @Inject
    public UserRepository(UserDao dao, @DiskExecutor Executor executor) {
        this.dao = dao;
        this.executor = executor;
    }

    public LiveData<AuthorizationStatus> getAuthorizationStatus() {
        return dao.getAuthorizationStatus();
    }

    public LiveData<Currency> getCurrency() {
        return dao.getCurrency();
    }

    public void setCurrency(Currency currency) {
        executor.execute(() -> dao.updateCurrency(currency));
    }

    public LiveData<String> getPassword() {
        return dao.getPassword();
    }

    public void setPassword(String password) {
        executor.execute(() -> dao.updatePassword(password));
    }

    public void register(String password, Currency currency) {
        executor.execute(() -> dao.insertOrReplace(new User(true, password, currency)));
    }

    public void login() {
        executor.execute(() -> dao.updateIsLoggedIn(true));
    }

    public void logout() {
        executor.execute(() -> dao.updateIsLoggedIn(false));
    }
}