package com.taxipnl.taxipnl.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.db.dao.ExpensesDao;
import com.taxipnl.taxipnl.db.entity.Expense;
import com.taxipnl.taxipnl.db.pojo.ExpenseTypeWithAmount;
import com.taxipnl.taxipnl.db.pojo.ExpenseWithType;
import com.taxipnl.taxipnl.di.qualifiers.DiskExecutor;
import com.taxipnl.taxipnl.util.DateTimeUtils;
import com.taxipnl.taxipnl.util.constants.SortingMode;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;

import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ExpensesRepository {

    private ExpensesDao dao;
    private Executor executor;

    @Inject
    public ExpensesRepository(ExpensesDao dao, @DiskExecutor Executor executor) {
        this.dao = dao;
        this.executor = executor;
    }

    public LiveData<List<ExpenseWithType>> getAll(@SortingMode.Expenses int sortingMode) {
        return dao.getAll(sortingMode);
    }

    public LiveData<Double> getAmount(LocalDate startDate, LocalDate endDate) {
        LocalDateTime startDateTime = startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate.plusDays(1).atStartOfDay();
        return dao.getAmount(startDateTime, endDateTime);
    }

    public LiveData<List<ExpenseTypeWithAmount>> getExpenseAmountByType(LocalDate startDate, LocalDate endDate) {
        LocalDateTime startDateTime = startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate.plusDays(1).atStartOfDay();
        return dao.getExpenseAmountByType(startDateTime, endDateTime);
    }

    public LiveData<List<ExpenseWithType>> get(LocalDate startDate, LocalDate endDate,
                                               @SortingMode.Expenses int sortingMode) {
        LocalDateTime startDateTime = startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate.plusDays(1).atStartOfDay();
        return dao.get(startDateTime, endDateTime, sortingMode);
    }

    public LiveData<List<ExpenseWithType>> get(Set<Integer> expenseTypesIds, @SortingMode.Expenses int sortingMode) {
        return dao.get(expenseTypesIds, sortingMode);
    }

    public LiveData<List<ExpenseWithType>> get(LocalDate startDate, LocalDate endDate,
                                               Set<Integer> expenseTypesIds, @SortingMode.Expenses int sortingMode) {
        LocalDateTime startDateTime = startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate.plusDays(1).atStartOfDay();
        return dao.get(startDateTime, endDateTime, expenseTypesIds, sortingMode);
    }

    public LiveData<ExpenseWithType> get(@Nullable Integer id) {
        return id != null ? dao.get(id) : new MutableLiveData<>();
    }

    public LiveData<Double> getWeeklyAmount(@Nullable LocalDate dateInTheWeek) {
        if (dateInTheWeek == null) {
            return new MutableLiveData<>();
        }
        LocalDateTime startDateTime = DateTimeUtils.getStartOfWeek(dateInTheWeek);
        LocalDateTime endDateTime = startDateTime.plusWeeks(1);
        return dao.sumAmount(startDateTime, endDateTime);
    }

    public void create(Expense expense) {
        executor.execute(() -> dao.insert(expense));
    }

    public void update(int id, double amount) {
        executor.execute(() -> dao.update(id, amount));
    }

    public void update(int id, double amount, int expenseTypeId) {
        executor.execute(() -> dao.update(id, amount, expenseTypeId));
    }

    public void delete(int id) {
        executor.execute(() -> dao.delete(id));
    }
}