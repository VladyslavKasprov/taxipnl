package com.taxipnl.taxipnl.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.db.dao.ExpenseTypesDao;
import com.taxipnl.taxipnl.db.entity.ExpenseType;
import com.taxipnl.taxipnl.di.qualifiers.DiskExecutor;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ExpenseTypesRepository {

    private ExpenseTypesDao dao;
    private Executor executor;

    @Inject
    public ExpenseTypesRepository(ExpenseTypesDao dao, @DiskExecutor Executor executor) {
        this.dao = dao;
        this.executor = executor;
    }

    public LiveData<List<ExpenseType>> getAll() {
        return dao.getAll();
    }

    public LiveData<List<ExpenseType>> getAllDispensable() {
        return dao.getAllDispensable();
    }

    public LiveData<List<ExpenseType>> getAllDispensablePlusToll() {
        return dao.getAllDispensablePlusToll();
    }

    public LiveData<ExpenseType> get(@Nullable Integer id) {
        return id != null ? dao.get(id) : new MutableLiveData<>();
    }

    public LiveData<ExpenseType> get(String name) {
        return dao.get(name);
    }

    public void create(ExpenseType expenseType) {
        executor.execute(() -> dao.insert(expenseType));
    }

    public void update(int id, String name) {
        executor.execute(() -> dao.update(id, name));
    }

    public void update(List<ExpenseType> expenseTypes) {
        executor.execute(() -> dao.update(expenseTypes));
    }

    public LiveData<Integer> delete(int id) {
        MutableLiveData<Integer> numberOfAffectedRows = new MutableLiveData<>();
        executor.execute(() -> numberOfAffectedRows.postValue(dao.delete(id)));
        return numberOfAffectedRows;
    }
}