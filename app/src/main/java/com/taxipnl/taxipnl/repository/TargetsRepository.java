package com.taxipnl.taxipnl.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.db.dao.TargetsDao;
import com.taxipnl.taxipnl.db.entity.Target;
import com.taxipnl.taxipnl.di.qualifiers.DiskExecutor;
import com.taxipnl.taxipnl.util.DateTimeUtils;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TargetsRepository {

    private TargetsDao dao;
    private Executor executor;

    @Inject
    public TargetsRepository(TargetsDao dao, @DiskExecutor Executor executor) {
        this.dao = dao;
        this.executor = executor;
    }

    public LiveData<Double> getTargetActingOn(@Nullable LocalDate date) {
        if (date == null) {
            return new MutableLiveData<>();
        }
        return dao.getLatestTargetBefore(date.atStartOfDay());
    }

    @Deprecated
    public LiveData<Double> getPeriodTarget(LocalDate startDate, LocalDate endDate) {
        MediatorLiveData<Double> result = new MediatorLiveData<>();
        executor.execute(() -> {
            Target firstTarget = dao.getLatestTargetBefore_Sync(startDate.atStartOfDay());
            List<Target> targetsExceptFirst = dao.getTargetsInPeriod_Sync(startDate.atStartOfDay(), endDate.atStartOfDay());
            if (firstTarget == null && targetsExceptFirst.isEmpty()) {
                result.postValue(0D);
            }

            double currentDailyTarget = 0;
            if (firstTarget != null) {
                currentDailyTarget = firstTarget.getTarget() / 7;
            }

            int i = 0;
            LocalDate nextTargetDate = LocalDate.MAX;
            double nextDailyTarget = currentDailyTarget;
            if (i < targetsExceptFirst.size()) {
                Target nextTarget = targetsExceptFirst.get(i);
                nextTargetDate = nextTarget.getPeriodStartDateTime().toLocalDate();
                nextDailyTarget = nextTarget.getTarget() / 7;
            }

            double target = 0;
            LocalDate date = startDate;
            while (date.compareTo(endDate) <= 0) {
                target += currentDailyTarget;
                date = date.plusDays(1);
                if (date.compareTo(nextTargetDate) >= 0) {
                    currentDailyTarget = nextDailyTarget;
                    i++;
                    if (i < targetsExceptFirst.size()) {
                        Target nextTarget = targetsExceptFirst.get(i);
                        nextTargetDate = nextTarget.getPeriodStartDateTime().toLocalDate();
                        nextDailyTarget = nextTarget.getTarget() / 7;
                    } else {
                        nextTargetDate = LocalDate.MAX;
                        nextDailyTarget = currentDailyTarget;
                    }
                }
            }
            result.postValue(target);
        });
        return result;
    }

    public LiveData<Double> getCurrentTarget() {
        return dao.getLatestTarget();
    }

    public void setCurrentTarget(double target) {
        // Target's periods are in weeks.
        // Every target's period starts at the start of the week.
        // There can't be two targets on same date or on the same week.
        // Week's target = target of this week (if present) or target of latest week (otherwise).
        LocalDateTime weekStartDateTime = DateTimeUtils.getStartOfCurrentWeek();
        createOrUpdate(new Target(weekStartDateTime, target));
    }

    private void createOrUpdate(Target target) {
        executor.execute(() -> dao.insertOrUpdate(target));
    }
}