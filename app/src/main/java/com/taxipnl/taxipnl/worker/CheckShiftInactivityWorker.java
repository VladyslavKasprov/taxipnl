package com.taxipnl.taxipnl.worker;

import android.content.Context;
import android.support.annotation.NonNull;

import com.taxipnl.taxipnl.db.dao.JobsDao;
import com.taxipnl.taxipnl.db.dao.ShiftsDao;
import com.taxipnl.taxipnl.db.entity.Shift;
import com.taxipnl.taxipnl.db.pojo.JobWithProvider;
import com.taxipnl.taxipnl.util.constants.SortingMode;

import org.threeten.bp.LocalDateTime;

import java.util.List;

import javax.inject.Inject;

import androidx.work.Worker;
import androidx.work.WorkerParameters;

import static com.taxipnl.taxipnl.util.constants.Constants.CLOSE_SHIFT_AFTER_LAST_ACTION_MINUTES;
import static com.taxipnl.taxipnl.util.constants.Constants.SHIFT_INACTIVITY_MAX_HOURS;
import static org.threeten.bp.LocalDateTime.now;

public class CheckShiftInactivityWorker extends Worker {

    private ShiftsDao shiftsDao;
    private JobsDao jobsDao;

    @Inject
    public CheckShiftInactivityWorker(@NonNull Context context, @NonNull WorkerParameters workerParams,
                                      ShiftsDao shiftsDao, JobsDao jobsDao) {
        super(context, workerParams);
        this.shiftsDao = shiftsDao;
        this.jobsDao = jobsDao;
    }

    @NonNull
    @Override
    public Result doWork() {
        Shift latestShift = shiftsDao.getLatestShiftSynchronous();
        if (latestShift == null || latestShift.getEndDateTime() != null) {
            return Result.success();
        }

        LocalDateTime lastActionDateTime = latestShift.getStartDateTime();
        List<JobWithProvider> jobs = jobsDao.getShiftJobsSynchronous(latestShift.getId(), SortingMode.Jobs.BY_ENTERED_DATE_DESC);
        if (jobs != null && !jobs.isEmpty()) {
            lastActionDateTime = jobs.get(0).enteredDateTime;
        }

        LocalDateTime timeout = lastActionDateTime.plusHours(SHIFT_INACTIVITY_MAX_HOURS);
        if (now().isAfter(timeout)) {
            if (jobs == null || jobs.isEmpty()) {
                shiftsDao.delete(latestShift);
            } else {
                LocalDateTime endDateTime = lastActionDateTime.plusMinutes(CLOSE_SHIFT_AFTER_LAST_ACTION_MINUTES);
                shiftsDao.update(latestShift.getId(), endDateTime);
            }
        }

        return Result.success();
    }
}