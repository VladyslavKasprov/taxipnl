package com.taxipnl.taxipnl.worker;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;
import com.taxipnl.taxipnl.db.Converters;
import com.taxipnl.taxipnl.db.dao.SubscriptionDetailsDao;
import com.taxipnl.taxipnl.di.qualifiers.PackageName;

import org.threeten.bp.LocalDateTime;

import java.io.IOException;

import javax.inject.Inject;

import androidx.work.Worker;
import androidx.work.WorkerParameters;

import static com.taxipnl.taxipnl.util.constants.Billing.SUBSCRIPTION_SKU;
import static org.threeten.bp.LocalDateTime.now;

public class CheckSubscriptionExpirationWorker extends Worker {

    private SubscriptionDetailsDao dao;
    private AndroidPublisher publisher;
    private String packageName;

    @Inject
    public CheckSubscriptionExpirationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams,
                                             SubscriptionDetailsDao dao, AndroidPublisher publisher,
                                             @PackageName String packageName) {
        super(context, workerParams);
        this.dao = dao;
        this.publisher = publisher;
        this.packageName = packageName;
    }

    @NonNull
    @Override
    public Result doWork() {
        String purchaseToken = dao.getPurchaseTokenSync();
        LocalDateTime expiryDateTime = dao.getExpiryDateTimeSync();
        if (purchaseToken == null || expiryDateTime == null) {
            return Result.success();
        }

        if (now().isAfter(expiryDateTime)) {
            try {
                SubscriptionPurchase purchaseFull = publisher
                        .purchases()
                        .subscriptions()
                        .get(packageName, SUBSCRIPTION_SKU, purchaseToken)
                        .execute();
                expiryDateTime = Converters.toLocalDateTimeFromMilli(purchaseFull.getExpiryTimeMillis());
                dao.updateExpiryDateTime(expiryDateTime);
                if (now().isAfter(expiryDateTime)) {
                    dao.updateIsSubscriptionActive(false);
                }
            } catch (IOException ignored) {}
        }

        return Result.success();
    }
}
