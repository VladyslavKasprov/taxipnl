package com.taxipnl.taxipnl.base;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;

import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuResId;
import com.taxipnl.taxipnl.base.config.NavHostFragmentId;
import com.taxipnl.taxipnl.base.config.ThemeResId;

import javax.inject.Inject;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import dagger.android.support.DaggerAppCompatActivity;

import static androidx.navigation.ui.NavigationUI.onNavDestinationSelected;

@ThemeResId(0)
@LayoutResId(0)
@MenuResId(0)
@NavHostFragmentId(0)
public abstract class BaseActivity extends DaggerAppCompatActivity {
    private ThemeResId themeResId = getClass().getAnnotation(ThemeResId.class);
    private LayoutResId layoutResId = getClass().getAnnotation(LayoutResId.class);
    private MenuResId menuResId = getClass().getAnnotation(MenuResId.class);
    private NavHostFragmentId navHostFragmentId = getClass().getAnnotation(NavHostFragmentId.class);

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (themeResId.value() != 0) {
            setTheme(themeResId.value());
        }
        super.onCreate(savedInstanceState);
        if (layoutResId.value() != 0) {
            setContentView(layoutResId.value());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menuResId.value() != 0) {
            getMenuInflater().inflate(menuResId.value(), menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO customize animations. Currently framework overrides any specified animations
        if (navHostFragmentId.value() != 0) {
            return onNavDestinationSelected(item, findNavController()) || super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    public <T extends ViewModel> T getViewModel(Class<T> viewModelClass) {
        return ViewModelProviders.of(this, viewModelFactory).get(viewModelClass);
    }

    protected NavController findNavController() {
        if (navHostFragmentId.value() != 0) {
            return Navigation.findNavController(this, navHostFragmentId.value());
        }
        return null;
    }
}