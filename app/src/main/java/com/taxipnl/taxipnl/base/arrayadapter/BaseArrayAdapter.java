package com.taxipnl.taxipnl.base.arrayadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class BaseArrayAdapter<T> extends ArrayAdapter<T> {

    private Converter<T> converter;
    private Decorator itemDecorator;
    private Decorator dropDownItemDecorator;

    public BaseArrayAdapter(@NonNull Context context, @NonNull Converter<T> converter) {
        super(context, android.R.layout.simple_spinner_item);
        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.converter = converter;
    }

    public BaseArrayAdapter<T> setItemDecorator(Decorator itemDecorator) {
        this.itemDecorator = itemDecorator;
        return this;
    }

    public BaseArrayAdapter<T> setDropDownItemDecorator(Decorator dropDownItemDecorator) {
        this.dropDownItemDecorator = dropDownItemDecorator;
        return this;
    }

    private String getName(int position) {
        return converter.toString(getItem(position));
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setText(getName(position));
        if (itemDecorator != null) {
            itemDecorator.decorate(view);
        }
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setText(getName(position));
        if (dropDownItemDecorator != null) {
            dropDownItemDecorator.decorate(view);
        }
        return view;
    }

    public interface Converter<S> {
        String toString(S item);
    }

    public interface Decorator {
        void decorate(TextView view);
    }
}