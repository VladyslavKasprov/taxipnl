package com.taxipnl.taxipnl.base.arrayadapter;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.util.TypedValue;
import android.widget.TextView;

public class TextDecorator implements BaseArrayAdapter.Decorator {

    private int textColor;
    private int textSize;

    public TextDecorator(@NonNull Context context, @ColorRes int textColor, @DimenRes int textSize) {
        this.textColor = context.getResources().getColor(textColor);
        this.textSize = context.getResources().getDimensionPixelSize(textSize);
    }

    @Override
    public void decorate(TextView view) {
        view.setTextColor(textColor);
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
    }
}
