package com.taxipnl.taxipnl.base;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.taxipnl.taxipnl.base.config.LayoutResId;
import com.taxipnl.taxipnl.base.config.MenuConfig;
import com.taxipnl.taxipnl.base.config.MenuResId;
import com.taxipnl.taxipnl.main.MainActivityVM;

import javax.inject.Inject;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import dagger.android.support.DaggerFragment;

@LayoutResId(0)
@MenuConfig(inheritsParentsMenu = false)
@MenuResId(0)
public abstract class BaseFragment<B extends ViewDataBinding> extends DaggerFragment {
    private LayoutResId layoutResId = getClass().getAnnotation(LayoutResId.class);
    private MenuConfig menuConfig = getClass().getAnnotation(MenuConfig.class);
    private MenuResId menuResId = getClass().getAnnotation(MenuResId.class);

    protected B view;

    protected MainActivityVM mainModel;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainModel = getGlobalViewModel(MainActivityVM.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(!menuConfig.inheritsParentsMenu());

        if (layoutResId.value() != 0) {
            view = DataBindingUtil.inflate(inflater, layoutResId.value(), container, false);
            view.setLifecycleOwner(getViewLifecycleOwner());
            return view.getRoot();
        }
        return null;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (menuResId.value() != 0) {
            requireActivity().getMenuInflater().inflate(menuResId.value(), menu);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        view.unbind();
        view = null;
    }

    protected void bind(int id, Object variable) {
        boolean isBound = view.setVariable(id, variable);
        if (!isBound) {
            throw new IllegalArgumentException("Binding failed! Incorrect binding variable name!");
        }
        view.executePendingBindings();
    }

    protected <T extends ViewModel> T getGlobalViewModel(Class<T> viewModelClass) {
        return ViewModelProviders.of(requireActivity(), viewModelFactory).get(viewModelClass);
    }

    protected <T extends ViewModel> T getLocalViewModel(Class<T> viewModelClass) {
        return ViewModelProviders.of(this, viewModelFactory).get(viewModelClass);
    }

    protected NavController findNavController() {
        return Navigation.findNavController(getView());
    }

    protected void displayMessage(@Nullable @StringRes Integer messageResId) {
        if (messageResId != null) {
            Toast.makeText(requireContext(), messageResId, Toast.LENGTH_LONG).show();
        }
    }
}