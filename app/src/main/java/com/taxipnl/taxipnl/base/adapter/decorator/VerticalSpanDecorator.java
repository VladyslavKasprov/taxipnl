package com.taxipnl.taxipnl.base.adapter.decorator;

import android.view.ViewGroup;

import com.taxipnl.taxipnl.base.adapter.BaseAdapter;

public class VerticalSpanDecorator implements BaseAdapter.Decorator {

    private final int verticalSpanCount;

    public VerticalSpanDecorator(int verticalSpanCount) {
        this.verticalSpanCount = verticalSpanCount;
    }

    @Override
    public void decorate(BaseAdapter.ViewHolder viewHolder, ViewGroup parent) {
        if (verticalSpanCount > 0) {
            ViewGroup.LayoutParams layoutParams = viewHolder.itemView.getLayoutParams();
            layoutParams.height = parent.getHeight() / verticalSpanCount;
            viewHolder.itemView.setLayoutParams(layoutParams);
        }
    }
}