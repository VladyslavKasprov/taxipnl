package com.taxipnl.taxipnl.base;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;

import com.taxipnl.taxipnl.main.MainActivityVM;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatDialogFragment;

public class BaseDialogFragment extends DaggerAppCompatDialogFragment {

    protected MainActivityVM viewModel;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    public <T extends ViewModel> T getGlobalViewModel(Class<T> viewModelClass) {
        return ViewModelProviders.of(requireActivity(), viewModelFactory).get(viewModelClass);
    }

    public <T extends ViewModel> T getLocalViewModelOf(Fragment fragment, Class<T> viewModelClass) {
        return ViewModelProviders.of(fragment, viewModelFactory).get(viewModelClass);
    }
}