package com.taxipnl.taxipnl.base.adapter;

public interface ViewTypeFactory<T> {

    int getViewType(T item);
}