package com.taxipnl.taxipnl.base.adapter;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.ViewModel;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.taxipnl.taxipnl.BR;

import java.util.ArrayList;
import java.util.List;

public class BaseAdapter<T> extends RecyclerView.Adapter<BaseAdapter.ViewHolder<T>> {

    protected int[] headersViewTypes;
    protected ViewModel headerModel;
    protected LifecycleOwner headerModelLifecycleOwner;

    protected ViewTypeFactory<T> viewTypeFactory;
    protected ViewModel model;
    protected LifecycleOwner modelLifecycleOwner;
    protected List<T> items = new ArrayList<>();

    protected OnItemClickListener<T> handler;
    protected Decorator decorator;

    public BaseAdapter(ViewTypeFactory<T> viewTypeFactory) {
        this.viewTypeFactory = viewTypeFactory;
    }

    public BaseAdapter<T> setHeaders(int[] viewTypes, ViewModel model, LifecycleOwner lifecycleOwner) {
        this.headersViewTypes = viewTypes;
        this.headerModel = model;
        this.headerModelLifecycleOwner = lifecycleOwner;
        return this;
    }

    public BaseAdapter<T> setModel(ViewModel model, LifecycleOwner lifecycleOwner) {
        this.model = model;
        this.modelLifecycleOwner = lifecycleOwner;
        return this;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(@NonNull List<T> newItems) {
        DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return items.size();
            }

            @Override
            public int getNewListSize() {
                return newItems.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                return items.get(oldItemPosition).equals(newItems.get(newItemPosition));
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                return true;
            }
        }).dispatchUpdatesTo(this);

        items = newItems;
    }

    public BaseAdapter<T> setHandler(OnItemClickListener<T> handler) {
        this.handler = handler;
        return this;
    }

    public BaseAdapter<T> setDecorator(Decorator decorator) {
        this.decorator = decorator;
        return this;
    }

    @Override
    public int getItemCount() {
        return headersViewTypes == null ? items.size() : headersViewTypes.length + items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (headersViewTypes != null && position < headersViewTypes.length) {
            return headersViewTypes[position];
        }
        int realPosition = headersViewTypes == null ? position : position - headersViewTypes.length;
        return viewTypeFactory.getViewType(items.get(realPosition));
    }

    @NonNull
    @Override
    public ViewHolder<T> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding view = DataBindingUtil.inflate(inflater, viewType, parent, false);
        if (view == null) {
            throw new IllegalArgumentException("Binding for this viewType doesn't exist");
        }

        ViewHolder<T> viewHolder = new ViewHolder<>(view);

        if (decorator != null) {
            decorator.decorate(viewHolder, parent);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder<T> viewHolder, int position) {
        if (headersViewTypes != null && position < headersViewTypes.length) {
            viewHolder.bind(headerModel, headerModelLifecycleOwner);
            return;
        }

        int realPosition = headersViewTypes == null ? position : position - headersViewTypes.length;
        T item = items.get(realPosition);
        viewHolder.bind(item);
        if (model != null) {
            viewHolder.bind(model, modelLifecycleOwner);
        }
        if (handler != null) {
            viewHolder.itemView.setOnClickListener(v -> handler.onItemClick(item));
        }
    }

    public static class ViewHolder<T> extends RecyclerView.ViewHolder {

        ViewDataBinding view;

        public ViewHolder(ViewDataBinding view) {
            super(view.getRoot());
            this.view = view;
        }

        public void bind(ViewModel model, LifecycleOwner lifecycleOwner) {
            boolean isBound = view.setVariable(BR.model, model);
            if (!isBound) {
                throw new IllegalArgumentException("Can't bind model to the ViewHolder");
            }
            view.setLifecycleOwner(lifecycleOwner);
            view.executePendingBindings();
        }

        public void bind(T item) {
            boolean isBound = view.setVariable(BR.item, item);
            if (!isBound) {
                throw new IllegalArgumentException("Can't bind item to the ViewHolder");
            }
            view.executePendingBindings();
        }
    }

    public interface OnItemClickListener<T> {
        void onItemClick(T item);
    }

    public interface Decorator {
        void decorate(BaseAdapter.ViewHolder viewHolder, ViewGroup parent);
    }
}