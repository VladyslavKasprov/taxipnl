package com.taxipnl.taxipnl.view;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class CenteredDividerItemDecoration extends DividerItemDecoration {

    public CenteredDividerItemDecoration(Context context) {
        super(context);
    }

    public CenteredDividerItemDecoration(Context context, int orientation, boolean isLastDrawn) {
        super(context, orientation, isLastDrawn);
    }

    public CenteredDividerItemDecoration(Context context, int orientation, boolean isLastDrawn, @DimenRes int paddingResId) {
        super(context, orientation, isLastDrawn, paddingResId);
    }

    @Override
    protected int getOffsetForHorizontalDivider() {
        return divider.getIntrinsicHeight()/2;
    }

    @Override
    protected int getOffsetForVerticalDivider() {
        return divider.getIntrinsicWidth()/2;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {}
}