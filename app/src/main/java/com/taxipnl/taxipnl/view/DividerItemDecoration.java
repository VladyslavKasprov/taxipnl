package com.taxipnl.taxipnl.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.taxipnl.taxipnl.R;

public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    protected Drawable divider;
    protected int orientation = LinearLayout.HORIZONTAL;
    protected final Rect bounds = new Rect();
    protected int paddingEnd = 0;
    protected int paddingStart = 0;
    protected boolean isLastDrawn = true;

    public DividerItemDecoration(Context context) {
        divider = ContextCompat.getDrawable(context, R.drawable.divider);
    }

    public DividerItemDecoration(Context context, int orientation, boolean isLastDrawn) {
        this(context);

        if (orientation != LinearLayout.HORIZONTAL && orientation != LinearLayout.VERTICAL) {
            throw new IllegalArgumentException(
                    "Invalid orientation. It should be either HORIZONTAL or VERTICAL");
        }
        this.orientation = orientation;
        this.isLastDrawn = isLastDrawn;
    }

    public DividerItemDecoration(Context context, int orientation, boolean isLastDrawn, @DimenRes int paddingResId) {
        this(context, orientation, isLastDrawn);

        int padding = context.getResources().getDimensionPixelOffset(paddingResId);
        paddingEnd = padding;
        paddingStart = padding;
    }

    public DividerItemDecoration withPaddingEnd(Context context, @DimenRes int paddingResId) {
        paddingEnd = context.getResources().getDimensionPixelOffset(paddingResId);
        return this;
    }

    public DividerItemDecoration withPaddingStart(Context context, @DimenRes int paddingResId) {
        paddingStart = context.getResources().getDimensionPixelOffset(paddingResId);
        return this;
    }

    @Override
    public void onDraw(@NonNull Canvas canvas, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        if (parent.getLayoutManager() == null || divider == null) {
            return;
        }
        if (orientation == LinearLayout.HORIZONTAL) {
            drawHorizontal(canvas, parent);
        } else {
            drawVertical(canvas, parent);
        }
    }

    protected void drawHorizontal(Canvas canvas, RecyclerView parent) {
        canvas.save();

        int left;
        int right;
        if (parent.getClipToPadding()) {
            left = parent.getPaddingLeft();
            right = parent.getWidth() - parent.getPaddingRight();
            canvas.clipRect(left, parent.getPaddingTop(), right, parent.getHeight() - parent.getPaddingBottom());
        } else {
            left = 0;
            right = parent.getWidth();
        }
        right -= paddingEnd;
        left += paddingStart;

        int childCount = parent.getChildCount();
        int spanCount = getSpanCount(parent);

        for(int i = 0; i < childCount; ++i) {
            if (isHorizontalDividerDrawnFor(i, spanCount, childCount)) {
                View child = parent.getChildAt(i);
                parent.getDecoratedBoundsWithMargins(child, bounds);
                int bottom = bounds.bottom + Math.round(child.getTranslationY()) + getOffsetForHorizontalDivider();
                int top = bottom - divider.getIntrinsicHeight();
                divider.setBounds(left, top, right, bottom);
                divider.draw(canvas);
            }
        }

        canvas.restore();
    }

    protected boolean isVerticalDividerDrawnFor(int position, int spanCount) {
        return isLastDrawn || !isLastInRow(position, spanCount);
    }

    protected static boolean isLastInRow(int position, int spanCount) {
        return (position + 1) % spanCount == 0;
    }

    protected int getOffsetForHorizontalDivider() {
        return 0;
    }

    protected void drawVertical(Canvas canvas, RecyclerView parent) {
        canvas.save();

        int childCount = parent.getChildCount();
        int spanCount = getSpanCount(parent);

        for(int i = 0; i < childCount; ++i) {
            if (isVerticalDividerDrawnFor(i, spanCount)) {
                View child = parent.getChildAt(i);
                parent.getLayoutManager().getDecoratedBoundsWithMargins(child, bounds);
                int bottom = bounds.bottom - paddingEnd;
                int right = bounds.right + Math.round(child.getTranslationX()) + getOffsetForVerticalDivider();
                int left = right - divider.getIntrinsicWidth();
                int top = bounds.top + paddingStart;
                divider.setBounds(left, top, right, bottom);
                divider.draw(canvas);
            }
        }

        canvas.restore();
    }

    protected boolean isHorizontalDividerDrawnFor(int position, int spanCount, int totalCount) {
        return isLastDrawn || !isInTheLastRow(position, spanCount, totalCount);
    }

    protected static boolean isInTheLastRow(int position, int spanCount, int totalCount) {
        return position >= totalCount - spanCount;
    }

    protected int getOffsetForVerticalDivider() {
        return 0;
    }

    protected static int getSpanCount(RecyclerView parent) {
        final RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager) {
            return ((GridLayoutManager) layoutManager).getSpanCount();
        } else {
            return 1;
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (this.divider == null) {
            outRect.set(0, 0, 0, 0);
        } else {
            if (orientation == LinearLayout.HORIZONTAL) {
                outRect.set(0, 0, 0, divider.getIntrinsicHeight());
            } else {
                outRect.set(0, 0, divider.getIntrinsicWidth(), 0);
            }
        }
    }
}