package com.taxipnl.taxipnl.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.widget.TextView;

import com.taxipnl.taxipnl.R;

public class CustomizedToolbar extends Toolbar {

    Drawable navIcon;
    @IdRes int titleTextViewId;

    public CustomizedToolbar(Context context) {
        super(context);
        initialize(context, null);
    }

    public CustomizedToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    public CustomizedToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context, attrs);
    }

    private void initialize(Context context, @Nullable AttributeSet attrs) {
        if (attrs == null) {
            return;
        }
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomizedToolbar);
        @DrawableRes int navIconId = a.getResourceId(R.styleable.CustomizedToolbar_navigationIcon, 0);
        titleTextViewId = a.getResourceId(R.styleable.CustomizedToolbar_titleTextView, 0);
        a.recycle();

        if (navIconId != 0) {
            navIcon = AppCompatResources.getDrawable(getContext(), navIconId);
        }
    }

    @Nullable
    private TextView getTitleTextView() {
        return titleTextViewId != 0 ? getRootView().findViewById(titleTextViewId) : null;
    }

    @Override
    public void setTitle(CharSequence title) {
        if (getTitleTextView() != null) {
            getTitleTextView().setText(title);
        } else {
            super.setTitle(title);
        }
    }

    @Override
    public CharSequence getTitle() {
        return getTitleTextView() != null ? getTitleTextView().getText().toString() : super.getTitle();
    }

    // TODO remove workaround  - with release of official support for setting hamburger icon
    @Override
    public void setNavigationIcon(@Nullable Drawable icon) {
        super.setNavigationIcon(
                navIcon == null ? icon :
                        icon == null ? null : navIcon
        );
    }
}