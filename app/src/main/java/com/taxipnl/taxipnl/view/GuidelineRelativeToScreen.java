package com.taxipnl.taxipnl.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.constraint.Guideline;
import android.util.AttributeSet;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.util.AndroidUtils;

/**
 * <p>
 *     This Guideline will convert guidePercent into guideBegin in the following way:
 * </p>
 * <center>
 *     guideBegin = guidePercent * screenSize (with orientation)
 * </center>
 * <p>
 *     This is useful for laying out views relatively to absolute screen size in case when parent
 *     ConstraintLayout is contained in a scroll container.
 * </p>
 */
public class GuidelineRelativeToScreen extends Guideline {

    private int orientation;
    private float guidePercent;

    public GuidelineRelativeToScreen(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    public GuidelineRelativeToScreen(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context, attrs);
    }

    public GuidelineRelativeToScreen(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(context, attrs);
    }

    @SuppressLint("CustomViewStyleable")
    private void initialize(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ConstraintLayout_Layout);
        orientation = a.getInt(R.styleable.ConstraintLayout_Layout_android_orientation, ConstraintLayout.LayoutParams.HORIZONTAL);
        guidePercent = a.getFloat(R.styleable.ConstraintLayout_Layout_layout_constraintGuide_percent, ConstraintSet.UNSET);
        a.recycle();
    }

    @Override
    protected void onAttachedToWindow() {
        setGuidelinePercent(ConstraintSet.UNSET);
        setGuidelineBegin(guidePercent);
        super.onAttachedToWindow();
    }

    private void setGuidelineBegin(float ratio) {
        Activity activity = (Activity) getContext();

        int screenSizePixels = orientation == ConstraintLayout.LayoutParams.HORIZONTAL ?
                AndroidUtils.getWindowHeightPixels(activity) : AndroidUtils.getWindowWidthPixels(activity);
        int guidelineBeginPixels = (int) (ratio * screenSizePixels);

        setGuidelineBegin(guidelineBeginPixels);
    }
}