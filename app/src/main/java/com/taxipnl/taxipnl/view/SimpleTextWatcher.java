package com.taxipnl.taxipnl.view;

import android.text.Editable;
import android.text.TextWatcher;

public abstract class SimpleTextWatcher implements TextWatcher {

    private boolean ignore = false;

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable sequence) {
        if (ignore) {
            return;
        }
        ignore = true;
        update(sequence);
        ignore = false;
    }

    protected abstract void update(Editable sequence);
}