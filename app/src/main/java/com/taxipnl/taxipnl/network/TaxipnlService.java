package com.taxipnl.taxipnl.network;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TaxipnlService {

    String BASE_URL = " http://taxipnl.com/";

    @POST("feedback.php")
    Call<Void> sendFeedback(@Query("d") String feedback);
}