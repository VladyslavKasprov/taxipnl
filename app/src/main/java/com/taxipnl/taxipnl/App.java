package com.taxipnl.taxipnl;

import android.app.Activity;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.Fragment;

import com.jakewharton.threetenabp.AndroidThreeTen;
import com.taxipnl.taxipnl.di.DaggerAppComponent;
import com.taxipnl.taxipnl.di.worker.DaggerWorkerFactory;
import com.taxipnl.taxipnl.worker.CheckShiftInactivityWorker;
import com.taxipnl.taxipnl.worker.CheckSubscriptionExpirationWorker;

import javax.inject.Inject;

import androidx.work.Configuration;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.support.HasSupportFragmentInjector;

import static androidx.work.PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS;
import static androidx.work.PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS;
import static com.taxipnl.taxipnl.util.constants.Constants.CHECK_SHIFT_INACTIVITY_UNIQUE_WORK_NAME;
import static com.taxipnl.taxipnl.util.constants.Constants.CHECK_SUBSCRIPTION_EXPIRATION_UNIQUE_WORK_NAME;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class App extends MultiDexApplication implements HasActivityInjector, HasSupportFragmentInjector {

    DaggerAppComponent appComponent;

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingSupportFragmentInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = (DaggerAppComponent) DaggerAppComponent.builder().application(this).create(this);
        appComponent.inject(this);

        AndroidThreeTen.init(this);

        initializeWorkManager();
        enqueueWorkers();
    }

    private void initializeWorkManager() {
        Configuration configuration = new Configuration.Builder()
                .setWorkerFactory(new DaggerWorkerFactory(appComponent))
                .build();
        WorkManager.initialize(this, configuration);
    }

    private void enqueueWorkers() {
        PeriodicWorkRequest checkSubscriptionExpirationWorkRequest = new PeriodicWorkRequest.Builder(
                CheckSubscriptionExpirationWorker.class,
                MIN_PERIODIC_INTERVAL_MILLIS, MILLISECONDS,
                MIN_PERIODIC_FLEX_MILLIS, MILLISECONDS
        ).setConstraints(
                new Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build()
        ).build();
        WorkManager.getInstance()
                .enqueueUniquePeriodicWork(
                        CHECK_SUBSCRIPTION_EXPIRATION_UNIQUE_WORK_NAME,
                        ExistingPeriodicWorkPolicy.REPLACE,
                        checkSubscriptionExpirationWorkRequest
                );

        PeriodicWorkRequest checkShiftInactivityWorkRequest = new PeriodicWorkRequest.Builder(
                CheckShiftInactivityWorker.class,
                MIN_PERIODIC_INTERVAL_MILLIS, MILLISECONDS
        ).build();
        WorkManager.getInstance()
                .enqueueUniquePeriodicWork(
                        CHECK_SHIFT_INACTIVITY_UNIQUE_WORK_NAME,
                        ExistingPeriodicWorkPolicy.REPLACE,
                        checkShiftInactivityWorkRequest
                );
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingSupportFragmentInjector;
    }
}