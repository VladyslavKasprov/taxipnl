package com.taxipnl.taxipnl.db.pojo;

import android.arch.persistence.room.ColumnInfo;

public class AuthorizationStatus {

    @ColumnInfo(name = "is_registered")
    public boolean isRegistered;

    @ColumnInfo(name = "is_logged_in")
    public boolean isLoggedIn;
}