package com.taxipnl.taxipnl.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import org.threeten.bp.LocalDateTime;

import java.io.Serializable;

@Entity(
        tableName = "Jobs",
        foreignKeys = {
                @ForeignKey(
                        entity = Provider.class,
                        parentColumns = "id",
                        childColumns = "provider_id",
                        onDelete = ForeignKey.RESTRICT
                ),
                @ForeignKey(
                        entity = Shift.class,
                        parentColumns = "id",
                        childColumns = "shift_id",
                        onDelete = ForeignKey.RESTRICT
                )
        }
)
public class Job implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "entered_timestamp", index = true)
    private LocalDateTime enteredDateTime;

    @ColumnInfo(name = "fare")
    private double fare;

    @ColumnInfo(name = "toll")
    private double toll;

    @ColumnInfo(name = "commission")
    private double commission;

    @ColumnInfo(name = "provider_id", index = true)
    private int providerId;

    @ColumnInfo(name = "shift_id", index = true)
    private int shiftId;

    public Job(LocalDateTime enteredDateTime, double fare, double toll, double commission, int providerId, int shiftId) {
        this.enteredDateTime = enteredDateTime;
        this.fare = fare;
        this.toll = toll;
        this.commission = commission;
        this.providerId = providerId;
        this.shiftId = shiftId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getEnteredDateTime() {
        return enteredDateTime;
    }

    public void setEnteredDateTime(LocalDateTime enteredDateTime) {
        this.enteredDateTime = enteredDateTime;
    }

    public double getFare() {
        return fare;
    }

    public void setFare(double fare) {
        this.fare = fare;
    }

    public double getToll() {
        return toll;
    }

    public void setToll(double toll) {
        this.toll = toll;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    public int getShiftId() {
        return shiftId;
    }

    public void setShiftId(int shiftId) {
        this.shiftId = shiftId;
    }
}