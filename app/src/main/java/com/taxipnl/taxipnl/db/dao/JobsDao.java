package com.taxipnl.taxipnl.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.taxipnl.taxipnl.db.entity.Expense;
import com.taxipnl.taxipnl.db.entity.Job;
import com.taxipnl.taxipnl.db.pojo.JobWithProvider;
import com.taxipnl.taxipnl.util.constants.Constants;
import com.taxipnl.taxipnl.util.constants.SortingMode;

import org.threeten.bp.LocalDateTime;

import java.util.List;
import java.util.Set;

@Dao
public abstract class JobsDao {

    private static final String SELECT_JOB_WITH_PROVIDER =
            "SELECT j.id, j.entered_timestamp, j.fare, j.toll, j.commission, j.provider_id, p.name, p.payment_method " +
                    "FROM Jobs j, Providers p " +
                    "WHERE j.provider_id = p.id ";

    private static final String ORDER_BY_SORTING_MODE =
            "ORDER BY CASE WHEN :sortingMode = " + SortingMode.Jobs.BY_ENTERED_DATE_DESC + " THEN entered_timestamp END DESC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Jobs.BY_ENTERED_DATE_ASC + " THEN entered_timestamp END ASC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Jobs.BY_PROVIDER_ORDER_DESC + " THEN order_number END DESC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Jobs.BY_PROVIDER_ORDER_DESC + " THEN entered_timestamp END DESC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Jobs.BY_PROVIDER_ORDER_ASC + " THEN order_number END ASC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Jobs.BY_PROVIDER_ORDER_ASC + " THEN entered_timestamp END DESC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Jobs.BY_FARE_DESC + " THEN fare END DESC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Jobs.BY_FARE_ASC + " THEN fare END ASC";

    private static final String SELECT_SHIFTS_IDS_STARTED_IN_PERIOD =
            "(SELECT id FROM Shifts WHERE start_timestamp BETWEEN :startDateTime AND :endDateTime) ";

    @Query(SELECT_JOB_WITH_PROVIDER +
            ORDER_BY_SORTING_MODE)
    public abstract LiveData<List<JobWithProvider>> getAll(@SortingMode.Jobs int sortingMode);

    @Query(SELECT_JOB_WITH_PROVIDER +
            "AND j.shift_id IN " + SELECT_SHIFTS_IDS_STARTED_IN_PERIOD +
            ORDER_BY_SORTING_MODE)
    public abstract LiveData<List<JobWithProvider>> get(LocalDateTime startDateTime, LocalDateTime endDateTime,
                                                        @SortingMode.Jobs int sortingMode);

    @Query(SELECT_JOB_WITH_PROVIDER +
            "AND p.id IN (:providersIds) " +
            ORDER_BY_SORTING_MODE)
    public abstract LiveData<List<JobWithProvider>> get(Set<Integer> providersIds, @SortingMode.Jobs int sortingMode);

    @Query(SELECT_JOB_WITH_PROVIDER +
            "AND p.id IN (:providersIds) " +
            "AND j.shift_id IN " + SELECT_SHIFTS_IDS_STARTED_IN_PERIOD +
            ORDER_BY_SORTING_MODE)
    public abstract LiveData<List<JobWithProvider>> get(LocalDateTime startDateTime, LocalDateTime endDateTime,
                                                        Set<Integer> providersIds, @SortingMode.Jobs int sortingMode);

    @Query(SELECT_JOB_WITH_PROVIDER +
            "AND j.shift_id = :shiftId " +
            ORDER_BY_SORTING_MODE)
    public abstract LiveData<List<JobWithProvider>> getShiftJobs(int shiftId, @SortingMode.Jobs int sortingMode);

    @Query(SELECT_JOB_WITH_PROVIDER +
            "AND j.shift_id = :shiftId " +
            ORDER_BY_SORTING_MODE)
    public abstract List<JobWithProvider> getShiftJobsSynchronous(int shiftId, @SortingMode.Jobs int sortingMode);

    @Query(SELECT_JOB_WITH_PROVIDER +
            "AND j.shift_id IN (:shiftsIds) " +
            "ORDER BY entered_timestamp DESC")
    public abstract LiveData<List<JobWithProvider>> getShiftsJobs(List<Integer> shiftsIds);

    @Query(SELECT_JOB_WITH_PROVIDER +
            "AND j.id = :id")
    public abstract LiveData<JobWithProvider> get(int id);

    @Query("SELECT sum((fare - toll) * (1 - commission / 100)) " +
            "FROM Jobs " +
            "WHERE shift_id IN " + SELECT_SHIFTS_IDS_STARTED_IN_PERIOD)
    public abstract LiveData<Double> sumIncome(LocalDateTime startDateTime, LocalDateTime endDateTime);


    //    Transactions with job and job expense synchronisation logic
    @Transaction
    public void insert(LocalDateTime enteredDateTime, double fare, double toll, int providerId, int shiftId) {
        double commission = getProvidersCommission(providerId);
        int jobId = (int) insertJob(new Job(enteredDateTime, fare, toll, commission, providerId, shiftId));
        afterJobInsert(jobId, enteredDateTime, toll);
    }

    @Transaction
    public void update(int id, double fare, double toll) {
        updateJob(id, fare, toll);
        afterJobUpdate(id, toll);
    }

    @Transaction
    public void update(int id, double fare, double toll, int providerId) {
        updateJob(id, fare, toll, providerId);
        afterJobUpdate(id, toll);
    }

    @Query("DELETE FROM Jobs WHERE id = :id")
    public abstract void delete(int id);

    //    Job and job expense synchronisation logic
    private void afterJobInsert(int jobId, LocalDateTime enteredDateTime, double toll) {
        if (toll > 0) {
            insertExpense(new Expense(enteredDateTime, toll, Constants.EXPENSE_TYPE_ID_TOLL, jobId));
        }
    }

    private void afterJobUpdate(int jobId, double toll) {
        Job job = getJob(jobId);
        if (job == null) {
            return;
        }

        Expense jobExpense = getExpense(jobId);
        if (jobExpense != null) {
            if (toll > 0) {
                updateExpense(jobId, toll);
            } else {
                deleteExpense(jobId);
            }
        } else {
            afterJobInsert(jobId, job.getEnteredDateTime(), toll);
        }
    }

    //    Job queries
    @Query("SELECT * FROM Jobs WHERE id = :id")
    abstract Job getJob(int id);

    @Query("SELECT commission FROM Providers WHERE id = :providerId")
    abstract Double getProvidersCommission(int providerId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract long insertJob(Job job);

    @Query("UPDATE Jobs SET fare = :fare, toll = :toll WHERE id = :id")
    abstract void updateJob(int id, double fare, double toll);

    @Query("UPDATE Jobs " +
            "SET fare = :fare, toll = :toll, commission = (SELECT commission FROM Providers WHERE id = :providerId), provider_id = :providerId " +
            "WHERE id = :id")
    abstract void updateJob(int id, double fare, double toll, int providerId);

    //    Job and job expense synchronisation queries
    @Query("SELECT * FROM Expenses WHERE job_id = :jobId")
    abstract Expense getExpense(int jobId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract void insertExpense(Expense expense);

    @Query("UPDATE Expenses SET amount = :toll WHERE job_id = :jobId")
    abstract void updateExpense(int jobId, double toll);

    @Query("DELETE FROM Expenses WHERE job_id = :jobId")
    abstract void deleteExpense(int jobId);
}