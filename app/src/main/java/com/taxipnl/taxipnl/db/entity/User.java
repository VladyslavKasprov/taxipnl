package com.taxipnl.taxipnl.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Currency;

@Entity(
        tableName = "User"
)
public class User {

    @PrimaryKey
    private int LOCK = 0;

    @ColumnInfo(name = "is_logged_in")
    private boolean isLoggedIn;

    @NonNull
    @ColumnInfo(name = "password")
    private String password;

    @NonNull
    @ColumnInfo(name = "currencyCode")
    private Currency currency;

    public User(boolean isLoggedIn, @NonNull String password, @NonNull Currency currency) {
        this.isLoggedIn = isLoggedIn;
        this.password = password;
        this.currency = currency;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

    public int getLOCK() {
        return LOCK;
    }

    public void setLOCK(int LOCK) {}

    @NonNull
    public String getPassword() {
        return password;
    }

    public void setPassword(@NonNull String password) {
        this.password = password;
    }

    @NonNull
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(@NonNull Currency currency) {
        this.currency = currency;
    }
}