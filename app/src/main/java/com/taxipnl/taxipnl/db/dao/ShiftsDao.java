package com.taxipnl.taxipnl.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.taxipnl.taxipnl.db.entity.Shift;

import org.threeten.bp.LocalDateTime;

import java.util.List;

@Dao
public interface ShiftsDao {

    String SELECT_LATEST_SHIFT = "SELECT * FROM Shifts ORDER BY start_timestamp DESC LIMIT 1";

    @Query(SELECT_LATEST_SHIFT)
    LiveData<Shift> getLatestShift();

    @Query(SELECT_LATEST_SHIFT)
    Shift getLatestShiftSynchronous();

    @Query("SELECT * FROM Shifts WHERE end_timestamp IS NOT NULL ORDER BY start_timestamp DESC LIMIT 1")
    LiveData<Shift> getLastClosedShift();

    @Query("SELECT * FROM Shifts WHERE start_timestamp >= :startDateTime")
    LiveData<List<Shift>> getShiftsStartedAfter(LocalDateTime startDateTime);

    @Query("SELECT * FROM Shifts WHERE start_timestamp BETWEEN :startDateTime AND :endDateTime")
    LiveData<List<Shift>> getShiftsStartedInPeriod(LocalDateTime startDateTime, LocalDateTime endDateTime);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Shift shift);

    @Update
    int update(Shift shift);

    @Query("UPDATE Shifts SET end_timestamp = :endDateTime WHERE id = :id")
    void update(int id, LocalDateTime endDateTime);

    @Delete
    int delete(Shift shift);
}