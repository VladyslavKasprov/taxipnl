package com.taxipnl.taxipnl.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

/**
 * This form of LiveData is used to avoid false positive notifications that are being fired by
 * Room TRIGGER's on SQLite database whenever queried tables get changed in any form.
 * For example selecting one column from one table will result in notifications if any other columns gets updated.
 *
 * @param <T> The type of data hold by this instance
 */
class DistinctLiveData<T> extends MediatorLiveData<T> {

    DistinctLiveData(LiveData<T> source) {
        addSource(source, new Observer<T>() {

            private boolean isInitialized = false;
            private T lastObject = null;

            @Override
            public void onChanged(@Nullable T object) {
                if (!isInitialized) {
                    isInitialized = true;
                    lastObject = object;
                    postValue(lastObject);
                } else if ((object == null && lastObject != null) || object != lastObject) {
                    lastObject = object;
                    postValue(lastObject);
                }
            }
        });
    }
}