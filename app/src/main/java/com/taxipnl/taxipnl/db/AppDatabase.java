package com.taxipnl.taxipnl.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.taxipnl.taxipnl.db.dao.ExpenseTypesDao;
import com.taxipnl.taxipnl.db.dao.ExpensesDao;
import com.taxipnl.taxipnl.db.dao.JobsDao;
import com.taxipnl.taxipnl.db.dao.PrepopulateDatabaseDao;
import com.taxipnl.taxipnl.db.dao.ProvidersDao;
import com.taxipnl.taxipnl.db.dao.ShiftsDao;
import com.taxipnl.taxipnl.db.dao.SubscriptionDetailsDao;
import com.taxipnl.taxipnl.db.dao.TargetsDao;
import com.taxipnl.taxipnl.db.dao.TipsDao;
import com.taxipnl.taxipnl.db.dao.UserDao;
import com.taxipnl.taxipnl.db.entity.Expense;
import com.taxipnl.taxipnl.db.entity.ExpenseType;
import com.taxipnl.taxipnl.db.entity.Job;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.db.entity.Shift;
import com.taxipnl.taxipnl.db.entity.SubscriptionDetails;
import com.taxipnl.taxipnl.db.entity.Target;
import com.taxipnl.taxipnl.db.entity.Tip;
import com.taxipnl.taxipnl.db.entity.User;

@Database(
        entities = {
                SubscriptionDetails.class,
                User.class,
                Shift.class,
                Provider.class,
                Job.class,
                ExpenseType.class,
                Expense.class,
                Target.class,
                Tip.class
        },
        version = 1,
        exportSchema = false
)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract PrepopulateDatabaseDao prepopulateDatabaseDao();

    public abstract SubscriptionDetailsDao subscriptionDetailsDao();

    public abstract UserDao userDao();

    public abstract ShiftsDao shiftsDao();

    public abstract ProvidersDao providersDao();

    public abstract JobsDao jobsDao();

    public abstract ExpenseTypesDao expenseTypesDao();

    public abstract ExpensesDao expensesDao();

    public abstract TargetsDao targetsDao();

    public abstract TipsDao tipsDao();
}