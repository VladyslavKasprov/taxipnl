package com.taxipnl.taxipnl.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.taxipnl.taxipnl.db.entity.Target;

import org.threeten.bp.LocalDateTime;

import java.util.List;

@Dao
public interface TargetsDao {

    @Query("SELECT target " +
            "FROM Targets " +
            "WHERE period_start_timestamp <= :dateTime " +
            "ORDER BY period_start_timestamp DESC " +
            "LIMIT 1")
    LiveData<Double> getLatestTargetBefore(LocalDateTime dateTime);

    @Query("SELECT * " +
            "FROM Targets " +
            "WHERE period_start_timestamp <= :dateTime " +
            "ORDER BY period_start_timestamp DESC " +
            "LIMIT 1")
    Target getLatestTargetBefore_Sync(LocalDateTime dateTime);

    @Query("SELECT * " +
            "FROM Targets " +
            "WHERE period_start_timestamp BETWEEN :startDateTime AND :endDateTime " +
            "ORDER BY period_start_timestamp ASC")
    List<Target> getTargetsInPeriod_Sync(LocalDateTime startDateTime, LocalDateTime endDateTime);

    @Query("SELECT target FROM Targets ORDER BY period_start_timestamp DESC LIMIT 1")
    LiveData<Double> getLatestTarget();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrUpdate(Target target);
}