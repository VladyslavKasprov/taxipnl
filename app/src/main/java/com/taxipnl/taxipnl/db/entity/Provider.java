package com.taxipnl.taxipnl.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(
        tableName = "Providers"
)
public class Provider implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "commission")
    private double commission;

    @ColumnInfo(name = "payment_method")
    private PaymentMethod paymentMethod;

    @ColumnInfo(name = "order_number", index = true)
    private int orderNumber;

    @ColumnInfo(name = "is_street_provider", index = true)
    private boolean isStreetProvider;

    @ColumnInfo(name = "is_selectable", index = true)
    private boolean isSelectable;

    public Provider() {}

    @Ignore
    public Provider(int id, String name, double commission, PaymentMethod paymentMethod, int orderNumber,
                    boolean isStreetProvider, boolean isSelectable) {
        this.id = id;
        this.name = name;
        this.commission = commission;
        this.paymentMethod = paymentMethod;
        this.orderNumber = orderNumber;
        this.isStreetProvider = isStreetProvider;
        this.isSelectable = isSelectable;
    }

    @Ignore
    public Provider(String name, double commission, PaymentMethod paymentMethod, int orderNumber,
                    boolean isStreetProvider, boolean isSelectable) {
        this.name = name;
        this.commission = commission;
        this.paymentMethod = paymentMethod;
        this.orderNumber = orderNumber;
        this.isStreetProvider = isStreetProvider;
        this.isSelectable = isSelectable;
    }

    @Ignore
    public Provider(String name, double commission, PaymentMethod paymentMethod, boolean isSelectable) {
        this.name = name;
        this.commission = commission;
        this.paymentMethod = paymentMethod;
        this.isSelectable = isSelectable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public boolean isStreetProvider() {
        return isStreetProvider;
    }

    public void setStreetProvider(boolean streetProvider) {
        isStreetProvider = streetProvider;
    }

    public boolean isSelectable() {
        return isSelectable;
    }

    public void setSelectable(boolean selectable) {
        isSelectable = selectable;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Provider)) {
            return false;
        }
        Provider other = (Provider) obj;
        return id == other.id
                && name.equalsIgnoreCase(other.name)
                && commission == other.commission
                && paymentMethod == other.paymentMethod
                && orderNumber == other.orderNumber;
    }
}