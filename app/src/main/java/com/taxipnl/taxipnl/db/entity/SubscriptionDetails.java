package com.taxipnl.taxipnl.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import org.threeten.bp.LocalDateTime;

@Entity(
        tableName = "SubscriptionDetails"
)
public class SubscriptionDetails {

    @PrimaryKey
    private int LOCK = 0;

    @ColumnInfo(name = "is_subscription_active")
    private Boolean isSubscriptionActive;

    @ColumnInfo(name = "purchase_token")
    private String purchaseToken;

    @ColumnInfo(name = "expiry_timestamp")
    private LocalDateTime expiryDateTime;

    @ColumnInfo(name = "price")
    private String price;

    public SubscriptionDetails(LocalDateTime expiryDateTime, String price) {
        this.expiryDateTime = expiryDateTime;
        this.price = price;
    }

    public int getLOCK() {
        return LOCK;
    }

    public void setLOCK(int LOCK) {}

    public Boolean isSubscriptionActive() {
        return isSubscriptionActive;
    }

    public void setSubscriptionActive(Boolean subscriptionActive) {
        isSubscriptionActive = subscriptionActive;
    }

    public String getPurchaseToken() {
        return purchaseToken;
    }

    public void setPurchaseToken(String purchaseToken) {
        this.purchaseToken = purchaseToken;
    }

    public LocalDateTime getExpiryDateTime() {
        return expiryDateTime;
    }

    public void setExpiryDateTime(LocalDateTime expiryDateTime) {
        this.expiryDateTime = expiryDateTime;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}