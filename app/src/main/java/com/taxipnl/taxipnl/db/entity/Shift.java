package com.taxipnl.taxipnl.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import org.threeten.bp.LocalDateTime;

@Entity(
        tableName = "Shifts"
)
public class Shift {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "start_timestamp", index = true)
    private LocalDateTime startDateTime;

    @ColumnInfo(name = "end_timestamp", index = true)
    private LocalDateTime endDateTime;

    @Ignore
    public Shift(@NonNull LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Shift(@NonNull LocalDateTime startDateTime, LocalDateTime endDateTime) {
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(@NonNull LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }
}