package com.taxipnl.taxipnl.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.taxipnl.taxipnl.db.entity.Expense;
import com.taxipnl.taxipnl.db.pojo.ExpenseTypeWithAmount;
import com.taxipnl.taxipnl.db.pojo.ExpenseWithType;
import com.taxipnl.taxipnl.util.constants.Constants;
import com.taxipnl.taxipnl.util.constants.SortingMode;

import org.threeten.bp.LocalDateTime;

import java.util.List;
import java.util.Set;

@Dao
public abstract class ExpensesDao {

    private static final String SELECT_EXPENSE_WITH_TYPE =
            "SELECT e.id, e.entered_timestamp, e.amount, e.expense_type_id, et.name " +
                    "FROM Expenses e, ExpenseTypes et " +
                    "WHERE e.expense_type_id = et.id ";

    private static final String ORDER_BY_SORTING_MODE =
            "ORDER BY CASE WHEN :sortingMode = " + SortingMode.Expenses.BY_ENTERED_DATE_DESC + " THEN entered_timestamp END DESC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Expenses.BY_ENTERED_DATE_ASC + " THEN entered_timestamp END ASC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Expenses.BY_EXPENSE_TYPE_ORDER_DESC + " THEN order_number END DESC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Expenses.BY_EXPENSE_TYPE_ORDER_DESC + " THEN entered_timestamp END DESC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Expenses.BY_EXPENSE_TYPE_ORDER_ASC + " THEN order_number END ASC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Expenses.BY_EXPENSE_TYPE_ORDER_ASC + " THEN entered_timestamp END DESC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Expenses.BY_AMOUNT_DESC + " THEN amount END DESC, " +
                    "CASE WHEN :sortingMode = " + SortingMode.Expenses.BY_AMOUNT_ASC + " THEN amount END ASC";

    @Query("SELECT total(amount) " +
            "FROM Expenses " +
            "WHERE entered_timestamp BETWEEN :startDateTime AND :endDateTime")
    public abstract LiveData<Double> getAmount(LocalDateTime startDateTime, LocalDateTime endDateTime);

    @Query("SELECT et.name, total(e.amount) as amount " +
            "FROM ExpenseTypes et " +
            "LEFT JOIN Expenses e " +
            "ON et.id = e.expense_type_id " +
            "AND e.entered_timestamp BETWEEN :startDateTime AND :endDateTime " +
            "GROUP BY et.id ")
    public abstract LiveData<List<ExpenseTypeWithAmount>> getExpenseAmountByType(LocalDateTime startDateTime, LocalDateTime endDateTime);

    @Query(SELECT_EXPENSE_WITH_TYPE +
            ORDER_BY_SORTING_MODE)
    public abstract LiveData<List<ExpenseWithType>> getAll(@SortingMode.Expenses int sortingMode);

    @Query(SELECT_EXPENSE_WITH_TYPE +
            "AND e.entered_timestamp BETWEEN :startDateTime AND :endDateTime " +
            ORDER_BY_SORTING_MODE)
    public abstract LiveData<List<ExpenseWithType>> get(LocalDateTime startDateTime, LocalDateTime endDateTime,
                                                        @SortingMode.Jobs int sortingMode);

    @Query(SELECT_EXPENSE_WITH_TYPE +
            "AND et.id IN (:expenseTypesIds) " +
            ORDER_BY_SORTING_MODE)
    public abstract LiveData<List<ExpenseWithType>> get(Set<Integer> expenseTypesIds, @SortingMode.Jobs int sortingMode);

    @Query(SELECT_EXPENSE_WITH_TYPE +
            "AND et.id IN (:expenseTypesIds) " +
            "AND e.entered_timestamp BETWEEN :startDateTime AND :endDateTime " +
            ORDER_BY_SORTING_MODE)
    public abstract LiveData<List<ExpenseWithType>> get(LocalDateTime startDateTime, LocalDateTime endDateTime,
                                                        Set<Integer> expenseTypesIds, @SortingMode.Jobs int sortingMode);

    @Query(SELECT_EXPENSE_WITH_TYPE +
            "AND e.id = :id")
    public abstract LiveData<ExpenseWithType> get(int id);

    @Query("SELECT sum(amount) " +
            "FROM Expenses " +
            "WHERE entered_timestamp BETWEEN :startDateTime AND :endDateTime")
    public abstract LiveData<Double> sumAmount(LocalDateTime startDateTime, LocalDateTime endDateTime);


    //    Transactions with job and job expense synchronisation logic
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(Expense expense);

    @Transaction
    public void update(int id, double amount) {
        updateExpense(id, amount);
        afterExpenseUpdate(id, amount);
    }

    @Transaction
    public void update(int id, double amount, int expenseTypeId) {
        updateExpense(id, amount, expenseTypeId);
        afterExpenseUpdate(id, amount);
    }

    @Transaction
    public void delete(int id) {
        beforeExpenseDelete(id);
        deleteExpense(id);
    }

    //    Job and job expense synchronisation logic
    private void afterExpenseUpdate(int id, double amount) {
        Expense expense = getExpense(id);
        if (expense == null) {
            return;
        }

        if (expense.getJobId() != null) {
            if (expense.getExpenseTypeId() == Constants.EXPENSE_TYPE_ID_TOLL) {
                updateJob(expense.getJobId(), amount);
            } else {
                updateJob(expense.getJobId(), 0);
                unbindExpense(id);
            }
        }
    }

    private void beforeExpenseDelete(int id) {
        Expense expense = getExpense(id);
        if (expense == null) {
            return;
        }

        if (expense.getJobId() != null) {
            updateJob(expense.getJobId(), 0);
        }
    }

    //    Expenses queries
    @Query("SELECT * FROM Expenses WHERE id = :id")
    abstract Expense getExpense(int id);

    @Query("UPDATE Expenses SET amount = :amount WHERE id = :id")
    abstract void updateExpense(int id, double amount);

    @Query("UPDATE Expenses SET amount = :amount, expense_type_id = :expenseTypeId WHERE id = :id")
    abstract void updateExpense(int id, double amount, int expenseTypeId);

    @Query("DELETE FROM Expenses WHERE id = :id")
    abstract void deleteExpense(int id);

    //    Job and job expense synchronisation queries
    @Query("UPDATE Jobs SET toll = :amount WHERE id = :jobId")
    abstract void updateJob(int jobId, double amount);

    @Query("UPDATE Expenses SET job_id = NULL WHERE id = :id")
    abstract void unbindExpense(int id);
}