package com.taxipnl.taxipnl.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.taxipnl.taxipnl.db.entity.User;
import com.taxipnl.taxipnl.db.pojo.AuthorizationStatus;

import java.util.Currency;

@Dao
public abstract class UserDao {

    @Query("SELECT EXISTS(SELECT 1 FROM User WHERE LOCK = 0) as is_registered, " +
            "(SELECT is_logged_in FROM User WHERE LOCK = 0) as is_logged_in")
    public abstract LiveData<AuthorizationStatus> getAuthorizationStatus();

    @Query("SELECT password FROM User")
    public abstract LiveData<String> getPassword();

    @Query("SELECT currencyCode FROM User")
    public abstract LiveData<Currency> getCurrency();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertOrReplace(User user);

    @Query("UPDATE User SET is_logged_in = :isLoggedIn")
    public abstract void updateIsLoggedIn(boolean isLoggedIn);

    @Query("UPDATE User SET password = :password")
    public abstract void updatePassword(String password);

    @Query("UPDATE User SET currencyCode = :currency")
    public abstract void updateCurrency(Currency currency);
}