package com.taxipnl.taxipnl.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import org.threeten.bp.LocalDateTime;

@Dao
public abstract class SubscriptionDetailsDao {

    @Query("SELECT is_subscription_active FROM SubscriptionDetails")
    public abstract LiveData<Boolean> getIsSubscriptionActive();

    @Query("SELECT purchase_token FROM SubscriptionDetails")
    public abstract String getPurchaseTokenSync();

    @Query("SELECT expiry_timestamp FROM SubscriptionDetails")
    public abstract LocalDateTime getExpiryDateTimeSync();

    @Query("SELECT price FROM SubscriptionDetails")
    public abstract LiveData<String> getPrice();

    @Query("UPDATE SubscriptionDetails SET purchase_token = :purchaseToken")
    public abstract void updatePurchaseToken(String purchaseToken);

    @Query("UPDATE SubscriptionDetails SET expiry_timestamp = :expiryDateTime")
    public abstract void updateExpiryDateTime(LocalDateTime expiryDateTime);

    @Query("UPDATE SubscriptionDetails SET price = :price")
    public abstract void updatePrice(String price);

    @Query("UPDATE SubscriptionDetails SET is_subscription_active = :isSubscriptionActive")
    public abstract void updateIsSubscriptionActive(boolean isSubscriptionActive);

    @Query("UPDATE SubscriptionDetails SET is_subscription_active = NULL")
    public abstract void clearIsSubscriptionActive();
}