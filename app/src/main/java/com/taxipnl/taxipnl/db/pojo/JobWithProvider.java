package com.taxipnl.taxipnl.db.pojo;

import android.arch.persistence.room.ColumnInfo;

import com.taxipnl.taxipnl.db.entity.PaymentMethod;

import org.threeten.bp.LocalDateTime;

import java.io.Serializable;

public class JobWithProvider implements Serializable {

    public int id;

    @ColumnInfo(name = "entered_timestamp")
    public LocalDateTime enteredDateTime;

    @ColumnInfo(name = "fare")
    public double fare;

    @ColumnInfo(name = "toll")
    public double toll;

    @ColumnInfo(name = "commission")
    public double commission;

    @ColumnInfo(name = "provider_id")
    public int providerId;

    @ColumnInfo(name = "name")
    public String providerName;

    @ColumnInfo(name = "payment_method")
    public PaymentMethod providerPaymentMethod;
}