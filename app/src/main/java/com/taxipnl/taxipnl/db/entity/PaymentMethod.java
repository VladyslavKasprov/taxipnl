package com.taxipnl.taxipnl.db.entity;

import android.content.Context;

import com.taxipnl.taxipnl.R;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public enum PaymentMethod implements Serializable {

    CARD(0, R.string.payment_method_card, R.mipmap.ic_card_white),
    CASH(1, R.string.payment_method_cash, R.mipmap.ic_cash_white),
    PAY(2, R.string.payment_method_pay, R.mipmap.ic_card_white);

    private static final Map<Integer, PaymentMethod> values = new HashMap<>();
    static {
        for (PaymentMethod paymentMethod : PaymentMethod.values()) {
            if (values.put(paymentMethod.id, paymentMethod) != null) {
                throw new IllegalArgumentException("PaymentMethod duplicate id: " + paymentMethod.id);
            }
        }
    }

    public static PaymentMethod getById(int id) {
        return values.get(id);
    }

    private int id;
    private int nameResId;
    private int iconResId;

    PaymentMethod(int id, int nameResId, int iconResId) {
        this.id = id;
        this.nameResId = nameResId;
        this.iconResId = iconResId;
    }

    public int getId() {
        return id;
    }

    public int getNameResId() {
        return nameResId;
    }

    public int getIconResId() {
        return iconResId;
    }

    public String getName(Context context) {
        return context.getString(nameResId);
    }
}