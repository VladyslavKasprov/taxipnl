package com.taxipnl.taxipnl.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Nullable;

import org.threeten.bp.LocalDateTime;

import java.io.Serializable;

@Entity(
        tableName = "Expenses",
        foreignKeys = {
                @ForeignKey(
                        entity = ExpenseType.class,
                        parentColumns = "id",
                        childColumns = "expense_type_id",
                        onDelete = ForeignKey.RESTRICT
                ),
                @ForeignKey(
                        entity = Job.class,
                        parentColumns = "id",
                        childColumns = "job_id",
                        onDelete = ForeignKey.CASCADE
                )
        }
)
public class Expense implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "entered_timestamp")
    private LocalDateTime enteredDateTime;

    @ColumnInfo(name = "amount")
    private double amount;

    @ColumnInfo(name = "expense_type_id", index = true)
    private int expenseTypeId;

    @Nullable
    @ColumnInfo(name = "job_id", index = true)
    private Integer jobId;

    public Expense(LocalDateTime enteredDateTime, double amount, int expenseTypeId) {
        this.enteredDateTime = enteredDateTime;
        this.amount = amount;
        this.expenseTypeId = expenseTypeId;
    }

    @Ignore
    public Expense(LocalDateTime enteredDateTime, double amount, int expenseTypeId, @Nullable Integer jobId) {
        this.enteredDateTime = enteredDateTime;
        this.amount = amount;
        this.expenseTypeId = expenseTypeId;
        this.jobId = jobId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getEnteredDateTime() {
        return enteredDateTime;
    }

    public void setEnteredDateTime(LocalDateTime enteredDateTime) {
        this.enteredDateTime = enteredDateTime;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getExpenseTypeId() {
        return expenseTypeId;
    }

    public void setExpenseTypeId(int expenseTypeId) {
        this.expenseTypeId = expenseTypeId;
    }

    @Nullable
    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(@Nullable Integer jobId) {
        this.jobId = jobId;
    }
}