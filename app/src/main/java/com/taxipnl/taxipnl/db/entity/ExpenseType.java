package com.taxipnl.taxipnl.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(
        tableName = "ExpenseTypes"
)
public class ExpenseType implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "order_number", index = true)
    private int orderNumber;

    @ColumnInfo(name = "is_indispensable", index = true)
    private boolean isIndispensable;

    public ExpenseType() {}

    @Ignore
    public ExpenseType(int id, String name, int orderNumber, boolean isIndispensable) {
        this.id = id;
        this.name = name;
        this.orderNumber = orderNumber;
        this.isIndispensable = isIndispensable;
    }

    @Ignore
    public ExpenseType(String name, int orderNumber, boolean isIndispensable) {
        this.name = name;
        this.orderNumber = orderNumber;
        this.isIndispensable = isIndispensable;
    }

    @Ignore
    public ExpenseType(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public boolean isIndispensable() {
        return isIndispensable;
    }

    public void setIndispensable(boolean indispensable) {
        isIndispensable = indispensable;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ExpenseType)) {
            return false;
        }
        ExpenseType other = (ExpenseType) obj;
        return id == other.id
                && name.equalsIgnoreCase(other.name)
                && orderNumber == other.orderNumber;
    }
}