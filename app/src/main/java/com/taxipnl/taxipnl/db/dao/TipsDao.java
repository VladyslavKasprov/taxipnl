package com.taxipnl.taxipnl.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.taxipnl.taxipnl.db.entity.Tip;

import org.threeten.bp.LocalDateTime;

@Dao
public interface TipsDao {

    @Query("SELECT sum(amount) FROM Tips WHERE entered_timestamp BETWEEN :startDateTime AND :endDateTime")
    LiveData<Double> getAmount(LocalDateTime startDateTime, LocalDateTime endDateTime);

    @Insert
    void insert(Tip tip);
}