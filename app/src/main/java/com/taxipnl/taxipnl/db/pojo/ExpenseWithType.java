package com.taxipnl.taxipnl.db.pojo;

import android.arch.persistence.room.ColumnInfo;

import org.threeten.bp.LocalDateTime;

import java.io.Serializable;

public class ExpenseWithType implements Serializable {

    public int id;

    @ColumnInfo(name = "entered_timestamp")
    public LocalDateTime enteredDateTime;

    @ColumnInfo(name = "amount")
    public double amount;

    @ColumnInfo(name = "expense_type_id")
    public int expenseTypeId;

    @ColumnInfo(name = "name")
    public String expenseTypeName;
}