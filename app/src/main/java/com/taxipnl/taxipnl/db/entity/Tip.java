package com.taxipnl.taxipnl.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import org.threeten.bp.LocalDateTime;

@Entity(
        tableName = "Tips"
)
public class Tip {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "entered_timestamp")
    private LocalDateTime enteredDateTime;

    @ColumnInfo(name = "amount")
    private double amount;

    public Tip(LocalDateTime enteredDateTime, double amount) {
        this.enteredDateTime = enteredDateTime;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getEnteredDateTime() {
        return enteredDateTime;
    }

    public void setEnteredDateTime(LocalDateTime enteredDateTime) {
        this.enteredDateTime = enteredDateTime;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}