package com.taxipnl.taxipnl.db.pojo;

import android.arch.persistence.room.ColumnInfo;

public class ExpenseTypeWithAmount {

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "amount")
    public double amount;
}
