package com.taxipnl.taxipnl.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import org.threeten.bp.LocalDateTime;

@Entity(
        tableName = "Targets",
        indices = @Index(
                value = "period_start_timestamp",
                unique = true
        )
)
public class Target {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "period_start_timestamp")
    private LocalDateTime periodStartDateTime;

    @ColumnInfo(name = "target")
    private double target;

    public Target(LocalDateTime periodStartDateTime, double target) {
        this.periodStartDateTime = periodStartDateTime;
        this.target = target;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getPeriodStartDateTime() {
        return periodStartDateTime;
    }

    public void setPeriodStartDateTime(LocalDateTime periodStartDateTime) {
        this.periodStartDateTime = periodStartDateTime;
    }

    public double getTarget() {
        return target;
    }

    public void setTarget(double target) {
        this.target = target;
    }
}