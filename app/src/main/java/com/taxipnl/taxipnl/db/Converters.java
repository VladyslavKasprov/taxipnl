package com.taxipnl.taxipnl.db;

import android.arch.persistence.room.TypeConverter;

import com.taxipnl.taxipnl.db.entity.PaymentMethod;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneOffset;

import java.util.Currency;

public class Converters {

    private static ZoneOffset getOffsetNow() {
        return OffsetDateTime.now().getOffset();
    }

    public static LocalDateTime toLocalDateTimeFromMilli(Long epochMilli) {
        return epochMilli == null ? null : LocalDateTime.ofInstant(Instant.ofEpochMilli(epochMilli), getOffsetNow());
    }

    @TypeConverter
    public static Long fromLocalDateTime(LocalDateTime dateTime) {
        return dateTime == null ? null : dateTime.toEpochSecond(getOffsetNow());
    }

    @TypeConverter
    public static LocalDateTime toLocalDateTime(Long epochSecond) {
        return epochSecond == null ? null : LocalDateTime.ofEpochSecond(epochSecond, 0, getOffsetNow());
    }


    @TypeConverter
    public static String fromDouble(double value) {
        return String.valueOf(value);
    }

    @TypeConverter
    public static double toDouble(String value) {
        return Double.valueOf(value);
    }


    @TypeConverter
    public static PaymentMethod toPaymentMethod(int id) {
        return PaymentMethod.getById(id);
    }

    @TypeConverter
    public static int fromPaymentMethod(PaymentMethod paymentMethod) {
        return paymentMethod == null ? -1 : paymentMethod.getId();
    }


    @TypeConverter
    public static Currency toCurrency(String currencyCode) {
        return Currency.getInstance(currencyCode);
    }

    @TypeConverter
    public static String fromCurrency(Currency currency) {
        return currency == null ? "EUR" : currency.getCurrencyCode();
    }
}