package com.taxipnl.taxipnl.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.taxipnl.taxipnl.db.entity.ExpenseType;
import com.taxipnl.taxipnl.util.constants.Constants;

import java.util.List;

@Dao
public interface ExpenseTypesDao {

    @Query("SELECT * FROM ExpenseTypes ORDER BY order_number")
    LiveData<List<ExpenseType>> getAll();

    @Query("SELECT * FROM ExpenseTypes WHERE is_indispensable = 0 ORDER BY order_number")
    LiveData<List<ExpenseType>> getAllDispensable();

    @Query("SELECT * " +
            "FROM ExpenseTypes " +
            "WHERE is_indispensable = 0 " +
            "OR id = " + Constants.EXPENSE_TYPE_ID_TOLL +
            " ORDER BY order_number")
    LiveData<List<ExpenseType>> getAllDispensablePlusToll();

    @Query("SELECT * FROM ExpenseTypes WHERE id = :id")
    LiveData<ExpenseType> get(int id);

    @Query("SELECT * FROM ExpenseTypes WHERE name LIKE :name")
    LiveData<ExpenseType> get(String name);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(ExpenseType expenseType);

    @Query("UPDATE ExpenseTypes SET name = :name WHERE id = :id")
    int update(int id, String name);

    @Update
    int update(List<ExpenseType> expenseTypes);

    @Query("DELETE " +
            "FROM ExpenseTypes " +
            "WHERE id = :id " +
            "AND id NOT IN (SELECT expense_type_id FROM Expenses) " +
            "AND is_indispensable = 0")
    int delete(int id);
}