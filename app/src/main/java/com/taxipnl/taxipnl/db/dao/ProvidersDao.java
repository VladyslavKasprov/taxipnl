package com.taxipnl.taxipnl.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.taxipnl.taxipnl.db.entity.PaymentMethod;
import com.taxipnl.taxipnl.db.entity.Provider;

import java.util.List;

@Dao
public interface ProvidersDao {

    @Query("SELECT * FROM Providers ORDER BY order_number")
    LiveData<List<Provider>> getAll();

    @Query("SELECT * FROM Providers WHERE is_street_provider = 0 ORDER BY order_number")
    LiveData<List<Provider>> getAllExceptStreet();

    @Query("SELECT * FROM Providers WHERE is_selectable = 1 ORDER BY order_number")
    LiveData<List<Provider>> getAllSelectable();

    @Query("SELECT * FROM Providers WHERE id = :id")
    LiveData<Provider> get(int id);

    @Query("SELECT * FROM Providers WHERE name LIKE :name AND payment_method = :paymentMethod")
    LiveData<Provider> get(String name, PaymentMethod paymentMethod);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Provider provider);

    @Query("UPDATE Providers SET name = :name, commission = :commission, is_selectable =:isSelectable WHERE id = :id")
    int update(int id, String name, double commission, boolean isSelectable);

    @Update
    int update(List<Provider> providers);

    @Query("DELETE " +
            "FROM Providers " +
            "WHERE id = :id " +
            "AND id NOT IN (SELECT provider_id FROM Jobs) " +
            "AND is_street_provider = 0")
    int delete(int id);
}