package com.taxipnl.taxipnl.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Transaction;

import com.taxipnl.taxipnl.db.entity.ExpenseType;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.db.entity.SubscriptionDetails;
import com.taxipnl.taxipnl.db.entity.Target;

import java.util.List;

@Dao
public abstract class PrepopulateDatabaseDao {

    @Transaction
    public void prepopulateDatabase(SubscriptionDetails subscriptionDetails,
                                    List<Provider> providers,
                                    List<ExpenseType> expenseTypes,
                                    Target initialTarget) {
        insertDefaultSubscriptionDetails(subscriptionDetails);
        insertDefaultProviders(providers);
        insertDefaultExpenseTypes(expenseTypes);
        insertInitialTarget(initialTarget);
    }

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract void insertDefaultSubscriptionDetails(SubscriptionDetails subscriptionDetails);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract void insertDefaultProviders(List<Provider> providers);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract void insertDefaultExpenseTypes(List<ExpenseType> expenseTypes);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract void insertInitialTarget(Target target);
}