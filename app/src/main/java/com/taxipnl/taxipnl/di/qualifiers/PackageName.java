package com.taxipnl.taxipnl.di.qualifiers;

import javax.inject.Qualifier;

@Qualifier
public @interface PackageName {}