package com.taxipnl.taxipnl.di;

import android.app.Application;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.support.annotation.NonNull;

import com.taxipnl.taxipnl.R;
import com.taxipnl.taxipnl.db.AppDatabase;
import com.taxipnl.taxipnl.db.dao.ExpenseTypesDao;
import com.taxipnl.taxipnl.db.dao.ExpensesDao;
import com.taxipnl.taxipnl.db.dao.JobsDao;
import com.taxipnl.taxipnl.db.dao.ProvidersDao;
import com.taxipnl.taxipnl.db.dao.ShiftsDao;
import com.taxipnl.taxipnl.db.dao.SubscriptionDetailsDao;
import com.taxipnl.taxipnl.db.dao.TargetsDao;
import com.taxipnl.taxipnl.db.dao.TipsDao;
import com.taxipnl.taxipnl.db.dao.UserDao;
import com.taxipnl.taxipnl.db.entity.ExpenseType;
import com.taxipnl.taxipnl.db.entity.PaymentMethod;
import com.taxipnl.taxipnl.db.entity.Provider;
import com.taxipnl.taxipnl.db.entity.SubscriptionDetails;
import com.taxipnl.taxipnl.db.entity.Target;
import com.taxipnl.taxipnl.di.qualifiers.DiskExecutor;
import com.taxipnl.taxipnl.util.DateTimeUtils;

import org.threeten.bp.LocalDateTime;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.taxipnl.taxipnl.util.constants.Billing.DEFAULT_SUBSCRIPTION_PRICE;
import static com.taxipnl.taxipnl.util.constants.Constants.EXPENSE_TYPE_ID_FUEL;
import static com.taxipnl.taxipnl.util.constants.Constants.EXPENSE_TYPE_ID_TOLL;
import static com.taxipnl.taxipnl.util.constants.Constants.EXPENSE_TYPE_ORDER_NUMBER_FUEL;
import static com.taxipnl.taxipnl.util.constants.Constants.EXPENSE_TYPE_ORDER_NUMBER_TOLL;
import static com.taxipnl.taxipnl.util.constants.Constants.INITIAL_WEEKLY_TARGET;
import static com.taxipnl.taxipnl.util.constants.Constants.PROVIDER_ID_STREET_CARD;
import static com.taxipnl.taxipnl.util.constants.Constants.PROVIDER_ID_STREET_CASH;
import static com.taxipnl.taxipnl.util.constants.Constants.PROVIDER_ORDER_NUMBER_STREET_CARD;
import static com.taxipnl.taxipnl.util.constants.Constants.PROVIDER_ORDER_NUMBER_STREET_CASH;

@Module
class DatabaseModule {

    private AppDatabase database;

    private void prepopulateDatabase(Application application) {
        SubscriptionDetails subscriptionDetails = new SubscriptionDetails(LocalDateTime.MIN, DEFAULT_SUBSCRIPTION_PRICE);

        List<Provider> providers = new LinkedList<>();
        providers.add(new Provider(PROVIDER_ID_STREET_CARD,"", 0, PaymentMethod.CARD,
                PROVIDER_ORDER_NUMBER_STREET_CARD, true, false));
        providers.add(new Provider(PROVIDER_ID_STREET_CASH,"", 0, PaymentMethod.CASH,
                PROVIDER_ORDER_NUMBER_STREET_CASH, true, false));
        int orderNumber = 1;
        String mytaxi = application.getString(R.string.provider_mytaxi);
        providers.add(new Provider(mytaxi, 12, PaymentMethod.CARD,
                orderNumber++, false, true));
        providers.add(new Provider(mytaxi, 12, PaymentMethod.CASH,
                orderNumber++, false, true));
        providers.add(new Provider(mytaxi, 0, PaymentMethod.PAY,
                orderNumber++, false, true));
        providers.add(new Provider(application.getString(R.string.provider_uber), 12, PaymentMethod.CARD,
                orderNumber++, false, true));
        String lynk = application.getString(R.string.provider_lynk);
        providers.add(new Provider(lynk, 10, PaymentMethod.CARD,
                orderNumber++, false, true));
        providers.add(new Provider(lynk, 0, PaymentMethod.CASH,
                orderNumber++, false, true));
        String cab2000 = application.getString(R.string.provider_cab2000);
        providers.add(new Provider(cab2000, 0, PaymentMethod.CARD,
                orderNumber++, false, true));
        providers.add(new Provider(cab2000, 0, PaymentMethod.CASH,
                orderNumber++, false, true));
        String vipTaxi = application.getString(R.string.provider_vip_taxi);
        providers.add(new Provider(vipTaxi, 12, PaymentMethod.CARD,
                orderNumber++, false, true));
        providers.add(new Provider(vipTaxi, 0, PaymentMethod.CASH,
                orderNumber++, false, true));

        List<ExpenseType> expenseTypes = new LinkedList<>();
        expenseTypes.add(new ExpenseType(EXPENSE_TYPE_ID_FUEL, application.getString(R.string.expense_type_fuel),
                EXPENSE_TYPE_ORDER_NUMBER_FUEL,true));
        expenseTypes.add(new ExpenseType(EXPENSE_TYPE_ID_TOLL, application.getString(R.string.expense_type_toll),
                EXPENSE_TYPE_ORDER_NUMBER_TOLL, true));
        orderNumber = 1;
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_car_wash_vallet),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_repairs),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_insurance),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_tax),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_license),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_nct),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_permits),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_radio_taxi_rental),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_mobile),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_loan_interest),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_union_bookkeeping),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_branding),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_misc),
                orderNumber++, false));
        expenseTypes.add(new ExpenseType(application.getString(R.string.expense_type_motor_depreciation),
                orderNumber++, false));

        Target initialTarget = new Target(DateTimeUtils.getStartOfCurrentWeek(), INITIAL_WEEKLY_TARGET);

        database.prepopulateDatabaseDao().prepopulateDatabase(
                subscriptionDetails,
                providers,
                expenseTypes,
                initialTarget
        );
    }

    @Provides
    @Singleton
    AppDatabase appDatabase(Application application, @DiskExecutor Executor executor) {
        AtomicBoolean isDatabaseBeingCreated = new AtomicBoolean(false);
        database = Room.databaseBuilder(application, AppDatabase.class, "taxipnl.db")
                .addCallback(new RoomDatabase.Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        isDatabaseBeingCreated.set(true);
                        executor.execute(() -> {
                            prepopulateDatabase(application);
                            database = null;
                        });
                    }

                    @Override
                    public void onOpen(@NonNull SupportSQLiteDatabase db) {
                        super.onOpen(db);
                        if (!isDatabaseBeingCreated.get()) {
                            database = null;
                        }
                    }
                })
                .build();
        return database;
    }

    @Provides
    @DiskExecutor
    @Singleton
    Executor diskExecutor() {
        return Executors.newFixedThreadPool(3);
    }

    @Provides
    @Singleton
    SubscriptionDetailsDao subscriptionDetailsDao(AppDatabase database) {
        return database.subscriptionDetailsDao();
    }

    @Provides
    @Singleton
    UserDao userDao(AppDatabase database) {
        return database.userDao();
    }

    @Provides
    @Singleton
    ProvidersDao providersDao(AppDatabase database) {
        return database.providersDao();
    }

    @Provides
    @Singleton
    ShiftsDao shiftsDao(AppDatabase database) {
        return database.shiftsDao();
    }

    @Provides
    @Singleton
    JobsDao jobsDao(AppDatabase database) {
        return database.jobsDao();
    }

    @Provides
    @Singleton
    ExpenseTypesDao expenseTypesDao(AppDatabase database) {
        return database.expenseTypesDao();
    }

    @Provides
    @Singleton
    ExpensesDao expensesDao(AppDatabase database) {
        return database.expensesDao();
    }

    @Provides
    @Singleton
    TargetsDao targetsDao(AppDatabase database) {
        return database.targetsDao();
    }

    @Provides
    @Singleton
    TipsDao tipsDao(AppDatabase database) {
        return database.tipsDao();
    }
}