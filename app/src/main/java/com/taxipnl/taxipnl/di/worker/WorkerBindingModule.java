package com.taxipnl.taxipnl.di.worker;

import com.taxipnl.taxipnl.worker.CheckShiftInactivityWorker;
import com.taxipnl.taxipnl.worker.CheckSubscriptionExpirationWorker;

import androidx.work.Worker;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class WorkerBindingModule {

    @Binds
    @WorkerKey(CheckSubscriptionExpirationWorker.class)
    @IntoMap
    abstract Worker checkSubscriptionExpirationWorker(CheckSubscriptionExpirationWorker worker);

    @Binds
    @WorkerKey(CheckShiftInactivityWorker.class)
    @IntoMap
    abstract Worker checkShiftInactivityWorker(CheckShiftInactivityWorker worker);
}
