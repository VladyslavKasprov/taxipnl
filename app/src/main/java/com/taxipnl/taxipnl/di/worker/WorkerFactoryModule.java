package com.taxipnl.taxipnl.di.worker;

import dagger.Module;

@Module(subcomponents = WorkerFactorySubcomponent.class)
public class WorkerFactoryModule {}