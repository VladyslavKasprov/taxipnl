package com.taxipnl.taxipnl.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.taxipnl.taxipnl.BuildConfig;
import com.taxipnl.taxipnl.network.TaxipnlService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
class NetworkModule {

    @Provides
    @Singleton
    public TaxipnlService taxipnlService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor()
                .setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.HEADERS);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .create();
        Converter.Factory converterFactory = GsonConverterFactory.create(gson);

        return new Retrofit.Builder()
                .client(client)
                .baseUrl(TaxipnlService.BASE_URL)
                .addConverterFactory(converterFactory)
                .build()
                .create(TaxipnlService.class);
    }
}