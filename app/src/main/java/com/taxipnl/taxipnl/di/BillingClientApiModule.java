package com.taxipnl.taxipnl.di;

import com.android.billingclient.api.BillingClient.SkuType;
import com.android.billingclient.api.SkuDetailsParams;

import java.util.Collections;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.taxipnl.taxipnl.util.constants.Billing.SUBSCRIPTION_SKU;

@Module
public class BillingClientApiModule {

    @Provides
    @Singleton
    public SkuDetailsParams skuDetailsParams() {
        return SkuDetailsParams.newBuilder()
                .setSkusList(Collections.singletonList(SUBSCRIPTION_SKU))
                .setType(SkuType.SUBS)
                .build();
    }
}