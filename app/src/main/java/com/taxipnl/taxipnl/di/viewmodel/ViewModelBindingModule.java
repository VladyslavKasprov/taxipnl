package com.taxipnl.taxipnl.di.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.taxipnl.taxipnl.main.ExpenseTypesVM;
import com.taxipnl.taxipnl.main.ExpensesVM;
import com.taxipnl.taxipnl.main.JobsVM;
import com.taxipnl.taxipnl.main.MainActivityVM;
import com.taxipnl.taxipnl.main.ProvidersVM;
import com.taxipnl.taxipnl.main.dashboard.addtip.TipsVM;
import com.taxipnl.taxipnl.main.expensesdashboard.expenses.ExpensesFragmentVM;
import com.taxipnl.taxipnl.main.expensesdashboard.expenses.editexpense.EditExpenseFragmentVM;
import com.taxipnl.taxipnl.main.expensetypes.persist.PersistExpenseTypeFragmentVM;
import com.taxipnl.taxipnl.main.jobs.JobsFragmentVM;
import com.taxipnl.taxipnl.main.jobs.editjob.EditJobFragmentVM;
import com.taxipnl.taxipnl.main.providers.persist.PersistProviderFragmentVM;
import com.taxipnl.taxipnl.main.reports.ReportsVM;
import com.taxipnl.taxipnl.main.settings.SettingsVM;
import com.taxipnl.taxipnl.main.settings.changecurrency.ChangeCurrencyVM;
import com.taxipnl.taxipnl.main.settings.changepassword.ChangePasswordVM;
import com.taxipnl.taxipnl.main.settings.changetarget.ChangeTargetVM;
import com.taxipnl.taxipnl.main.settings.sendfeedback.SendFeedbackVM;
import com.taxipnl.taxipnl.main.setup.login.LoginVM;
import com.taxipnl.taxipnl.main.setup.register.RegisterVM;
import com.taxipnl.taxipnl.main.setup.subscibe.SubscribeVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelBindingModule {

    @Binds
    @ViewModelKey(MainActivityVM.class)
    @IntoMap
    abstract ViewModel mainActivityVM(MainActivityVM viewModel);

    @Binds
    @ViewModelKey(SubscribeVM.class)
    @IntoMap
    abstract ViewModel subscribeVM(SubscribeVM viewModel);

    @Binds
    @ViewModelKey(RegisterVM.class)
    @IntoMap
    abstract ViewModel registerVM(RegisterVM viewModel);

    @Binds
    @ViewModelKey(LoginVM.class)
    @IntoMap
    abstract ViewModel loginVM(LoginVM viewModel);

    @Binds
    @ViewModelKey(ReportsVM.class)
    @IntoMap
    abstract ViewModel reportsVM(ReportsVM viewModel);

    @Binds
    @ViewModelKey(ProvidersVM.class)
    @IntoMap
    abstract ViewModel providersVM(ProvidersVM viewModel);

    @Binds
    @ViewModelKey(PersistProviderFragmentVM.class)
    @IntoMap
    abstract ViewModel persistProviderFragmentVM(PersistProviderFragmentVM viewModel);

    @Binds
    @ViewModelKey(JobsVM.class)
    @IntoMap
    abstract ViewModel jobsVM(JobsVM viewModel);

    @Binds
    @ViewModelKey(JobsFragmentVM.class)
    @IntoMap
    abstract ViewModel jobsFragmentVM(JobsFragmentVM viewModel);

    @Binds
    @ViewModelKey(EditJobFragmentVM.class)
    @IntoMap
    abstract ViewModel editJobFragmentVM(EditJobFragmentVM viewModel);

    @Binds
    @ViewModelKey(ExpenseTypesVM.class)
    @IntoMap
    abstract ViewModel expenseTypesVM(ExpenseTypesVM viewModel);

    @Binds
    @ViewModelKey(PersistExpenseTypeFragmentVM.class)
    @IntoMap
    abstract ViewModel persistExpenseTypeFragmentVM(PersistExpenseTypeFragmentVM viewModel);

    @Binds
    @ViewModelKey(ExpensesVM.class)
    @IntoMap
    abstract ViewModel expensesVM(ExpensesVM viewModel);

    @Binds
    @ViewModelKey(ExpensesFragmentVM.class)
    @IntoMap
    abstract ViewModel expensesFragmentVM(ExpensesFragmentVM viewModel);

    @Binds
    @ViewModelKey(EditExpenseFragmentVM.class)
    @IntoMap
    abstract ViewModel editExpenseFragmentVM(EditExpenseFragmentVM viewModel);

    @Binds
    @ViewModelKey(TipsVM.class)
    @IntoMap
    abstract ViewModel tipsVM(TipsVM viewModel);

    @Binds
    @ViewModelKey(SettingsVM.class)
    @IntoMap
    abstract ViewModel settingsVM(SettingsVM viewModel);

    @Binds
    @ViewModelKey(ChangeTargetVM.class)
    @IntoMap
    abstract ViewModel changeTargetVM(ChangeTargetVM viewModel);

    @Binds
    @ViewModelKey(SendFeedbackVM.class)
    @IntoMap
    abstract ViewModel sendFeedbackVM(SendFeedbackVM viewModel);

    @Binds
    @ViewModelKey(ChangePasswordVM.class)
    @IntoMap
    abstract ViewModel changePasswordVM(ChangePasswordVM viewModel);

    @Binds
    @ViewModelKey(ChangeCurrencyVM.class)
    @IntoMap
    abstract ViewModel changeCurrencyVM(ChangeCurrencyVM viewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}