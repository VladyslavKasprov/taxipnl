package com.taxipnl.taxipnl.di;

import com.taxipnl.taxipnl.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = MainModule.class)
    @MainScope
    abstract MainActivity mainActivity();
}