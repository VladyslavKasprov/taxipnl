package com.taxipnl.taxipnl.di;

import com.taxipnl.taxipnl.main.HomeFragment;
import com.taxipnl.taxipnl.main.StartFragment;
import com.taxipnl.taxipnl.main.dashboard.CloseShiftDialogFragment;
import com.taxipnl.taxipnl.main.dashboard.DashboardExtraFragment;
import com.taxipnl.taxipnl.main.dashboard.DashboardFragment;
import com.taxipnl.taxipnl.main.dashboard.addjob.AddJobEnterFareTollFragment;
import com.taxipnl.taxipnl.main.dashboard.addjob.AddJobSelectProviderFragment;
import com.taxipnl.taxipnl.main.dashboard.addtip.AddTipFragment;
import com.taxipnl.taxipnl.main.expensesdashboard.ExpensesDashboardFragment;
import com.taxipnl.taxipnl.main.expensesdashboard.addexpense.AddExpenseEnterAmountFragment;
import com.taxipnl.taxipnl.main.expensesdashboard.expenses.ExpensesFragment;
import com.taxipnl.taxipnl.main.expensesdashboard.expenses.editexpense.EditExpenseEnterAmountFragment;
import com.taxipnl.taxipnl.main.expensesdashboard.expenses.editexpense.EditExpenseFragment;
import com.taxipnl.taxipnl.main.expensesdashboard.expenses.editexpense.EditExpenseSelectExpenseTypeFragment;
import com.taxipnl.taxipnl.main.expensesdashboard.expenses.filter.ExpensesFilterFragment;
import com.taxipnl.taxipnl.main.expensesdashboard.expenses.filter.ExpensesFilterPickEndDateDialog;
import com.taxipnl.taxipnl.main.expensesdashboard.expenses.filter.ExpensesFilterPickStartDateDialog;
import com.taxipnl.taxipnl.main.expensetypes.ExpenseTypesFragment;
import com.taxipnl.taxipnl.main.expensetypes.persist.AddExpenseTypeFragment;
import com.taxipnl.taxipnl.main.expensetypes.persist.EditExpenseTypeFragment;
import com.taxipnl.taxipnl.main.jobs.JobsFragment;
import com.taxipnl.taxipnl.main.jobs.editjob.EditJobEnterFareTollFragment;
import com.taxipnl.taxipnl.main.jobs.editjob.EditJobFragment;
import com.taxipnl.taxipnl.main.jobs.editjob.EditJobSelectProviderFragment;
import com.taxipnl.taxipnl.main.jobs.filter.JobsFilterFragment;
import com.taxipnl.taxipnl.main.jobs.filter.JobsFilterPickEndDateDialog;
import com.taxipnl.taxipnl.main.jobs.filter.JobsFilterPickStartDateDialog;
import com.taxipnl.taxipnl.main.providers.ProvidersFragment;
import com.taxipnl.taxipnl.main.providers.persist.AddProviderFragment;
import com.taxipnl.taxipnl.main.providers.persist.EditProviderFragment;
import com.taxipnl.taxipnl.main.reports.ExpensesReportFragment;
import com.taxipnl.taxipnl.main.reports.IncomeReportFragment;
import com.taxipnl.taxipnl.main.reports.ReportsFilterFragment;
import com.taxipnl.taxipnl.main.reports.ReportsFilterPickEndDateDialog;
import com.taxipnl.taxipnl.main.reports.ReportsFilterPickStartDateDialog;
import com.taxipnl.taxipnl.main.reports.ReportsFragment;
import com.taxipnl.taxipnl.main.settings.SettingsFragment;
import com.taxipnl.taxipnl.main.settings.changecurrency.ChangeCurrencyFragment;
import com.taxipnl.taxipnl.main.settings.changepassword.ChangePasswordFragment;
import com.taxipnl.taxipnl.main.settings.changetarget.ChangeTargetFragment;
import com.taxipnl.taxipnl.main.settings.sendfeedback.SendFeedbackFragment;
import com.taxipnl.taxipnl.main.setup.login.LoginFragment;
import com.taxipnl.taxipnl.main.setup.register.RegisterFragment;
import com.taxipnl.taxipnl.main.setup.subscibe.BillingErrorResponseDialogFragment;
import com.taxipnl.taxipnl.main.setup.subscibe.SubscribeFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainModule {

    @ContributesAndroidInjector
    abstract StartFragment startFragment();

    @ContributesAndroidInjector
    abstract SubscribeFragment subscribeFragment();

    @ContributesAndroidInjector
    abstract BillingErrorResponseDialogFragment billingErrorResponseDialogFragment();

    @ContributesAndroidInjector
    abstract RegisterFragment registerFragment();

    @ContributesAndroidInjector
    abstract LoginFragment loginFragment();


    @ContributesAndroidInjector
    abstract HomeFragment homeFragment();


    @ContributesAndroidInjector
    abstract DashboardFragment dashboardFragment();

    @ContributesAndroidInjector
    abstract AddJobEnterFareTollFragment addJobEnterFareTollFragment();

    @ContributesAndroidInjector
    abstract AddJobSelectProviderFragment addJobSelectProviderFragment();


    @ContributesAndroidInjector
    abstract DashboardExtraFragment dashboardExtraFragment();

    @ContributesAndroidInjector
    abstract AddTipFragment addTipFragment();

    @ContributesAndroidInjector
    abstract CloseShiftDialogFragment closeShiftDialogFragment();


    @ContributesAndroidInjector
    abstract ExpensesDashboardFragment expensesDashboardFragment();

    @ContributesAndroidInjector
    abstract AddExpenseEnterAmountFragment addExpenseEnterAmountFragment();

    @ContributesAndroidInjector
    abstract ExpensesFragment expensesFragment();

    @ContributesAndroidInjector
    abstract ExpensesFilterFragment expensesFilterFragment();

    @ContributesAndroidInjector
    abstract ExpensesFilterPickStartDateDialog expensesFilterPickStartDateDialog();

    @ContributesAndroidInjector
    abstract ExpensesFilterPickEndDateDialog expensesFilterPickEndDateDialog();

    @ContributesAndroidInjector
    abstract EditExpenseFragment editExpenseFragment();

    @ContributesAndroidInjector
    abstract EditExpenseEnterAmountFragment editExpenseEnterAmountFragment();

    @ContributesAndroidInjector
    abstract EditExpenseSelectExpenseTypeFragment editExpenseSelectExpenseTypeFragment();


    @ContributesAndroidInjector
    abstract ReportsFragment reportsFragment();

    @ContributesAndroidInjector
    abstract IncomeReportFragment incomeReportFragment();

    @ContributesAndroidInjector
    abstract ExpensesReportFragment expenseReportFragment();

    @ContributesAndroidInjector
    abstract ReportsFilterFragment reportsFilterFragment();

    @ContributesAndroidInjector
    abstract ReportsFilterPickStartDateDialog reportsFilterPickStartDateDialog();

    @ContributesAndroidInjector
    abstract ReportsFilterPickEndDateDialog reportsFilterPickEndDateDialog();


    @ContributesAndroidInjector
    abstract SettingsFragment settingsFragment();


    @ContributesAndroidInjector
    abstract ProvidersFragment providersFragment();

    @ContributesAndroidInjector
    abstract AddProviderFragment addProviderFragment();

    @ContributesAndroidInjector
    abstract EditProviderFragment editProviderFragment();


    @ContributesAndroidInjector
    abstract ExpenseTypesFragment expenseTypesFragment();

    @ContributesAndroidInjector
    abstract AddExpenseTypeFragment addExpenseTypeFragment();

    @ContributesAndroidInjector
    abstract EditExpenseTypeFragment editExpenseTypeFragment();


    @ContributesAndroidInjector
    abstract JobsFragment jobsFragment();

    @ContributesAndroidInjector
    abstract JobsFilterFragment jobsFilterFragment();

    @ContributesAndroidInjector
    abstract JobsFilterPickStartDateDialog jobsFilterPickStartDateDialog();

    @ContributesAndroidInjector
    abstract JobsFilterPickEndDateDialog jobsFilterPickEndDateDialog();

    @ContributesAndroidInjector
    abstract EditJobFragment editJobFragment();

    @ContributesAndroidInjector
    abstract EditJobEnterFareTollFragment editJobFareTollFragment();

    @ContributesAndroidInjector
    abstract EditJobSelectProviderFragment editJobSelectProviderFragment();


    @ContributesAndroidInjector
    abstract ChangeTargetFragment changeTargetFragment();


    @ContributesAndroidInjector
    abstract SendFeedbackFragment sendFeedbackFragment();


    @ContributesAndroidInjector
    abstract ChangePasswordFragment changePasswordFragment();


    @ContributesAndroidInjector
    abstract ChangeCurrencyFragment changeCurrencyFragment();
}