package com.taxipnl.taxipnl.di;

import android.app.Application;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.androidpublisher.AndroidPublisher;
import com.taxipnl.taxipnl.di.qualifiers.ApplicationName;
import com.taxipnl.taxipnl.di.qualifiers.PackageName;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.taxipnl.taxipnl.util.constants.Billing.CLIENT_ID;
import static com.taxipnl.taxipnl.util.constants.Billing.CLIENT_SECRET;
import static com.taxipnl.taxipnl.util.constants.Billing.REFRESH_TOKEN;

@Module
public class GooglePlayDeveloperApiModule {

    @Provides
    @ApplicationName
    @Singleton
    public String applicationName(Application application) {
        return application.getApplicationInfo().loadLabel(application.getPackageManager()).toString();
    }

    @Provides
    @PackageName
    @Singleton
    public String packageName(Application application) {
        return application.getPackageName();
    }

    @Provides
    @Singleton
    public AndroidPublisher androidPublisher(@ApplicationName String applicationName, HttpTransport httpTransport,
                                             JsonFactory jsonFactory, Credential credential) {
        return new AndroidPublisher.Builder(httpTransport, jsonFactory, credential)
                .setApplicationName(applicationName)
                .build();
    }

    @Provides
    @Singleton
    public Credential credential(HttpTransport httpTransport, JsonFactory jsonFactory) {
        return new GoogleCredential.Builder()
                .setClientSecrets(CLIENT_ID, CLIENT_SECRET)
                .setTransport(httpTransport)
                .setJsonFactory(jsonFactory)
                .build()
                .setRefreshToken(REFRESH_TOKEN);
    }

    @Provides
    @Singleton
    public HttpTransport httpTransport() {
        return new NetHttpTransport();
    }

    @Provides
    @Singleton
    public JsonFactory jsonFactory() {
        return JacksonFactory.getDefaultInstance();
    }
}