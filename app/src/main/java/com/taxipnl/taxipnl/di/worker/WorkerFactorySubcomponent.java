package com.taxipnl.taxipnl.di.worker;

import android.content.Context;

import java.util.Map;

import javax.inject.Provider;

import androidx.work.Worker;
import androidx.work.WorkerParameters;
import dagger.BindsInstance;
import dagger.Subcomponent;

@Subcomponent(modules = {
        WorkerBindingModule.class
})
public interface WorkerFactorySubcomponent {

    Map<Class<? extends Worker>, Provider<Worker>> creators();

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        Builder applicationContext(Context applicationContext);

        @BindsInstance
        Builder workerParameters(WorkerParameters workerParameters);

        WorkerFactorySubcomponent build();
    }
}