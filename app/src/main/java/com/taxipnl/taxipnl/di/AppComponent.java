package com.taxipnl.taxipnl.di;

import android.app.Application;

import com.taxipnl.taxipnl.App;
import com.taxipnl.taxipnl.di.viewmodel.ViewModelBindingModule;
import com.taxipnl.taxipnl.di.worker.WorkerFactoryModule;
import com.taxipnl.taxipnl.di.worker.WorkerFactorySubcomponent;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        ActivityBindingModule.class,
        ViewModelBindingModule.class,
        WorkerFactoryModule.class,
        DatabaseModule.class,
        NetworkModule.class,
        BillingClientApiModule.class,
        GooglePlayDeveloperApiModule.class
})
public interface AppComponent extends AndroidInjector<App> {

    WorkerFactorySubcomponent.Builder workerFactorySubcomponentBuilder();

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<App> {

        @BindsInstance
        public abstract Builder application(Application application);
    }
}