package com.taxipnl.taxipnl.di.worker;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.taxipnl.taxipnl.di.AppComponent;

import java.util.Map;

import javax.inject.Provider;

import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerFactory;
import androidx.work.WorkerParameters;

public class DaggerWorkerFactory extends WorkerFactory {

    private AppComponent appComponent;

    public DaggerWorkerFactory(AppComponent appComponent) {
        this.appComponent = appComponent;
    }

    @Nullable
    @Override
    public ListenableWorker createWorker(@NonNull Context applicationContext, @NonNull String workerClassName,
                                         @NonNull WorkerParameters workerParameters) {
        WorkerFactorySubcomponent workerFactorySubcomponent = appComponent
                .workerFactorySubcomponentBuilder()
                .applicationContext(applicationContext)
                .workerParameters(workerParameters)
                .build();
        return createWorker(workerClassName, workerFactorySubcomponent.creators());
    }

    private ListenableWorker createWorker(String workerClassName, Map<Class<? extends Worker>, Provider<Worker>> creators) {
        try {
            Class<? extends Worker> workerClass = Class.forName(workerClassName).asSubclass(Worker.class);
            Provider<Worker> creator = creators.get(workerClass);
            if (creator == null) {
                for (Map.Entry<Class<? extends Worker>, Provider<Worker>> entry : creators.entrySet()) {
                    if (workerClass.isAssignableFrom(entry.getKey())) {
                        creator = entry.getValue();
                    }
                }
            }
            if (creator == null) {
                throw new IllegalArgumentException("No creator found for worker: " + workerClassName);
            }
            return creator.get();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}